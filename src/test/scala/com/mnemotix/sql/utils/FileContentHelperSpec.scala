/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.sql.utils

import com.mnemotix.synaptix.cache.RocksDBStore
import fr.mnhn.datahub.connectors.{AnyFlatTestSpec, RDFSerializer}
import fr.mnhn.datahub.connectors.es.model.CollectionGroup
import fr.mnhn.datahub.connectors.es.utils.BuConfig
import play.api.libs.json.Json
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.es.helper.CollectionGroupRdfMapper.collectionGroupToRdfModel

class FileContentHelperSpec extends AnyFlatTestSpec {

  it should "create a sha256 from a file string content" ignore {
    val cacheDir = BuConfig.collectionGroupcacheDir
    lazy val cache = new RocksDBStore(cacheDir)
    cache.init()
    val entry = cache.iterator().toSeq.head
    val entryVal = new String(entry.value)
    val collectionGroup = Json.parse(entryVal).as[CollectionGroup]
    val modelString = RDFSerializer.toTriG(collectionGroup.toRdfModel)
    val modelString2 = RDFSerializer.toTriG(collectionGroup.toRdfModel)
    val sha = FileContentHelper.sha256(modelString)
    val sha2 = FileContentHelper.sha256(modelString2)
    sha.shouldEqual(sha2)
  }
}