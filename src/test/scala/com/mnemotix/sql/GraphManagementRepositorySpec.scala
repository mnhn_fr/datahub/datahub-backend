/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.sql

import akka.stream.alpakka.slick.scaladsl.SlickSession
import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import com.mnemotix.synaptix.cache.RocksDBStore
import fr.mnhn.datahub.connectors.{AnyFlatTestSpec, RDFSerializer}
import fr.mnhn.datahub.connectors.es.model.CollectionGroup
import fr.mnhn.datahub.connectors.es.utils.BuConfig
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}
import play.api.libs.json.Json
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.es.helper.CollectionGroupRdfMapper.collectionGroupToRdfModel
import slick.lifted.Query

import java.time.LocalDate
import scala.util.{Failure, Success}


class GraphManagementRepositorySpec extends AnyFlatTestSpec {

  it should "create tables" ignore {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val graphManagementRepository = new GraphManagementRepository()
    val result = databaseConfig.db.run(graphManagementRepository.createTables())

    Await.result(result, 10.seconds)
  }

  it should "insert a subgraph" ignore {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val cacheDir = BuConfig.collectionGroupcacheDir
    lazy val cache = new RocksDBStore(cacheDir)
    cache.init()
    val entry = cache.iterator().toSeq.head
    val entryVal = new String(entry.value)

    val collectionGroup = Json.parse(entryVal).as[CollectionGroup]
    val modelString = RDFSerializer.toTriG(collectionGroup.toRdfModel)

    val sha = FileContentHelper.sha256(modelString)
    val subgraph = Subgraph(collectionGroup.id(), "https://www.datahub.mnhn.fr/data/baseunifiee/NG", collectionGroup.resourceUri(), collectionGroup.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")

    val graphManagementRepository = new GraphManagementRepository()
    val mainInfo = databaseConfig.db.run(graphManagementRepository.subGraphInsertable.insertOrUpdate(subgraph))
    Await.result(mainInfo, Duration.Inf)
  }

  it should "get a value from the database" ignore {
    import slick.jdbc.PostgresProfile.api._
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)
    val graphManagementRepository = new GraphManagementRepository()

    val cacheDir = BuConfig.collectionGroupcacheDir
    lazy val cache = new RocksDBStore(cacheDir)
    cache.init()
    val entry = cache.iterator().toSeq.head
    val entryVal = new String(entry.value)
    val collectionGroup = Json.parse(entryVal).as[CollectionGroup]
    val modelString = RDFSerializer.toTriG(collectionGroup.toRdfModel)
    val sha = FileContentHelper.sha256(modelString)

    val id = collectionGroup.id()
    val rs = graphManagementRepository.subGraphTable.filter(_.id === id)
    val result: Subgraph = Await.result(databaseConfig.db.run(rs.result), Duration.Inf).toSeq.head

    result.id shouldEqual id
    result.checksum shouldEqual sha
  }
}