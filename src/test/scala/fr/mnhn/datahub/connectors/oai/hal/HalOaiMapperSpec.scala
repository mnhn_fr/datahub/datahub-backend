package fr.mnhn.datahub.connectors.oai.hal

import fr.mnhn.datahub.TestSpec

class HalOaiMapperSpec extends TestSpec {
  "HalOaiMapper" should {
    val mapper = new HalOaiMapperJob
    "iterate through cache entries and map it to RDF Model" in {
      mapper.init()
      mapper.process()
      mapper.shutdown()
    }
  }
}
