/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.oai

import com.mnemotix.synaptix.cache.{CacheEntry, RocksDBStore}
import fr.mnhn.datahub.TestSpec
import com.mnemotix.synaptix.core.utils.StringUtils

import java.nio.file.{Files, Paths}

class OaiCacheSpec extends TestSpec {
  val cacheStore = new RocksDBStore(OaiHarvestingConfiguration.halCacheDir)
  cacheStore.init()
  "OaiCacheSpec" should {
    "count the number of elements in HAL" in {
      val it = cacheStore.iterator()
      val keys = it.keys().toSeq
      println(s"The number of values for HAL is ${keys.size}")
      it.close()
    }
  }
  "serialize cache values into files in a given folder" in {
    val it = cacheStore.iterator()
    Files.createDirectories(Paths.get("./data/hal/oai/xml"))
    var count = 0
    while (it.hasNext) {
      val pageIterator = it.take(OaiHarvestingConfiguration.halMergeSize)
      count += OaiHarvestingConfiguration.halMergeSize
      val sb = new StringBuilder();
      sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<dataset>\n")
      while (pageIterator.hasNext) {
        val entry: CacheEntry = pageIterator.next()
        sb.append(new String(entry.value, "UTF-8"))
        sb.append("\n")
      }
      sb.append("\n</dataset>")
      val filepath = Paths.get(s"./data/hal/oai/xml/$count.xml")
      logger.debug(s"File created ${filepath.toAbsolutePath.toString}")
      Files.write(filepath, sb.toString().getBytes("UTF-8"))
    }
    it.close()
    cacheStore.shutdown()
  }
}