package fr.mnhn.datahub.connectors.rest.hal

import akka.Done
import fr.mnhn.datahub.TestSpec

import scala.concurrent.Future
import scala.util._

class HalRestHarvesterJobSpec extends TestSpec {
  "HalOaiHarvesterJob" should {
    val harvester = new HalRestHarvesterJob()
    "harvest HAL DC data and put it into a cache structure without error" in {
      val f: Future[Done] = harvester.execute()
      f.onComplete {
        case Success(value: Done) => value.isInstanceOf[Done] shouldBe true
        case Failure(_) => assert(false)
      }
      f.futureValue
    }
  }
}
