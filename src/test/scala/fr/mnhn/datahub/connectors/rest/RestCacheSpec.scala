/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.rest

import com.mnemotix.synaptix.cache.{CacheEntry, RocksDBStore}
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.TestSpec
import fr.mnhn.datahub.connectors.rest.hal.RestHarvestingConfiguration

import java.nio.file.{Files, Paths}

class RestCacheSpec extends TestSpec {
  val cacheStore = new RocksDBStore(RestHarvestingConfiguration.halCacheDir)
  cacheStore.init()
  "RestCacheSpec" should {
    "count the number of elements in cache" in {
      val it = cacheStore.iterator()
      val keys = it.keys().toSeq
      println(s"The number of values for HAL is ${keys.size}")
      it.close()
    }
  }
  "serialize cache values into files in a given folder" ignore {
    val it = cacheStore.iterator()
    Files.createDirectories(Paths.get("./data/hal/rest/xml"))
    var count = 0
    while (it.hasNext) {
      val pageIterator = it.take(RestHarvestingConfiguration.halMergeSize)
      count += RestHarvestingConfiguration.halMergeSize
      val sb = new StringBuilder();
      while(pageIterator.hasNext){
        val entry: CacheEntry = pageIterator.next()
        println(entry.key)
        sb.append(entry.value)
      }
      val filepath = Paths.get(s"./data/hal/rest/xml/$count.xml")
      logger.debug(s"File created ${filepath.toAbsolutePath.toString}")
      Files.write(filepath, sb.toString().getBytes("UTF-8"))
    }
    it.close()
    cacheStore.shutdown()
  }
}