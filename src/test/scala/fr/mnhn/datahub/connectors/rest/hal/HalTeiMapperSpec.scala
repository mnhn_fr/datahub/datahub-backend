package fr.mnhn.datahub.connectors.rest.hal

import fr.mnhn.datahub.TestSpec

class HalTeiMapperSpec extends TestSpec {
  "HalOaiMapper" should {
    val mapper = new HalTeiMapperJob
    "iterate through cache entries and map it to RDF Model" in {
      mapper.init()
      mapper.process()
      mapper.shutdown()
    }
  }
}
