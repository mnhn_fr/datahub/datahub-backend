package fr.mnhn.datahub.connectors.es.mapping

object Institution {
  def mapping = """{
                  |    "properties": {
                  |      "attributes": {
                  |        "type": "object",
                  |        "properties": {
                  |          "collected_by": { "type": "text", "store": true },
                  |          "data_origin": { "type": "keyword" },
                  |          "duplicate_count": { "type": "integer" },
                  |          "duplicate_destination": { "type": "text", "store": true },
                  |          "event_date": { "type": "date", "format": "strict_date_time" },
                  |          "event_date_begin": { "type": "date", "format": "strict_date_time" },
                  |          "event_date_end": { "type": "date", "format": "strict_date_time" },
                  |          "event_date_verbatim": { "type": "text", "store": true },
                  |          "field_number": { "type": "keyword" },
                  |          "harvest_identifier": { "type": "keyword" },
                  |          "host": { "type": "text", "store": true },
                  |          "old_harvest_identifier": { "type": "keyword" },
                  |          "part_count": { "type": "integer" },
                  |          "remarks": { "type": "text", "store": true },
                  |          "station_number": { "type": "keyword" },
                  |          "usage": { "type": "keyword" }
                  |        }
                  |      },
                  |      "created_at": { "type": "date", "format": "strict_date_time" },
                  |      "institution_id": { "type": "integer" },
                  |      "code": { "type": "keyword" },
                  |      "description": { "type": "text", "store": true },
                  |      "name": { "type": "text", "store": true },
                  |      "entityId": {  "type": "keyword"  }
                  |    }
                  |  }""".stripMargin
}
