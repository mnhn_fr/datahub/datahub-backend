/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.connectors.AnyFlatTestSpec
import fr.mnhn.datahub.connectors.es.utils.BuConfig
import org.scalatest.BeforeAndAfter

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class GenericHarvesterSpec extends AnyFlatTestSpec with BeforeAndAfter {

  override def beforeAll(): Unit = {
    FakeDataInit.init()
    FakeCacheInit.init()
  }

  override def afterAll(): Unit = (FakeDataInit.shutdown())

  it should "harvest collection and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectioncacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionindexName
    val sortField = "collection_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    Await.result(fut, Duration.Inf)
    geolocationHarvester.cache.iterator().size should be > 0
    geolocationHarvester.shutdown()
  }

  it should "harvest collectionEvent and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionEventcacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionEventindexName
    val sortField = "collection_event_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    val r = Await.result(fut, Duration.Inf)
    geolocationHarvester.cache.iterator().size should be > 0
    geolocationHarvester.shutdown()
    r shouldBe(true)
  }

  it should "harvest collectionGroup and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionGroupcacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionGroupindexName
    val sortField = "collection_group_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }

  it should "harvest coordinates.json and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.coordinatescacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.coordinatesindexName
    val sortField = "coordinates_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }

  it should "harvest domain and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.domaincacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.domainindexName
    val sortField = "domain_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }

  it should "harvest geolocation and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.geolocationcacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.geolocationindexName
    val sortField = "geolocation_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }

  it should "harvest identification and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.identificationcacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.identificationindexName
    val sortField = "identification_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }

  it should "harvest institution and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.institutioncacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.institutionindexName
    val sortField = "institution_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }

  it should "harvest material and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.materialcacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.materialindexName
    val sortField = "material_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }


  it should "harvest materialRelationship and add them to the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.materialRelationshipcacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.materialRelationshipindexName
    val sortField = "subject_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }

  it should "harvest taxon and add them in the cache" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.taxoncacheDir
    val indexName = ESConfiguration.prefix.getOrElse("")+BuConfig.taxonindexName
    val sortField = "taxon_id"

    val geolocationHarvester = new GenericHarvester(indexName, cacheDir, sortField)
    geolocationHarvester.init()
    val fut = geolocationHarvester.process()
    geolocationHarvester.cache.iterator().size should be > 0
    Await.result(fut, Duration.Inf)
    geolocationHarvester.shutdown()
  }

}

