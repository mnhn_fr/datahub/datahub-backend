/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.Done
import akka.stream.scaladsl.Sink
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.connectors.AnyFlatTestSpec
import fr.mnhn.datahub.connectors.es.helper.GeolocationRdfMapper.geolocationsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.MaterialRdfMapper.materialsToRdfModel
import fr.mnhn.datahub.connectors.es.model.Material.defaultValueForMaterial
import fr.mnhn.datahub.connectors.es.model.{Collection, CollectionEvent, CollectionGroup, Domain, Geolocation, Identification, Material, MaterialRelationship, Taxon}
import fr.mnhn.datahub.connectors.es.model.CollectionEvent.defaultForCollectionEvent
import fr.mnhn.datahub.connectors.es.utils.BuConfig
import fr.mnhn.datahub.connectors.rest.model.Digitalizations
import fr.mnhn.datahub.connectors.rest.troisdtheque.DigitalizationsRdfMapper.digitalizationsToRdfModel

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

class RdfMapperSpec extends AnyFlatTestSpec {

  override def beforeAll(): Unit = {
    FakeDataInit.init()
    FakeCacheInit.init()
  }

  override def afterAll(): Unit = (FakeDataInit.shutdown())


  it should "map collection event to obj" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionEventcacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val events = mapper.cacheExtractor[CollectionEvent].runWith(Sink.foreach(event => println(event)))
   val rs = Await.result(events, Duration.Inf)
    rs.isInstanceOf[Done] shouldBe(true)
  }

  it should "map identification event to obj" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.identificationcacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val events = mapper.cacheExtractor[Identification].runWith(Sink.foreach(event => println(event.attributes.identified_by)))
    val rs = Await.result(events, Duration.Inf)
    rs.isInstanceOf[Done] shouldBe(true)

  }

  it should "map geolocation to RDF" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.geolocationcacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Geolocation](100000).run()
    val rs = Await.result(res, Duration.Inf)
    rs.isInstanceOf[Done] shouldBe(true)

  }

  it should "map material to RDF" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.materialcacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
   val res = mapper.process[Material](10000).run()
   val rs=  Await.result(res, Duration.Inf)
    rs.isInstanceOf[Done] shouldBe(true)

  }

  it should "map Digitalizations to RDF" ignore  {
    val cacheDir = s"data/troisdtheque/cache.db"
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Digitalizations](1000).run()
    val rs=  Await.result(res, Duration.Inf)
    println(rs)
  }

  it should "map material relationship to obj" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.materialRelationshipcacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val events = mapper.cacheExtractor[MaterialRelationship].runWith(Sink.foreach(matRel => println(matRel)))
    Await.result(events, Duration.Inf)
  }

  it should "map collection group to obj" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionGroupcacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val events = mapper.cacheExtractor[CollectionGroup].runWith(Sink.foreach(group => println(group)))
    Await.result(events, Duration.Inf)
  }

  it should "map domain to obj" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.domaincacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val events = mapper.cacheExtractor[Domain].runWith(Sink.foreach(group => println(group)))
    Await.result(events, Duration.Inf)
  }

  it should "map taxon to obj" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.taxoncacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val events = mapper.cacheExtractor[Taxon].runWith(Sink.foreach(taxon => println(taxon)))
    Await.result(events, Duration.Inf)
  }


  it should "map material to obj" in {
  // https://www.data.mnhn.fr/data/material/006f2788-6b7e-592d-bd61-997d66f245b5
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.materialcacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()

    val mat= mapper.cacheExtractor[Material].runWith(Sink.foreach(mat => println(mat)))
    Await.result(mat, Duration.Inf)
  }

  it should "map materialRelationship to obj" in {
    // https://www.data.mnhn.fr/data/material/006f2788-6b7e-592d-bd61-997d66f245b5
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.materialRelationshipcacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val mat = mapper.cacheExtractor[MaterialRelationship].filter(mat => mat.object_id == "006f2788-6b7e-592d-bd61-997d66f245b5" || mat.subject_id == "006f2788-6b7e-592d-bd61-997d66f245b5").runWith(Sink.foreach(mat => println(mat)))
    Await.result(mat, Duration.Inf)
  }

  it should "map collection to obj" in {
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectioncacheDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    //mapper.cacheExtractor[Collection].runWith(Sink.foreach(mat => println(mat)))
    val fut: Future[Seq[Collection]] = mapper.cacheExtractor[Collection].runWith(Sink.seq)

    val seq = Await.result(fut, Duration.Inf)
    println(seq.size)
  }
}