package fr.mnhn.datahub.connectors.es.mapping

object Collection_event {
  val mapping = """{
                  |  "properties": {
                  |    "attributes": {
                  |      "type": "object",
                  |      "properties": {
                  |        "collected_by": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "data_origin": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "duplicate_count": {
                  |          "type": "float"
                  |        },
                  |        "duplicate_destination": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "event_date": {
                  |          "type": "date",
                  |          "format": "strict_date_time"
                  |        },
                  |        "event_date_begin": {
                  |          "type": "date",
                  |          "format": "strict_date_time"
                  |        },
                  |        "event_date_end": {
                  |          "type": "date",
                  |          "format": "strict_date_time"
                  |        },
                  |        "event_date_verbatim": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "field_number": {
                  |          "type": "keyword"
                  |        },
                  |        "harvest_identifier": {
                  |          "type": "keyword"
                  |        },
                  |        "host": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "old_harvest_identifier": {
                  |          "type": "keyword"
                  |        },
                  |        "part_count": {
                  |          "type": "float"
                  |        },
                  |        "remarks": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "station_number": {
                  |          "type": "keyword"
                  |        },
                  |        "usage": {
                  |          "type": "text",
                  |          "store": true
                  |        }
                  |      }
                  |    },
                  |    "code": {
                  |      "type": "keyword"
                  |    },
                  |    "collection_event_id": {
                  |      "type": "keyword"
                  |    },
                  |    "coordinates_id": {
                  |      "type": "keyword"
                  |    },
                  |    "entityId": {
                  |      "type": "keyword"
                  |    },
                  |    "created_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "description": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "geolocation_id": {
                  |      "type": "keyword"
                  |    },
                  |    "is_deleted": {
                  |      "type": "boolean"
                  |    },
                  |    "label": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "last_modified_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "name": {
                  |      "type": "text",
                  |      "store": true
                  |    }
                  |  }
                  |}""".stripMargin
}
