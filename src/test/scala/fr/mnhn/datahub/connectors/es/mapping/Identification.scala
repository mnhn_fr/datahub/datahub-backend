package fr.mnhn.datahub.connectors.es.mapping

object Identification {

  val mapping = """{
                  |  "properties": {
                  |    "code": {
                  |      "type": "keyword"
                  |    },
                  |    "entityId": {
                  |      "type": "keyword"
                  |    },
                  |    "created_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "deleted_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "description": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "identification_id": {
                  |      "type": "keyword"
                  |    },
                  |    "is_deleted": {
                  |      "type": "boolean"
                  |    },
                  |    "label": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "last_modified_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "material_id": {
                  |      "type": "keyword"
                  |    },
                  |    "name": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "taxon_id": {
                  |      "type": "keyword"
                  |    },
                  |    "attributes": {
                  |      "type": "object",
                  |      "properties": {
                  |        "date_identified": {
                  |          "type": "date",
                  |          "format": "strict_date"
                  |        },
                  |        "date_identified_begin": {
                  |          "type": "date",
                  |          "format": "strict_date"
                  |        },
                  |        "date_identified_end": {
                  |          "type": "date",
                  |          "format": "strict_date"
                  |        },
                  |        "identification_qualifier": {
                  |          "type": "keyword"
                  |        },
                  |        "identified_by": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "is_current": {
                  |          "type": "boolean"
                  |        },
                  |        "remarks": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "type_status": {
                  |          "type": "keyword"
                  |        },
                  |        "type_status_remarks": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "verbatim_date_identified": {
                  |          "type": "keyword"
                  |        }
                  |      }
                  |    }
                  |  }
                  |}""".stripMargin

}
