/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.stream.scaladsl.Sink
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.connectors.AnyFlatTestSpec
import fr.mnhn.datahub.connectors.es.model.{CollectionEvent, Coordinates, Geolocation, Identification, Material, Taxon}
import org.scalatest.BeforeAndAfter

import java.util.concurrent.TimeUnit
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

class ESClientSpec extends AnyFlatTestSpec with BeforeAndAfter {

  override def beforeAll(): Unit = (FakeDataInit.init())

  override def afterAll(): Unit = (FakeDataInit.shutdown())

  it should "init index cluster" in {
    val es = new ESClient()
    es.init()
  }

  it should "list index name" in {
    val es = new ESClient()
    es.init()
    val fut = es.listIndexName()
    val l = Await.result(fut, Duration.Inf)
    l.foreach(println(_))

    l.size should be > 0
  }

  it should "extract all taxon" in {
    val es = new ESClient()
    es.init()
    val start = System.nanoTime()
    val s  = es
      .sourceFromES[Taxon](Seq(s"${ESConfiguration.prefix.getOrElse("")}taxon"), "taxon_id")
      .runWith(Sink.seq)

    val res = Await.result(s, Duration.Inf).flatten
    println(res.size)
    val end = System.nanoTime()
    val difference = end - start
    println("Total execution time: " +
      TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
      ( TimeUnit.NANOSECONDS.toMinutes(difference) -  TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference)))   + " min " +
      ( TimeUnit.NANOSECONDS.toSeconds(difference) -  TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
      " - " + difference + " nSec (Total)")

    res.size should be > 0
  }

  it should "extract material in test-material" in {
    val es = new ESClient()
    es.init()

    val start = System.nanoTime()
    val s = es.sourceFromES[Material](Seq(s"${ESConfiguration.prefix.getOrElse("")}material"), "material_id")
      .runWith(Sink.seq)

    val res = Await.result(s, Duration.Inf).flatten
    println(res.size)
    val end = System.nanoTime()
    val difference = end - start
    println("Total execution time: " +
      TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
      (TimeUnit.NANOSECONDS.toMinutes(difference) - TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference))) + " min " +
      (TimeUnit.NANOSECONDS.toSeconds(difference) - TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
      " - " + difference + " nSec (Total)")

    res.size should be > 0

  }

  it should "extract all test-identification" in {
    val es = new ESClient()
    es.init()

    val start = System.nanoTime()
    val s = es.sourceFromES[Identification](Seq(s"${ESConfiguration.prefix.getOrElse("")}identification"), "identification_id")
      .runWith(Sink.seq)

    val res = Await.result(s, Duration.Inf).flatten
    println(res.size)
    val end = System.nanoTime()
    val difference = end - start
    println("Total execution time: " +
      TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
      (TimeUnit.NANOSECONDS.toMinutes(difference) - TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference))) + " min " +
      (TimeUnit.NANOSECONDS.toSeconds(difference) - TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
      " - " + difference + " nSec (Total)")

    res.size should be > 0

  }

  it should "extract test-geolocation" in {
    val es = new ESClient()
    es.init()

    val start = System.nanoTime()
    val s = es.sourceFromES[Geolocation](Seq(s"${ESConfiguration.prefix.getOrElse("")}geolocation"), "geolocation_id")
      .runWith(Sink.seq)

    val res = Await.result(s, Duration.Inf).flatten
    println(res.size)
    val end = System.nanoTime()
    val difference = end - start
    println("Total execution time: " +
      TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
      (TimeUnit.NANOSECONDS.toMinutes(difference) - TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference))) + " min " +
      (TimeUnit.NANOSECONDS.toSeconds(difference) - TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
      " - " + difference + " nSec (Total)")

    res.size should be > 0

  }

  it should "extract test-collection_event" in {
    val es = new ESClient()
    es.init()

    val start = System.nanoTime()
    val s = es.sourceFromES[CollectionEvent](Seq(s"${ESConfiguration.prefix.getOrElse("")}collection_event"), "collection_event_id")
      .runWith(Sink.seq)

    val res = Await.result(s, Duration.Inf).flatten
    println(res.size)
    val end = System.nanoTime()
    val difference = end - start
    println("Total execution time: " +
      TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
      (TimeUnit.NANOSECONDS.toMinutes(difference) - TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference))) + " min " +
      (TimeUnit.NANOSECONDS.toSeconds(difference) - TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
      " - " + difference + " nSec (Total)")

    res.size should be > 0

  }

  it should "extract test-coordinates" in {
    val es =new ESClient()
    es.init()
    val start = System.nanoTime()
    val s = es.sourceFromES[Coordinates](Seq(s"${ESConfiguration.prefix.getOrElse("")}coordinates"), "coordinates_id")
      .runWith(Sink.seq)

    val res = Await.result(s, Duration.Inf).flatten
    println(res.size)
    val end = System.nanoTime()
    val difference = end - start
    println("Total execution time: " +
      TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
      (TimeUnit.NANOSECONDS.toMinutes(difference) - TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference))) + " min " +
      (TimeUnit.NANOSECONDS.toSeconds(difference) - TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
      " - " + difference + " nSec (Total)")

    res.size should be > 0

  }

  it should "extract searchhit in an index" in {
    val es = new ESClient()
    es.init()
    val start = System.nanoTime()
    val s = es.searchHitSourceFromES(Seq(s"${ESConfiguration.prefix.getOrElse("")}geolocation"), "geolocation_id")
      .runWith(Sink.seq)

    val res = Await.result(s, Duration.Inf).flatten
    println(res.size)
    val end = System.nanoTime()
    val difference = end - start
    println("Total execution time: " +
      TimeUnit.NANOSECONDS.toHours(difference) + " hours " +
      (TimeUnit.NANOSECONDS.toMinutes(difference) - TimeUnit.HOURS.toMinutes(TimeUnit.NANOSECONDS.toHours(difference))) + " min " +
      (TimeUnit.NANOSECONDS.toSeconds(difference) - TimeUnit.MINUTES.toSeconds(TimeUnit.NANOSECONDS.toMinutes(difference))) + " sec " +
      " - " + difference + " nSec (Total)")

    res.size should be > 0

  }
}
