package fr.mnhn.datahub.connectors.es.mapping

object Material {

  val mapping = """{
                  |  "properties": {
                  |    "attributes": {
                  |     "type": "object",
                  |      "properties": {
                  |        "material_type": {
                  |          "type": "keyword"
                  |        },
                  |        "remarks": {
                  |          "type": "text",
                  |          "store": true
                  |        }
                  |      }
                  |    },
                  |    "catalog_number": {
                  |      "type": "keyword"
                  |    },
                  |    "code": {
                  |      "type": "keyword"
                  |    },
                  |    "entityId": {
                  |      "type": "keyword"
                  |    },
                  |    "collection_code": {
                  |      "type": "keyword"
                  |    },
                  |    "collection_event_id": {
                  |      "type": "keyword"
                  |    },
                  |    "collection_id": {
                  |      "type": "integer"
                  |    },
                  |    "created_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "description": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "domain_id": {
                  |      "type": "integer"
                  |    },
                  |    "entry_number": {
                  |      "type": "keyword"
                  |    },
                  |    "institution_code": {
                  |      "type": "keyword"
                  |    },
                  |    "is_deleted": {
                  |      "type": "boolean"
                  |    },
                  |    "is_private": {
                  |      "type": "boolean"
                  |    },
                  |    "label": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "last_modified_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "material_id": {
                  |      "type": "keyword"
                  |    },
                  |    "name": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "entityId": {  "type": "keyword"  }
                  |  }
                  |}""".stripMargin


}
