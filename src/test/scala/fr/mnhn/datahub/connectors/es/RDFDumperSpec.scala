/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.stream.alpakka.slick.scaladsl.SlickSession
import akka.stream.scaladsl.Sink
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.RDFClientWriteConnection
import fr.mnhn.datahub.connectors.AnyFlatTestSpec
import fr.mnhn.datahub.connectors.es.helper.CollectionEventRdfMapper.collectionEventsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.CollectionGroupRdfMapper.collectionGroupsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.CollectionRdfMapper.collectionsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.CoordinatesRdfMapper.coordinatesToRdfModel
import fr.mnhn.datahub.connectors.es.helper.DomainRdfMapper.domainsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.GeolocationRdfMapper.geolocationsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.IdentificationRdfMapper.identificationsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.InstitutionRdfMapper.institutionsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.MaterialRdfMapper.materialsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.MaterialRelationshipRdfMapper.materialRelationShipsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.TaxonRdfMapper.taxonsToRdfModel
import fr.mnhn.datahub.connectors.es.model.{Collection, CollectionEvent, CollectionGroup, Coordinates, Domain, Geolocation, Identification, Institution, Material, MaterialRelationship, Taxon}
import fr.mnhn.datahub.connectors.es.utils.BuConfig
import fr.mnhn.datahub.connectors.rest.model.{BoldRecords, Digitalizations, SequencesGenBank, Specimens}
import fr.mnhn.datahub.connectors.rest.moleculaire.BoldRdfMapper.boldsToRdfModel
import fr.mnhn.datahub.connectors.rest.troisdtheque.DigitalizationsRdfMapper.digitalizationsToRdfModel
import fr.mnhn.datahub.connectors.rest.moleculaire.MoleculaireRdfMapper.moleculairesToRdfModel
import fr.mnhn.datahub.connectors.rest.moleculaire.GenBankRdfMapper.genbanksToRdfModel
import play.api.libs.json.Json
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

class RDFDumperSpec extends AnyFlatTestSpec {

  override def beforeAll(): Unit = {
    FakeDataInit.init()
    FakeCacheInit.init()
  }

  override def afterAll(): Unit = (FakeDataInit.shutdown())

  it should "dump rdf geolocation file in an repository" in {

    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.geolocationcacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.geolocationrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Geolocation](500000).runWith(rdfDumper.toFileDumper(dirLoc))
    Await.result(res, Duration.Inf)
  }
  
  it should "dump rdf material file in an repository" in {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.materialcacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.materialrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Material](500000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf collection file in an repository" in {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectioncacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Collection](100000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf collection-event file in an repository" in {
   // val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
   // implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionEventcacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionEventrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[CollectionEvent](500000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf Collection-Group file in an repository" in {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionGroupcacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.collectionGrouprdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[CollectionGroup](1000000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf coordinates.json file in an repository" in {
    //val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    //implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.coordinatescacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.coordinatesrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Coordinates](1000000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf domain file in an repository" in {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.domaincacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.domainrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Domain](1000000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf identification file in an repository" in {
    //val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    //implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.identificationcacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.identificationrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Identification](500000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf Institution file in an repository" in {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.institutioncacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.institutionrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Institution](100000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf MaterialRelationship file in an repository" in {
   // val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
   // implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.materialRelationshipcacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.materialRelationshiprdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[MaterialRelationship](100000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf taxon file in an repository" in {
   // val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
  //implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = ESConfiguration.prefix.getOrElse("")+BuConfig.taxoncacheDir
    val dirLoc = ESConfiguration.prefix.getOrElse("")+BuConfig.taxonrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Taxon](100000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf Digitalizations file in a repository" ignore  {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = s"data/troisdtheque/cache.db"
    val dirLoc = s"data/troisdtheque/rdf/"
    val mapper = new RdfMapper(cacheDir)
    mapper.init()

   val cacheContent = mapper.cacheExtractor[Digitalizations].runWith(Sink.seq)
   val sq =   Await.result(cacheContent, Duration.Inf)
    println(sq.toSeq.size)

    val res = mapper.process[Digitalizations](100000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }


  it should "dump rdf Moleculaire file in a repository" ignore  {
   // val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
   // implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = s"data/moleculaires/cache.db"
    val dirLoc = s"data/moleculaires/rdf/"
    val mapper = new RdfMapper(cacheDir)
    mapper.init()

    val cacheContent = mapper.cacheExtractor[Specimens].runWith(Sink.seq)
    val sq =   Await.result(cacheContent, Duration.Inf)
    println(sq.toSeq.size)
    sq.foreach(s => println(Json.prettyPrint(Json.toJson(s))) )



    val res = mapper.process[Specimens](100000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf bold file in repository" ignore  {
    val rdfDumper = new RdfDumper()
    val cacheDir = s"data/bold/cache.db"
    val dirLoc = s"data/bold/rdf/"
    val mapper = new RdfMapper(cacheDir)
    mapper.init()

    val cacheContent = mapper.cacheExtractor[BoldRecords].runWith(Sink.seq)
    val sq =   Await.result(cacheContent, Duration.Inf)
    println(sq.toSeq.size)
    sq.foreach(s => println(Json.prettyPrint(Json.toJson(s))) )

    val res = mapper.process[BoldRecords](100000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

  it should "dump rdf genbank file in repository" ignore  {
    val rdfDumper = new RdfDumper()
    val cacheDir = s"data/genbank/cache.db"
    val dirLoc = s"data/genbank/rdf/"
    val mapper = new RdfMapper(cacheDir)
    mapper.init()

    val cacheContent = mapper.cacheExtractor[SequencesGenBank].runWith(Sink.seq)
    val sq =   Await.result(cacheContent, Duration.Inf)
    println(sq.toSeq.size)
    sq.foreach(s => println(Json.prettyPrint(Json.toJson(s))) )


    val res = mapper.process[SequencesGenBank](100000).runWith(rdfDumper.toFileDumper(dirLoc))

    Await.result(res, Duration.Inf)
  }

}