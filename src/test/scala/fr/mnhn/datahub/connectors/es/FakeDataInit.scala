package fr.mnhn.datahub.connectors.es

import akka.actor.ActorSystem
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, ESMappingDefinitions}
import com.typesafe.scalalogging.LazyLogging
import fr.mnhn.datahub.connectors.es.mapping.{Collection, Collection_event, Collection_group, Coordinates, Domain, Geolocation, Identification, Institution, Material, MaterialRelationship, Taxon}
import play.api.libs.json.{JsArray, JsObject, JsValue, Json}

import java.io.File
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.io.Source


object FakeDataInit extends LazyLogging {

  val path = getClass.getResource("/es_data")
  val folder = new File(path.getPath)
  if (folder.exists && folder.isDirectory)
    folder.listFiles
      .toList
      .foreach(file => {
        val ESIndexable = parseJsonFile(file)
        logger.info(s"The following file ${file.getName} contains ${ESIndexable.size} json")
      })

  def getFileNameWithoutExtension(file: File): String = {
    val name = file.getName
    val dotIndex = name.lastIndexOf(".")
    if (dotIndex > 0) name.substring(0, dotIndex) else name
  }

  // Function to parse each JSON line, extract entityId, and convert to ESIndexable
  def parseJsonFileByLine(file: File): Seq[ESIndexable] = {
    val source = Source.fromFile(file)
    try {
      source.getLines().flatMap { line =>
        val json = Json.parse(line)

        // Extract entityId and convert the entire JSON to JsObject
        (json \ "entityId").asOpt[String].map { entityId =>
          ESIndexable(id = entityId, source = Some(json.as[JsObject]))
        }
      }.toSeq
    } finally {
      source.close()
    }
  }

  /*
  def parseJsonFile(file: File, idField: Option[String] = Some("entityId")): Seq[ESIndexable] = {
    val source = Source.fromFile(file)
    try {
      // Read the entire content of the file as a single string
      val fileContent = source.mkString

      // Parse the entire content as a JSON array or object
      val json = Json.parse(fileContent)

      // If the file contains a JSON array, you can directly map it to a sequence
      // If it's an object, you may need to wrap it in a Seq for consistency
      (json.asOpt[Seq[JsObject]].getOrElse(Seq(json.as[JsObject]))).flatMap { jsonObj =>
          // Extract entityId and convert the entire JSON to JsObject
          (jsonObj \ s"${idField.get}").asOpt[String].map { entityId =>
            println(file.getName + " - " + entityId)
            ESIndexable(id = entityId, source = Some(jsonObj))
          }
        }
    } finally {
      source.close()
    }
  }
   */

  def parseJsonFile(file: File, idField: Option[String] = Some("entityId")): Seq[ESIndexable] = {
    val source = Source.fromFile(file)
    try {
      // Read the entire content of the file as a single string
      val fileContent = source.mkString

      // Parse the JSON content
      val json = Json.parse(fileContent)

      // A recursive function to extract all objects containing the specified `idField`
      def extractEntities(json: JsValue): Seq[JsObject] = json match {
        case obj: JsObject =>
          // Check if the object has the `idField`
          if ((obj \ idField.get).isDefined) Seq(obj)
          else {
            // Recursively check for nested arrays or objects
            obj.fields.flatMap { case (_, value) => extractEntities(value) }.toSeq
          }
        case arr: JsArray =>
          // Recursively process each element in the array
          arr.value.flatMap(extractEntities).toSeq
        case _ =>
          Seq.empty // Ignore other JSON types
      }

      // Extract all matching objects
      val extractedObjects = extractEntities(json)

      // Map each object to an ESIndexable
      extractedObjects.flatMap { obj =>
        (obj \ idField.get).asOpt[String].map { entityId =>
          ESIndexable(id = entityId, source = Some(obj))
        }
      }
    } finally {
      source.close()
    }
  }


  implicit val actorSystem = ActorSystem("FakeDataESIndexer")
  implicit val ec = actorSystem.dispatcher

  val settings = Map[String, Any](
    "analysis.filter.autocomplete_filter.type" -> "edge_ngram",
    "analysis.filter.autocomplete_filter.min_gram" -> 3,
    "analysis.filter.autocomplete_filter.max_gram" -> 30,

    "analysis.analyzer.autocomplete.type" -> "custom",
    "analysis.analyzer.autocomplete.tokenizer" -> "standard",
    "analysis.analyzer.autocomplete.filter" -> Array("lowercase", "asciifolding", "autocomplete_filter"),

    "analysis.normalizer.lowercase_no_accent.type" -> "custom",
    "analysis.normalizer.lowercase_no_accent.filter" -> Array("lowercase", "asciifolding"),

    "number_of_replicas" -> ESConfiguration.defaultReplicas,
    "refresh_interval" -> ESConfiguration.defaultRefreshRate,
    "max_result_window" -> ESConfiguration.maxResultWindow,
    "mapping.total_fields.limit" -> ESConfiguration.mappingTotalFieldsLimit,
    "mapping.depth.limit" -> ESConfiguration.mappingDepthLimit,
    "mapping.nested_fields.limit" -> ESConfiguration.mappingNestedFieldsLimit
  )

  val settings_search: Map[String, Serializable] = Map(
    "analysis.analyzer.custom_asciifolding.type" -> "custom",
    "analysis.analyzer.custom_asciifolding.tokenizer" -> "standard",
    "analysis.analyzer.custom_asciifolding.filter" -> Array("lowercase", "my_ascii_folding"),
    "analysis.filter.my_ascii_folding.type" -> "asciifolding",
    "analysis.filter.my_ascii_folding.preserve_original" -> "true",
    "analysis.normalizer.custom_normalizer.type" -> "custom",
    "analysis.normalizer.custom_normalizer.filter" -> Array("asciifolding", "lowercase")
  )

  val taxon_idx_name= (s"${ESConfiguration.prefix.getOrElse("")}taxon")
  val material_idx_name = (s"${ESConfiguration.prefix.getOrElse("")}material")
  val identification_idx_name = (s"${ESConfiguration.prefix.getOrElse("")}identification")
  val geolocation_idx_name: String = (s"${ESConfiguration.prefix.getOrElse("")}geolocation")
  val collection_event_idx_name: String = (s"${ESConfiguration.prefix.getOrElse("")}collection-event")
  val coordinates_idx_name: String = (s"${ESConfiguration.prefix.getOrElse("")}coordinates")

  val collection_idx_name: String = (s"${ESConfiguration.prefix.getOrElse("")}collection")
  val collection_group_idx_name: String = (s"${ESConfiguration.prefix.getOrElse("")}collection-group")
  val domain_idx_name = (s"${ESConfiguration.prefix.getOrElse("")}domain")

  val institution_idx_name = (s"${ESConfiguration.prefix.getOrElse("")}institution")
  val material_relationship_idx_name = (s"${ESConfiguration.prefix.getOrElse("")}material-relationship")


  def deleteIndex(idx:Seq[String]) = {
    IndexClient.deleteIndices(idx)
  }

  def createIndex(indexName:String, mapping: String, settings: Map[String, Any]) = {
    IndexClient.indexExists(indexName).flatMap { result =>
      if (result.status == 200) {
        IndexClient.deleteIndices(Seq(indexName)).flatMap { delete =>
          if (delete.isSuccess) {
            IndexClient.createIndex(indexName = indexName, settings, ESMappingDefinitions(Json.parse(mapping)).toMappingDefinition())
          }
          else throw delete.error.asException
        }
      }
      else {
        IndexClient.createIndex(indexName = indexName, settings, ESMappingDefinitions(Json.parse(mapping)).toMappingDefinition())
      }
    }
  }

  def init(): Unit = {
    IndexClient.init()
    Await.result( createIndex(taxon_idx_name, Taxon.mapping, settings), 5.seconds)
    Await.result( createIndex(material_idx_name, Material.mapping, settings), 5.seconds)
    Await.result( createIndex(identification_idx_name,  Identification.mapping, settings), 5.seconds)


    Await.result(createIndex(geolocation_idx_name, Geolocation.mapping, settings), 5.seconds)
    Await.result(createIndex(collection_event_idx_name, Collection_event.mapping, settings), 5.seconds)
    Await.result(createIndex(coordinates_idx_name, Coordinates.mapping, settings), 5.seconds)
    Await.result(createIndex(geolocation_idx_name, Geolocation.mapping, settings), 5.seconds)


    Await.result(createIndex(collection_idx_name, Collection.mapping, settings), 5.seconds)
    Await.result(createIndex(collection_group_idx_name, Collection_group.mapping, settings), 5.seconds)
    Await.result(createIndex(domain_idx_name, Domain.mapping, settings), 5.seconds)

    Await.result(createIndex(institution_idx_name, Institution.mapping, settings), 5.seconds)
    Await.result(createIndex(material_relationship_idx_name, MaterialRelationship.mapping, settings), 5.seconds)




    Thread.sleep(5000)

    val path = getClass.getResource("/es_data")
    val folder = new File(path.getPath)
    if (folder.exists && folder.isDirectory)
      folder.listFiles
        .toList
        .foreach(file => {
          val esindexables = parseJsonFile(file)
          val index_name = getFileNameWithoutExtension(file).replaceAll("_", "-")
          Await.result( IndexClient.bulkInsert(ESConfiguration.prefix.getOrElse("") + index_name, esindexables: _*), 5.seconds)
        })
  }

  def shutdown() = {
    Await.result(deleteIndex(
      Seq(taxon_idx_name,
        material_idx_name,
        identification_idx_name,
        collection_event_idx_name,
        coordinates_idx_name,
        geolocation_idx_name,
        collection_idx_name,
        collection_group_idx_name,
        domain_idx_name,
        institution_idx_name,
        material_relationship_idx_name
      )
    ), 5.seconds)
  }
}


