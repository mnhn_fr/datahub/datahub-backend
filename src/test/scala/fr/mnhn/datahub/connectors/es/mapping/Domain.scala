package fr.mnhn.datahub.connectors.es.mapping

object Domain {

  val mapping =""" {
                 |    "properties": {
                 |      "code": { "type": "keyword" },
                 |      "created_at": { "type": "date", "format": "strict_date_time" },
                 |      "description": { "type": "text", "store": true },
                 |      "domain_id": { "type": "integer" },
                 |      "label": { "type": "text", "store": true },
                 |    "entityId": {  "type": "keyword"  },
                 |      "name": { "type": "text", "store": true }
                 |    }
                 |  }""".stripMargin

}
