/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.stream.alpakka.slick.scaladsl.SlickSession
import com.mnemotix.sql.{GraphManagementRepository, Insertable}
import com.mnemotix.sql.model.Subgraph
import fr.mnhn.datahub.connectors.AnyFlatTestSpec
import fr.mnhn.datahub.connectors.es.utils.BuConfig
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import fr.mnhn.datahub.connectors.es.model.Collection

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class SqlDumperSpec extends AnyFlatTestSpec {

  val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
  implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)
  val graphManagementRepository =  new GraphManagementRepository()

  it should "dump collection to SLQ Database" ignore {
    import fr.mnhn.datahub.connectors.es.helper.CollectionRdfMapper.collectionToSubgraph
    import graphManagementRepository.subGraphInsertable


    val cacheDir = BuConfig.collectioncacheDir
    val sqlDumper = new SqlDumper(cacheDir)
    sqlDumper.init()
    val result = sqlDumper.dumpSubgraph[Collection]
    Await.result(result, Duration.Inf)
  }

  it should "retrieve the same number of collection in the sql database that is in the cache" ignore {
    val rs = graphManagementRepository.subGraphTableSize()
    val tableSize =  Await.result(session.db.run(rs), Duration.Inf)

    val cacheDir = BuConfig.collectioncacheDir
    val sqlDumper = new SqlDumper(cacheDir)
    sqlDumper.cache.init()
    val cacheSize =  sqlDumper.cache.iterator().size

    tableSize shouldEqual cacheSize
  }
}

