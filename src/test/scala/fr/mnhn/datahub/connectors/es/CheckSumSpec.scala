/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.stream.alpakka.slick.javadsl.SlickSession
import akka.stream.scaladsl.Sink
import fr.mnhn.datahub.connectors.AnyFlatTestSpec
import fr.mnhn.datahub.connectors.es.model.Geolocation
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile
import fr.mnhn.datahub.connectors.es.helper.GeolocationRdfMapper.geolocationsToRdfModel
import fr.mnhn.datahub.connectors.es.utils.BuConfig

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class CheckSumSpec extends AnyFlatTestSpec {

  it should "produce the same hash for the same entry of the cache" ignore {
    val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
    implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

    val rdfDumper = new RdfDumper()
    val cacheDir = BuConfig.geolocationcacheDir
    val dirLoc = BuConfig.geolocationrdfDir
    val mapper = new RdfMapper(cacheDir)
    mapper.init()
    val res = mapper.process[Geolocation](1).take(1).runWith(Sink.seq)
    Await.result(res, Duration.Inf)
  }
}