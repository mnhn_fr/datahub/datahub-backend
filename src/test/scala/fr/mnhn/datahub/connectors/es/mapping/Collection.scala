package fr.mnhn.datahub.connectors.es.mapping

object Collection {
val mapping = """{
                |    "properties": {
                |      "attributes": {
                |        "type": "object",
                |        "properties": {
                |          "continent": { "type": "keyword" },
                |          "country": { "type": "keyword" },
                |          "is_deleted": { "type": "boolean" },
                |          "county": { "type": "keyword" },
                |          "first_administrative_level": { "type": "keyword" },
                |          "island": { "type": "keyword" },
                |          "island_group": { "type": "keyword" },
                |          "locality": { "type": "keyword" },
                |          "municipality": { "type": "keyword" },
                |          "ocean": { "type": "keyword" },
                |          "original_name": { "type": "text", "store": true },
                |          "remarks": { "type": "text", "store": true },
                |          "sea": { "type": "keyword" },
                |          "second_administrative_level": { "type": "keyword" },
                |          "state_province": { "type": "keyword" },
                |          "verbatim_country": { "type": "keyword" },
                |          "verbatim_locality": { "type": "text", "store": true }
                |        }
                |      },
                |      "code": { "type": "keyword" },
                |      "collection_group_id": { "type": "integer" },
                |      "collection_id": { "type": "integer" },
                |      "created_at": { "type": "date", "format": "strict_date_time" },
                |      "description": { "type": "text", "store": true },
                |      "institution_id": { "type": "integer" },
                |      "label": { "type": "text", "store": true },
                |      "name": { "type": "text", "store": true },
                |      "entityId": {  "type": "keyword"  }
                |    }
                |  }""".stripMargin
}
