package fr.mnhn.datahub.connectors.es.mapping

object MaterialRelationship {

  def mapping = """{
                  |    "properties": {
                  |      "attributes": {
                  |        "type": "object",
                  |        "properties": {
                  |          "collected_by": { "type": "text" },
                  |          "data_origin": { "type": "text" },
                  |          "duplicate_count": { "type": "integer" },
                  |          "duplicate_destination": { "type": "text" },
                  |          "event_date": { "type": "date" },
                  |          "event_date_begin": { "type": "date" },
                  |          "event_date_end": { "type": "date" },
                  |          "event_date_verbatim": { "type": "text" },
                  |          "field_number": { "type": "text" },
                  |          "harvest_identifier": { "type": "text" },
                  |          "host": { "type": "text" },
                  |          "old_harvest_identifier": { "type": "text" },
                  |          "part_count": { "type": "integer" },
                  |          "remarks": { "type": "text" },
                  |          "station_number": { "type": "text" },
                  |          "usage": { "type": "text" }
                  |        }
                  |      },
                  |      "code": { "type": "keyword" },
                  |      "description": { "type": "text" },
                  |      "label": { "type": "text" },
                  |      "name": { "type": "text" },
                  |      "object_id": { "type": "keyword" },
                  |      "subject_id": { "type": "keyword" },
                  |      "type": { "type": "keyword" },
                  |      "entityId": {  "type": "keyword"  }
                  |    }
                  |  }""".stripMargin
}