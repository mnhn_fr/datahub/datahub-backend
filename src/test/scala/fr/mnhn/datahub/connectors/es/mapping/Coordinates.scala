package fr.mnhn.datahub.connectors.es.mapping

object Coordinates {
  val mapping = """{
                  |  "properties": {
                  |    "attributes": {
                  |      "type": "object",
                  |      "properties": {
                  |        "altitude": {
                  |          "type": "float"
                  |        },
                  |        "altitude_accuracy": {
                  |          "type": "keyword"
                  |        },
                  |        "altitude_max": {
                  |          "type": "float"
                  |        },
                  |        "altitude_min": {
                  |          "type": "float"
                  |        },
                  |        "altitude_remarks": {
                  |          "type": "float"
                  |        },
                  |        "altitude_unit": {
                  |          "type": "keyword"
                  |        },
                  |        "coordinates_accuracy": {
                  |          "type": "keyword"
                  |        },
                  |        "coordinates_estimated": {
                  |          "type": "boolean"
                  |        },
                  |        "coordinates_geojson": {
                  |          "type": "geo_shape"
                  |        },
                  |        "coordinates_source": {
                  |          "type": "keyword"
                  |        },
                  |        "coordinates_verbatim": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "depth": {
                  |          "type": "float"
                  |        },
                  |        "depth_max": {
                  |          "type": "float"
                  |        },
                  |        "depth_min": {
                  |          "type": "float"
                  |        },
                  |        "latitude": {
                  |          "type": "float"
                  |        },
                  |        "latitude_degrees": {
                  |          "type": "float"
                  |        },
                  |        "latitude_minutes": {
                  |          "type": "float"
                  |        },
                  |        "latitude_orientation": {
                  |          "type": "keyword"
                  |        },
                  |        "latitude_seconds": {
                  |          "type": "float"
                  |        },
                  |        "longitude": {
                  |          "type": "float"
                  |        },
                  |        "longitude_degrees": {
                  |          "type": "float"
                  |        },
                  |        "longitude_minutes": {
                  |          "type": "float"
                  |        },
                  |        "longitude_orientation": {
                  |          "type": "keyword"
                  |        },
                  |        "longitude_seconds": {
                  |          "type": "float"
                  |        }
                  |      }
                  |    },
                  |    "code": {
                  |      "type": "keyword"
                  |    },
                  |    "coordinates_id": {
                  |      "type": "keyword"
                  |    },
                  |    "entityId": {
                  |      "type": "keyword"
                  |    },
                  |    "created_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "description": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "is_deleted": {
                  |      "type": "boolean"
                  |    },
                  |    "label": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "last_modified_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "name": {
                  |      "type": "text",
                  |      "store": true
                  |    }
                  |  }
                  |}""".stripMargin
}
