package fr.mnhn.datahub.connectors.es

import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.connectors.es.utils.BuConfig

import java.nio.file.{Files, Path, Paths}
import java.io.IOException
object FakeCacheInit {

  def createDirectoryIfNotExists(directoryPath: String): Unit = {
    // Concatenate the directory path

    try {
      // Create the directory if it doesn't exist
      val path: Path = Paths.get(directoryPath)
      if (!Files.exists(path)) {
        Files.createDirectories(path)
        println(s"Directory created at: $directoryPath")
      } else {
        println(s"Directory already exists: $directoryPath")
      }
    } catch {
      case e: IOException =>
        println(s"Error creating directory: ${e.getMessage}")
    }
  }

  def init() = {
    Seq(ESConfiguration.prefix.getOrElse("")+BuConfig.collectioncacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.collectionEventcacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.collectionGroupcacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.coordinatescacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.domaincacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.geolocationcacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.identificationcacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.institutioncacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.materialcacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.materialRelationshipcacheDir,
      ESConfiguration.prefix.getOrElse("")+BuConfig.taxoncacheDir,

        ESConfiguration.prefix.getOrElse("")+BuConfig.collectionrdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.collectionEventrdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.collectionGrouprdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.coordinatesrdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.domainrdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.geolocationrdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.identificationrdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.institutionrdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.materialrdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.materialRelationshiprdfDir,
    ESConfiguration.prefix.getOrElse("")+BuConfig.taxonrdfDir

    ).foreach(createDirectoryIfNotExists(_))
  }
}