package fr.mnhn.datahub.connectors.es.mapping

object Taxon {
  val mapping = """{
                  |  "properties": {
                  |    "attributes": {
                  |     "type": "object",
                  |      "properties": {
                  |        "authors": {
                  |          "type": "keyword"
                  |        },
                  |        "authors_infrasp": {
                  |          "type": "keyword"
                  |        },
                  |        "authors_infrasp_nominal_taxon": {
                  |          "type": "keyword"
                  |        },
                  |        "authors_nominal_taxon": {
                  |          "type": "keyword"
                  |        },
                  |        "date_taxon": {
                  |          "type": "date",
                  |          "format": "strict_date"
                  |        },
                  |        "num_family": {
                  |          "type": "integer"
                  |        },
                  |        "num_family_apg": {
                  |          "type": "integer"
                  |        },
                  |        "num_genus": {
                  |          "type": "integer"
                  |        },
                  |        "remarks": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "scientific_name_authorship": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "scientific_name_with_authorship": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "scientific_name_without_authorship": {
                  |          "type": "text",
                  |          "store": true
                  |        }
                  |      }
                  |    },
                  |    "code": {
                  |      "type": "keyword"
                  |    },
                  |    "created_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "description": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "is_deleted": {
                  |      "type": "boolean"
                  |    },
                  |    "deleted_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "label": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "last_modified_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "name": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "ranks": {
                  |     "type": "object",
                  |      "properties": {
                  |        "class": {
                  |          "type": "keyword"
                  |        },
                  |        "code": {
                  |          "type": "keyword"
                  |        },
                  |        "family": {
                  |          "type": "keyword"
                  |        },
                  |        "genus": {
                  |          "type": "keyword"
                  |        },
                  |        "infra_class": {
                  |          "type": "keyword"
                  |        },
                  |        "infra_order": {
                  |          "type": "keyword"
                  |        },
                  |        "infra_specific_epithet": {
                  |          "type": "keyword"
                  |        },
                  |        "kingdom": {
                  |          "type": "keyword"
                  |        },
                  |        "level": {
                  |          "type": "keyword"
                  |        },
                  |        "name": {
                  |          "type": "keyword"
                  |        },
                  |        "order": {
                  |          "type": "keyword"
                  |        },
                  |        "phylum": {
                  |          "type": "keyword"
                  |        },
                  |        "species": {
                  |          "type": "keyword"
                  |        },
                  |        "specific_epithet": {
                  |          "type": "keyword"
                  |        },
                  |        "rank_id": {
                  |          "type": "keyword"
                  |        },
                  |        "sub_class": {
                  |          "type": "keyword"
                  |        },
                  |        "sub_family": {
                  |          "type": "keyword"
                  |        },
                  |        "sub_genus": {
                  |          "type": "keyword"
                  |        },
                  |        "sub_order": {
                  |          "type": "keyword"
                  |        },
                  |        "sub_phylum": {
                  |          "type": "keyword"
                  |        },
                  |        "super_family": {
                  |          "type": "keyword"
                  |        },
                  |        "super_order": {
                  |          "type": "keyword"
                  |        }
                  |      }
                  |    },
                  |    "taxon_id": {
                  |      "type": "keyword"
                  |    },
                  |    "id": {
                  |      "type": "keyword"
                  |    },
                  |    "entityId": {
                  |      "type": "keyword"
                  |    },
                  |    "graphUri": {
                  |      "type": "keyword"
                  |    },
                  |    "resourceUri": {
                  |      "type": "keyword"
                  |    },
                  |    "datatype": {
                  |      "type": "keyword"
                  |    },
                  |    "entityId": {  "type": "keyword"  }
                  |  }
                  |}""".stripMargin

}
