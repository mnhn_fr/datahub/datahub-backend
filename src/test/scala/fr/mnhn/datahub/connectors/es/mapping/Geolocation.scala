package fr.mnhn.datahub.connectors.es.mapping

object Geolocation {
  val mapping = """{
                  |  "properties": {
                  |    "attributes": {
                  |      "type": "object",
                  |      "properties": {
                  |        "continent": {
                  |          "type": "keyword"
                  |        },
                  |        "country": {
                  |          "type": "keyword"
                  |        },
                  |        "country_code": {
                  |          "type": "keyword"
                  |        },
                  |        "county": {
                  |          "type": "keyword"
                  |        },
                  |        "first_administrative_level": {
                  |          "type": "keyword"
                  |        },
                  |        "island": {
                  |          "type": "keyword"
                  |        },
                  |        "island_group": {
                  |          "type": "keyword"
                  |        },
                  |        "locality": {
                  |          "type": "keyword"
                  |        },
                  |        "municipality": {
                  |          "type": "keyword"
                  |        },
                  |        "ocean": {
                  |          "type": "keyword"
                  |        },
                  |        "original_name": {
                  |          "type": "keyword"
                  |        },
                  |        "remarks": {
                  |          "type": "text",
                  |          "store": true
                  |        },
                  |        "sea": {
                  |          "type": "keyword"
                  |        },
                  |        "second_administrative_level": {
                  |          "type": "keyword"
                  |        },
                  |        "state_province": {
                  |          "type": "keyword"
                  |        },
                  |        "verbatim_country": {
                  |          "type": "keyword"
                  |        },
                  |        "verbatim_locality": {
                  |          "type": "keyword"
                  |        }
                  |      }
                  |    },
                  |    "code": {
                  |      "type": "keyword"
                  |    },
                  |    "created_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "deleted_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "description": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "geolocation_id": {
                  |      "type": "keyword"
                  |    },
                  |    "entityId": {
                  |      "type": "keyword"
                  |    },
                  |    "is_deleted": {
                  |      "type": "boolean"
                  |    },
                  |    "label": {
                  |      "type": "text",
                  |      "store": true
                  |    },
                  |    "last_modified_at": {
                  |      "type": "date",
                  |      "format": "strict_date_time"
                  |    },
                  |    "name": {
                  |      "type": "text",
                  |      "store": true
                  |    }
                  |  }
                  |}""".stripMargin
}

