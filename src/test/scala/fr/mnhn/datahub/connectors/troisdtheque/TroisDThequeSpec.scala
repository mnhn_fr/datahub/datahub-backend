/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.troisdtheque

import com.mnemotix.synaptix.cache.RocksDBStore
import fr.mnhn.datahub.connectors.rest.model.{Digitalizations, DigitalizationsRootInterface}
import fr.mnhn.datahub.connectors.{AnyFlatTestSpec, GenericRestClient}
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class TroisDThequeSpec extends AnyFlatTestSpec {

 it should "get the digitalizations in 3DThèque MNHN API" in {
   val store = new RocksDBStore(s"data/troisdtheque/cache.db")
   store.init()
   val restClient = new GenericRestClient(store)
   val fut = restClient.getAsMany[DigitalizationsRootInterface](1000, "http://datahub-test-2.arzt.mnhn.fr:8096", "digitalizations")
   val nbr = Await.result(fut, Duration.Inf)
   println(nbr)
   store.shutdown()
   nbr should be > 0
  }

  it should "get the value in store of data/troisdtheque/cache.db" in {
    val store = new RocksDBStore(s"data/troisdtheque/cache.db")
    store.init()
    store.iterator().iterator.foreach { kv =>
      val s = new String(kv.value, "UTF-8")
      println(s)
      val e = Json.parse(s).as[Digitalizations]
      println(e)
    }
  }

}