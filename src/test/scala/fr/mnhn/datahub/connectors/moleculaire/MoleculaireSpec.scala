/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.moleculaire

import com.mnemotix.synaptix.cache.RocksDBStore
import fr.mnhn.datahub.connectors.MnhnEndpointResultConverter.{BoldRootInterfaceConverter, GenBankConverter}
import fr.mnhn.datahub.connectors.rest.model.{BoldRootInterface, Digitalizations, GenBankRootInterface, SpecimenRootInterface, Specimens}
import fr.mnhn.datahub.connectors.{AnyFlatTestSpec, GenericRestClient}
import play.api.libs.json.Json

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}

class MoleculaireSpec extends AnyFlatTestSpec {

  it should "get all data from the moleculaire rest client" in {
    val store = new RocksDBStore(s"data/moleculaires/cache2.db")
    store.init()
    val restClient = new GenericRestClient(store)
    val fut = restClient.getAsMany[SpecimenRootInterface](1000, "http://datahub-test-2.arzt.mnhn.fr:8092", "specimens")
    val nbr = Await.result(fut, Duration.Inf)
    println(nbr)
    store.shutdown()
    nbr should be > 0
  }

  it should "get the value in store of data/moleculaires/cache.db" in {
    val store = new RocksDBStore(s"data/moleculaires/cache2.db")

    store.init()
    store.iterator().iterator.foreach { kv =>
      val s = new String(kv.value, "UTF-8")
      val e = Json.parse(s).as[Specimens]
      println(e.id)
    }
  }

  it should "get all data from the moleculaire cache and get the bold" in {
    val store = new RocksDBStore(s"data/moleculaires/cache.db")
    val boldStore = new RocksDBStore(s"data/bold/cache.db")
    val restClient = new GenericRestClient(boldStore)

    store.init()
    boldStore.init()
    store.iterator().iterator.foreach { kv =>
      val s = new String(kv.value, "UTF-8")
      val specimen = Json.parse(s).as[Specimens]
      println(specimen)
      val linkBold= specimen._links.flatMap { links =>
        links.bold.flatMap { bold =>
          bold.href.map(_.toString)
        }
      }
      val future = linkBold.map { documentUrl =>
        println(documentUrl)
        restClient.getDocument[BoldRootInterface](documentUrl, specimen.id.get)
      }
      if (future.isDefined) {
        Await.result(future.get, 20.seconds)
      }
      else Seq.empty
    }
  }

  it should "get all data from the moleculaire cache and get the GenBank" in {
    val store = new RocksDBStore(s"data/moleculaires/cache2.db")
    val genbankStore = new RocksDBStore(s"data/genbank/cache.db")
    val restClient = new GenericRestClient(genbankStore)
    val j = new AtomicInteger()
    store.init()
    genbankStore.init()
    store.iterator().iterator.foreach { kv =>
      val nj = j.incrementAndGet()
      if (nj % 1000 == 0) logger.info(s"process $nj")
      val s = new String(kv.value, "UTF-8")
      val specimen = Json.parse(s).as[Specimens]
      val linkgb= specimen._links.flatMap { links =>
        links.sequences.flatMap { gb =>
          //println(gb)
          gb.href.map(_.toString)
        }
      }
      val future = linkgb.map { documentUrl =>
        val parsed = documentUrl.replace("{&sampleId}", "")
        //println(parsed)
        restClient.getDocument[GenBankRootInterface](parsed, specimen.id.get)
      }
      if (future.isDefined) {
        Await.result(future.get, 20.seconds)
      }
      else Seq.empty
    }
  }
}