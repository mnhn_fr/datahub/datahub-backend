package fr.mnhn.datahub.connectors.graphdb

import fr.mnhn.datahub.connectors.AnyFlatTestSpec

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}

class GdbConnectorSpec extends AnyFlatTestSpec {

 it should "index collection" in {
  val collectionConnector = new CollectionConnector()
  collectionConnector.init()
 val rs = collectionConnector.reload()
 Await.result(rs, Duration.Inf)
  collectionConnector.shutdown()
  Thread.sleep(5000)
 }

 it should "index collectionEvent" in {
  val collectionEventConnector = new CollectionEventConnector()
  collectionEventConnector.init()
  println(collectionEventConnector.getConnectorRequest)
 // val rs = collectionEventConnector.reload()
  //Await.result(rs, Duration.Inf)
  //collectionEventConnector.shutdown()
  Thread.sleep(5000)

 }

 it should "index collectionItem" in {
  val collectionItemConnector = new CollectionItemConnector()
  collectionItemConnector.init()
  val rs = collectionItemConnector.reload()
  Await.result(rs, 5.minutes)
  //collectionItemConnector.shutdown()
  Thread.sleep(5000)

 }

 it should "index geolocation" in {
  val geolocationConnector = new GeolocationConnector()
  geolocationConnector.init()
  val rs = geolocationConnector.reload()
  Await.result(rs, Duration.Inf)
  //geolocationConnector.shutdown()
  Thread.sleep(5000)

 }

 it should "index geometry" in {
  val geometryConnector = new GeometryConnector()
  geometryConnector.init()
  val rs = geometryConnector.reload()
  Await.result(rs, Duration.Inf)
  //geometryConnector.shutdown()
  Thread.sleep(5000)

 }

 it should "index identification" in {
  val identificationConnector = new IdentificationConnector()
  identificationConnector.init()
  println(identificationConnector.getConnectorRequest)

  val rs = identificationConnector.reload()
  Await.result(rs, Duration.Inf)
 // identificationConnector.shutdown()
  Thread.sleep(5000)

 }

 it should "index material" in {
  val materialConnector = new MaterialConnector()
  materialConnector.init()
  println(materialConnector.getConnectorRequest)
  val rs = materialConnector.reload()
  Await.result(rs, Duration.Inf)
  //materialConnector.shutdown()
  Thread.sleep(5000)

 }

 it should "index person" in {
  val personConnector = new PersonConnector()
  personConnector.init()
  //val rs = personConnector.reload()
  //personConnector.getConnectorRequest
  println(personConnector.getConnectorRequest)

  //Await.result(rs, 5.minutes)
  //personConnector.shutdown()
  //Thread.sleep(5000)

 }

 it should "index taxon" in {
  val taxonConnector = new TaxonConnector()
  taxonConnector.init()
  val rs = taxonConnector.reload()
  Await.result(rs, Duration.Inf)
  //taxonConnector.shutdown()
  Thread.sleep(5000)

 }

 it should "index concept" in {
  val conceptConnector = new ConceptConnector()
  conceptConnector.init()
  val rs = conceptConnector.reload()
  Await.result(rs, Duration.Inf)
  //conceptConnector.shutdown()
  Thread.sleep(5000)
 }

 it should "index collection group" in {
  val collectionGroupConnector = new CollectionGroupConnector()
  collectionGroupConnector.init()
  val rs = collectionGroupConnector.reload()
  Await.result(rs, Duration.Inf)
  //collectionGroupConnector.shutdown()
  Thread.sleep(5000)
 }

 it should "index taxon Alignment" in {
  val taxonAlignmentConnector = new TaxonAlignmentConnector()
  taxonAlignmentConnector.init()
  val rs = taxonAlignmentConnector.reload()
  Await.result(rs, Duration.Inf)
  Thread.sleep(5000)
 }

 it should "index GenomicSequenceRecord" in {
  val genomicSequenceRecordConnector = new GenomicSequenceRecordConnector()
  println(genomicSequenceRecordConnector.getConnectorRequest)

  genomicSequenceRecordConnector.init()
 val rs = genomicSequenceRecordConnector.reload()
  Await.result(rs, Duration.Inf)
 // Thread.sleep(5000)
 }

 it should "index Digitalization" in {
  val digitalizationConnector = new DigitalizationConnector()
  println(digitalizationConnector.getConnectorRequest)

  //digitalizationConnector.reload()
  val rs = digitalizationConnector.reload()
  Await.result(rs, Duration.Inf)
 /* val rs = digitalizationConnector.reload()
  Await.result(rs, Duration.Inf)
  Thread.sleep(5000)

  */
 }

 it should "index TaxonAlignment for Search Index" in {
  val connector= new TaxonAlignmentMNHNSearchConnector()
  println(connector.getConnectorRequest)
  connector.init()
  val rs = connector.reload()
  Await.result(rs, Duration.Inf)

  val connector2 = new TaxonAlignmentTargetSearchConnector()
  println(connector2.getConnectorRequest)
  connector2.init()
  val rs2 = connector2.reload()
  Await.result(rs2, Duration.Inf)
 }

 it should "index Material for Search Index" in {
  val connector = new MaterialSearchConnector()
  println(connector.getConnectorRequest)
  connector.init()
  val rs = connector.reload()
  Await.result(rs, Duration.Inf)
 }

 it should "index Person for Search Index" in {
  val connector = new PersonSearch()
  println(connector.getConnectorRequest)
  connector.init()
  val rs = connector.reload()
  Await.result(rs, Duration.Inf)
 }

 it should "index Taxon Citation" in {
  val connector = new TaxonCitation()
  println(connector.getConnectorRequest)
  connector.init()
  val rs = connector.reload()
  Await.result(rs, Duration.Inf)
 }

 it should "index Person Alignment" in {
  val connector = new PersonAlignmentConnector()
  println(connector.getConnectorRequest)
  connector.init()
  val rs = connector.reload()
  Await.result(rs, Duration.Inf)
 }
}