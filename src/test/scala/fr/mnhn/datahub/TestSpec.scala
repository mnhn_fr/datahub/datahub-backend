package fr.mnhn.datahub

import akka.actor.ActorSystem
import akka.dispatch.ExecutionContexts
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 28/05/2020
 */

trait TestSpec extends AnyWordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with LazyLogging {

  override implicit val patienceConfig = PatienceConfig(50.minutes)
  implicit val system = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext =  system.dispatcher
//  override protected def afterAll(): Unit = system.terminate()

}
