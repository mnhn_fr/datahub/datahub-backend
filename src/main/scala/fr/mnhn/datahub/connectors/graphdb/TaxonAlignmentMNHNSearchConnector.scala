package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap
import org.eclipse.rdf4j.model.vocabulary.SKOS

import scala.concurrent.ExecutionContext

class TaxonAlignmentMNHNSearchConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "taxon-search"
  override val types = s"${nameSpaceMap.get("mnhn").get}TaxonAlignment"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false
  override val bulkUpdateBatchSize = 5000

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(

    EsGraphDBConnectorField("taxonId", Seq(s"${nameSpaceMap.get("mnhn").get}isAlignmentOfTaxon", s"${nameSpaceMap.get("dwc").get}taxonID"), STRING, indexed = false, stored = false, keyword = false, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("mnhnTaxonId", Seq(s"${nameSpaceMap.get("mnhn").get}isAlignmentOfTaxon", s"${nameSpaceMap.get("dwc").get}taxonID"), STRING, indexed = false, stored = false, keyword = false, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("scientificName", Seq(s"${nameSpaceMap.get("mnhn").get}isAlignmentOfTaxon", s"${nameSpaceMap.get("dwc").get}scientificName"), STRING, indexed = false, stored = false, keyword = false, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("source", Seq(s"${nameSpaceMap.get("mnhn").get}isAlignmentOfTaxon", s"${nameSpaceMap.get("mnhn").get}taxonSource"), STRING, indexed = false, stored = false, keyword = false, multivalued = false, analyzed = false)

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd
}


//mnhn:isAlignmentOfTaxon