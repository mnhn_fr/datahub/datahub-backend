/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{DATE, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap

import scala.concurrent.ExecutionContext

class PersonConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "person"
  override val types = s"${nameSpaceMap.get("mnhn").get}Person"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  //nameSpaceMap

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("lastName", Seq("http://xmlns.com/foaf/0.1/lastName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("firstName", Seq("http://xmlns.com/foaf/0.1/firstName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
//    EsGraphDBConnectorField("nick", Seq("http://xmlns.com/foaf/0.1/nick"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
//    EsGraphDBConnectorField("name", Seq("http://xmlns.com/foaf/0.1/name"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
//    EsGraphDBConnectorField("title", Seq("http://xmlns.com/foaf/0.1/title"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
//    EsGraphDBConnectorField("gender", Seq("http://xmlns.com/foaf/0.1/gender"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
//    EsGraphDBConnectorField("birthday", Seq("http://purl.org/vocab/bio/0.1/birth", "http://purl.org/vocab/bio/0.1/date"), DATE, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
//    EsGraphDBConnectorField("birthPlace", Seq("http://purl.org/vocab/bio/0.1/birth", "http://purl.org/vocab/bio/0.1/place"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
//    EsGraphDBConnectorField("deathday", Seq("http://purl.org/vocab/bio/0.1/death", "http://purl.org/vocab/bio/0.1/date"), DATE, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
//    EsGraphDBConnectorField("deathPlace", Seq("http://purl.org/vocab/bio/0.1/death", "http://purl.org/vocab/bio/0.1/place"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
//    EsGraphDBConnectorField("prefName", Seq(s"${nameSpaceMap.get("mnhn").get}prefName"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),

    // lien personne

    EsGraphDBConnectorField("knows", Seq("http://xmlns.com/foaf/0.1/knows"),  URI, indexed = true, stored = true, keyword = true),
//    EsGraphDBConnectorField("hasAlignment", Seq(s"${nameSpaceMap.get("mnx").get}hasAlignment"), URI, indexed = true, stored = true, keyword = true),

    // lien CollectionItem

    EsGraphDBConnectorField("isCreatorOf", Seq(s"${nameSpaceMap.get("mnhn").get}isCreatorOf"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("isAuthorOf", Seq(s"${nameSpaceMap.get("mnhn").get}isAuthorOf"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("isPublisherOf", Seq(s"${nameSpaceMap.get("mnhn").get}isPublisherOf"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("isScientificEditorOf", Seq(s"${nameSpaceMap.get("mnhn").get}isScientificEditorOf"),URI, indexed = true, stored = true, keyword = true),


    // other uri in LOD

     EsGraphDBConnectorField("idrefURI", Seq(s"${nameSpaceMap.get("mnhn").get}idrefURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("wikidataURI", Seq(s"${nameSpaceMap.get("mnhn").get}wikidataURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("viafURI", Seq(s"${nameSpaceMap.get("mnhn").get}viafURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("orcidURI", Seq(s"${nameSpaceMap.get("mnhn").get}orcidURI"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("bnfURI", Seq(s"${nameSpaceMap.get("mnhn").get}bnfURI"), URI, indexed = true, stored = true, keyword = true),
     EsGraphDBConnectorField("isniURI", Seq(s"${nameSpaceMap.get("mnhn").get}isniURI"), URI, indexed = true, stored = true, keyword = true),


    // lien taxon

    EsGraphDBConnectorField("authoredTaxon", Seq(s"${nameSpaceMap.get("mnhn").get}authoredTaxon"), URI, indexed = true, stored = true, keyword = true, multivalued = true),

    // lien Identification
    EsGraphDBConnectorField("identifierOf", Seq(s"${nameSpaceMap.get("mnhn").get}identifierOf"), URI, indexed = true, stored = true, keyword = true, multivalued = true),

    // lien CollectionEvent
    EsGraphDBConnectorField("recorderOf", Seq(s"${nameSpaceMap.get("mnhn").get}recorderOf"), URI, indexed = true, stored = true, keyword = true, multivalued = true),

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override val bulkUpdateBatchSize: Int =  10
}
