/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{BOOLEAN, DATE, INT, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap
import org.eclipse.rdf4j.model.vocabulary.{DCTERMS, FOAF, RDFS}

import scala.concurrent.ExecutionContext

class CollectionConnector(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "mnhn-collection"
  override val types = s"${nameSpaceMap.get("mnhn").get}Collection"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("collectionID", Seq(s"${nameSpaceMap.get("dwc").get}collectionID"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("label", Seq(s"${RDFS.NS}label"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("collectionCode", Seq(s"${nameSpaceMap.get("dwc").get}collectionCode"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("description", Seq(s"${DCTERMS.NS}description"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    // link with Organization
    EsGraphDBConnectorField("hasInstitution", Seq(s"${nameSpaceMap.get("mnhn").get}hasInsitution", s"${FOAF.NAMESPACE}name"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    // link with Group
    EsGraphDBConnectorField("hasCollectionGroup", Seq(s"${nameSpaceMap.get("mnhn").get}hasCollectionGroup"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false)
  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd
}