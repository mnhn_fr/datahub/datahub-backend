package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{DOUBLE, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap

import scala.concurrent.ExecutionContext

class PersonAlignmentConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "person-alignment"
  override val types = s"${nameSpaceMap.get("mnhn").get}PersonAlignment"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),

    EsGraphDBConnectorField("sourcePerson", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("sourcePersonLastName", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", "http://xmlns.com/foaf/0.1/lastName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("sourcePersonFirstName", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", "http://xmlns.com/foaf/0.1/firstName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("sourcePersonPrefName", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}prefName"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("sourcePersonIdrefURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s" ${nameSpaceMap.get("mnhn").get}idrefURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("sourcePersonWikidataURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson",s"${nameSpaceMap.get("mnhn").get}wikidataURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("sourcePersonViafURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}viafURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("sourcePersonOrcidURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}orcidURI"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("sourcePersonBnfURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}bnfURI"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("sourcePersonIsniURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}isniURI"), URI, indexed = true, stored = true, keyword = true),


    EsGraphDBConnectorField("targetPerson", Seq(s"${nameSpaceMap.get("mnhn").get}targetPerson"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("targetPersonLastName", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", "http://xmlns.com/foaf/0.1/lastName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("targetPersonFirstName", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", "http://xmlns.com/foaf/0.1/firstName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("targetPersonPrefName", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}prefName"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("targetPersonIdrefURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}idrefURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("targetPersonWikidataURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}wikidataURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("targetPersonViafURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}viafURI"),URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("targetPersonOrcidURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}orcidURI"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("targetPersonBnfURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}bnfURI"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("targetPersonIsniURI", Seq(s"${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}isniURI"), URI, indexed = true, stored = true, keyword = true),

    EsGraphDBConnectorField("confidenceScore", Seq(s"${nameSpaceMap.get("mnx").get}confidenceScore"),  DOUBLE, indexed = true, stored = true, keyword = false, multivalued = false, analyzed = false)
  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd
}