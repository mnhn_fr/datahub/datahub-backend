package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.GraphDBConnector

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object ConnectorHelper {

  def name() = {
    "collection \n collectionEvent \n collectionItem \n geolocation \n geometry \n identification \n material \n person \n taxon \n concept \n collectionGroup" +
      " \n taxonAlignment \n genomicSequenceRecord \n digitalization"
  }

  def reloadIndex(connector: GraphDBConnector)(implicit ec: ExecutionContext) = {
    connector.init()
    connector.reload().onComplete {
      case Failure(exception) => println("We have an exception " + exception.getMessage)
      case Success(value) => print(s"indexation in ${connector.indexName} finish with value = $value")
    }
  }

  def reloadList(connector: Seq[GraphDBConnector])(implicit ec: ExecutionContext) = {
    connector.grouped(4).foreach { groups =>
      groups.foreach(reloadIndex(_))
    }
  }

  def reloadAll()(implicit actorSystem: ActorSystem, executionContext: ExecutionContext): Seq[GraphDBConnector] = {
    Seq(
      new CollectionGroupConnector,
      new CollectionConnector,
      new ConceptConnector,
      new DigitalizationConnector,
      new GenomicSequenceRecordConnector,
      new TaxonAlignmentConnector,
      new TaxonConnector,
      new PersonConnector,
      new CollectionEventConnector,
      new MaterialConnector,
      new IdentificationConnector,
      new MaterialConnector
    )
  }

  def reloadSingle(name: String)(implicit actorSystem: ActorSystem, executionContext: ExecutionContext): Unit = {
    name match {
      case "collection" => reloadIndex(new CollectionConnector)
      case "collectionEvent" => reloadIndex(new CollectionEventConnector)
      case "collectionItem" => reloadIndex(new CollectionItemConnector)
      case "geolocation" => reloadIndex(new GeolocationConnector)
      case "geometry" => reloadIndex(new GeometryConnector)
      case "identification" => reloadIndex(new IdentificationConnector)
      case "material" => reloadIndex(new MaterialConnector)
      case "person" => reloadIndex(new PersonConnector)
      case "taxon" => reloadIndex(new TaxonConnector)
      case "concept" => reloadIndex(new ConceptConnector)
      case "collectionGroup" => reloadIndex(new CollectionGroupConnector)
      case "taxonAlignment" => reloadIndex(new TaxonAlignmentConnector)
      case "genomicSequenceRecord" => reloadIndex(new GenomicSequenceRecordConnector)
      case "digitalization" => reloadIndex(new DigitalizationConnector)
      case _ => println(s"Invalid connector name: $name")
    }
  }

  def nameToConnectorConverter(name:String)(implicit actorSystem: ActorSystem, executionContext: ExecutionContext) = {

      name match {
        case "collection" => (new CollectionConnector)
        case "collectionEvent" => (new CollectionEventConnector)
        case "collectionItem" => (new CollectionItemConnector)
        case "geolocation" => (new GeolocationConnector)
        case "geometry" => (new GeometryConnector)
        case "identification" => (new IdentificationConnector)
        case "material" => (new MaterialConnector)
        case "person" => (new PersonConnector)
        case "taxon" => (new TaxonConnector)
        case "concept" => (new ConceptConnector)
        case "collectionGroup" => (new CollectionGroupConnector)
        case "taxonAlignment" => (new TaxonAlignmentConnector)
        case "genomicSequenceRecord" => (new GenomicSequenceRecordConnector)
        case "digitalization" => (new DigitalizationConnector)
        case _ => throw new Exception(s"Invalide connector name : $name")

    }
  }
}