package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{ConceptCommonFields, EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap
import org.eclipse.rdf4j.model.vocabulary.{DCTERMS, SKOS}

import scala.concurrent.ExecutionContext

class  ConceptConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "concept"
  override val types = s"${SKOS.CONCEPT}"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("hasAuthority", Seq(s"${nameSpaceMap.get("taxrefprop").get}hasAuthority"), STRING, indexed = true, stored = true, keyword = false, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("scientificName", Seq(s"${nameSpaceMap.get("dwc").get}scientificName"), STRING, indexed = true, stored = true, keyword = false, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("taxonID", Seq(s"${DCTERMS.NS}taxonID"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("taxonRank", Seq(s"${nameSpaceMap.get("dwc").get}taxonRank"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false), // skos taxrefrk:Species


    EsGraphDBConnectorField("identifier", Seq(s"${nameSpaceMap.get("dcterms").get}identifier"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("taxonRankLevel", Seq(s"${nameSpaceMap.get("mnhn").get}taxonRankLevel"), STRING, indexed = true, stored = true, keyword = false, multivalued = false, analyzed = false)
  ) ++ ConceptCommonFields.esGraphDBConnectorFields


  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd
}
