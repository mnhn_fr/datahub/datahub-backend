package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{INT, NESTED, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap

import scala.concurrent.ExecutionContext

class PersonSearch()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "person-search"
  override val types = s"${nameSpaceMap.get("mnhn").get}Person"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("lastName", Seq("http://xmlns.com/foaf/0.1/lastName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("firstName", Seq("http://xmlns.com/foaf/0.1/firstName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("name", Seq("http://xmlns.com/foaf/0.1/name"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("prefName", Seq(s"${nameSpaceMap.get("mnhn").get}prefName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true),

    //EsGraphDBConnectorField("targetPerson", Seq(s"^${nameSpaceMap.get("mnhn").get}sourcePerson", s"${nameSpaceMap.get("mnhn").get}targetPerson"), URI, indexed = true, stored = true, keyword = true, multivalued = true),
    EsGraphDBConnectorField("targetPerson", Seq(s"^${nameSpaceMap.get("mnhn").get}sourcePerson"),NESTED, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true, objectFields = Some(personAlignment())),

    )

  def personAlignment() = {
    Seq(
      EsGraphDBConnectorField("lastName", Seq(s"${nameSpaceMap.get("mnhn").get}targetPerson", "http://xmlns.com/foaf/0.1/lastName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
      EsGraphDBConnectorField("firstName", Seq(s"${nameSpaceMap.get("mnhn").get}targetPerson", "http://xmlns.com/foaf/0.1/firstName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
      EsGraphDBConnectorField("name", Seq(s"${nameSpaceMap.get("mnhn").get}targetPerson", "http://xmlns.com/foaf/0.1/name"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
      EsGraphDBConnectorField("prefName", Seq(s"${nameSpaceMap.get("mnhn").get}targetPerson", s"${nameSpaceMap.get("mnhn").get}prefName"), STRING, indexed = true, stored = true, keyword = true, multivalued = true),
      EsGraphDBConnectorField("targetPerson", Seq(s"${nameSpaceMap.get("mnhn").get}targetPerson"), URI, indexed = true, stored = true, keyword = true),
      EsGraphDBConnectorField("confidenceScore", Seq(s"${nameSpaceMap.get("mnx").get}confidenceScore"), INT, indexed = true, stored = true, keyword = true)
    )
  }

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd
}
