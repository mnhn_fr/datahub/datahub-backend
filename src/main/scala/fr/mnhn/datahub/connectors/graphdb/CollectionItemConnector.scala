/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{DATE, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap
import org.eclipse.rdf4j.model.vocabulary.{DCTERMS, FOAF, SKOS}

import scala.concurrent.ExecutionContext

class CollectionItemConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "collection-item"
  override val types = s"${nameSpaceMap.get("mnhn").get}CollectionItem"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  //nameSpaceMap

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(

    // CollectionItem
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("RDFTYPE", Seq("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = false),
    EsGraphDBConnectorField("date", Seq(s"${DCTERMS.NS}date"), DATE, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("format", Seq(s"${DCTERMS.NS}format"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("title", Seq(s"${DCTERMS.NS}title"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("topic", Seq(s"${DCTERMS.NS}subject"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("description", Seq(s"${DCTERMS.NS}description"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("note", Seq(s"${SKOS.NS}note"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("subject", Seq(s"${DCTERMS.NS}subject"), URI, indexed = true, stored = true, keyword = true), // link with Concept and Taxon
    EsGraphDBConnectorField("references", Seq(s"${DCTERMS.NS}references"), URI, indexed = true, stored = true, keyword = true), // link with Material


    //Document
    EsGraphDBConnectorField("publisherName", Seq(s"${DCTERMS.NS}publisher"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),

    // ArchiveItem

    EsGraphDBConnectorField("originalInfo", Seq(s"${nameSpaceMap.get("mnhn").get}originalInfo"), STRING, indexed = true, stored = true, keyword = false, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("acquisitionInfo", Seq(s"${nameSpaceMap.get("mnhn").get}acquisitionInfo"), STRING, indexed = true, stored = true, keyword = false, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("physicalDescription", Seq(s"${nameSpaceMap.get("mnhn").get}physicalDescription"), STRING, indexed = true, stored = true, keyword = false, multivalued = false, analyzed = true),
    EsGraphDBConnectorField("containsArchiveItem", Seq(s"${nameSpaceMap.get("mnhn").get}containsArchiveItem"), URI, indexed = true, stored = true, keyword = false, multivalued = true, analyzed = true), // links on Archive Item

    // sourceDataset

    EsGraphDBConnectorField("collection", Seq(s"${nameSpaceMap.get("mnx").get}sourceDataset"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true), // links on Collection


    EsGraphDBConnectorField("creator", Seq(s"${DCTERMS.NS}creator"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("author", Seq(s"${nameSpaceMap.get("mnhn").get}author"), URI, indexed = true, stored = true, keyword = true),
    //EsGraphDBConnectorField("publisher", Seq(s"${DCTERMS.NS}publisher"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("scientificEditor", Seq(s"${nameSpaceMap.get("mnhn").get}scientificEditor"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("primaryTopic", Seq(s"${FOAF.NS}primaryTopic"), URI, indexed = true, stored = true, keyword = true),

    EsGraphDBConnectorField("issn", Seq("http://purl.org/ontology/bibo/issn"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("doi", Seq("http://purl.org/ontology/bibo/doi"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("publisher", Seq(s"http://purl.org/dc/elements/1.1/publisher"), STRING, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = true),
    EsGraphDBConnectorField("holonymTitle", Seq("http://ns.mnemotix.com/ontologies/2019/8/generic-model/holonymTitle"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = true),


    // todo plazy add citations
    EsGraphDBConnectorField("publicationHasCitation", Seq(s"${nameSpaceMap.get("mnhn").get}publicationHasCitation"), URI, indexed = true, stored = true, keyword = true, multivalued = true),

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override val bulkUpdateBatchSize: Int =  100
}