/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{BOOLEAN, DATE, INT, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap

import scala.concurrent.ExecutionContext

class MaterialConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "material"
  override val types = s"${nameSpaceMap.get("mnhn").get}Material"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("materialEntityID", Seq(s"${nameSpaceMap.get("dwc").get}materialEntityID"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("occurrenceID", Seq(s"${nameSpaceMap.get("dwc").get}occurrenceID"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("datasetID", Seq(s"${nameSpaceMap.get("dwc").get}datasetID"), URI, indexed = true, stored = true, keyword = true),

    EsGraphDBConnectorField("isPrivate", Seq(s"${nameSpaceMap.get("mnhn").get}isPrivate"), BOOLEAN, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("entryNumber", Seq(s"${nameSpaceMap.get("mnhn").get}entryNumber"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("catalogNumber", Seq(s"${nameSpaceMap.get("dwc").get}catalogNumber"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

   // EsGraphDBConnectorField("autopsyDate", Seq(s"${nameSpaceMap.get("mnhn").get}autopsyDate"), DATE, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
   // EsGraphDBConnectorField("birthDate", Seq(s"${nameSpaceMap.get("mnhn").get}birthDate"), DATE, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("boneCondition", Seq(s"${nameSpaceMap.get("mnhn").get}boneCondition"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("captivity", Seq(s"${nameSpaceMap.get("mnhn").get}captivity"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("captivityPlace", Seq(s"${nameSpaceMap.get("mnhn").get}captivityPlace"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
   // EsGraphDBConnectorField("deathDate", Seq(s"${nameSpaceMap.get("mnhn").get}deathDate"), DATE, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),


    EsGraphDBConnectorField("exsiccataAuthor", Seq(s"${nameSpaceMap.get("mnhn").get}exsiccataAuthor"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("exsiccataNumber", Seq(s"${nameSpaceMap.get("mnhn").get}exsiccataNumber"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("exsiccataPrefix", Seq(s"${nameSpaceMap.get("mnhn").get}exsiccataPrefix"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("exsiccataSuffix", Seq(s"${nameSpaceMap.get("mnhn").get}exsiccataSuffix"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("exsiccataTitle", Seq(s"${nameSpaceMap.get("mnhn").get}exsiccataTitle"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("fossil", Seq(s"${nameSpaceMap.get("mnhn").get}fossil"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("individualCount", Seq(s"${nameSpaceMap.get("dwc").get}individualCount"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("karyotype2N", Seq(s"${nameSpaceMap.get("mnhn").get}karyotype2N"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("karyotypeNf", Seq(s"${nameSpaceMap.get("mnhn").get}karyotypeNf"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("karyotypeNfa", Seq(s"${nameSpaceMap.get("mnhn").get}karyotypeNfa"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("materialNature", Seq(s"${nameSpaceMap.get("mnhn").get}materialNature"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("materialEntityRemarks", Seq(s"${nameSpaceMap.get("dwc").get}materialEntityRemarks"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("occurrenceRemarks", Seq(s"${nameSpaceMap.get("dwc").get}occurrenceRemarks"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("skinCondition", Seq(s"${nameSpaceMap.get("mnhn").get}skinCondition"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("stateOfPreservation", Seq(s"${nameSpaceMap.get("mnhn").get}stateOfPreservation"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("preparations", Seq(s"${nameSpaceMap.get("dwc").get}preparations"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),


    //link to Collection
    EsGraphDBConnectorField("inCollection", Seq(s"${nameSpaceMap.get("dwciri").get}inCollection"), URI, indexed = true, stored = true, keyword = true,  multivalued = true),
    EsGraphDBConnectorField("previouslyInCollection", Seq(s"${nameSpaceMap.get("mnhn").get}previouslyInCollection"), URI, indexed = true, stored = true, keyword = true,  multivalued = true),

    // link to CollectionEvent

    EsGraphDBConnectorField("hasCollectionEvent", Seq(s"${nameSpaceMap.get("mnhn").get}hasCollectionEvent"), URI, indexed = true, stored = true, keyword = true,  multivalued = true),

    // link to Identification

    EsGraphDBConnectorField("hasIdentification", Seq(s"${nameSpaceMap.get("mnhn").get}hasIdentification"),URI, indexed = true, stored = true, keyword = true,  multivalued = true),

    // link to deletation
    //EsGraphDBConnectorField("hasDeletation", Seq(s"${nameSpaceMap.get("mnx").get}hasDeletation", s"${nameSpaceMap.get("mnx").get}occursAt"), DATE, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),


    // link to digitalization
    EsGraphDBConnectorField("hasDigitalization", Seq(s"${nameSpaceMap.get("mnhn").get}hasDigitalization"),URI, indexed = true, stored = true, keyword = true, multivalued = true),

    // link to genomicSequenceRecord
    EsGraphDBConnectorField("hasGenomicSequenceRecord", Seq(s"${nameSpaceMap.get("mnhn").get}hasGenomicSequenceRecord"),URI, indexed = true, stored = true, keyword = true,  multivalued = true)


  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override val bulkUpdateBatchSize: Int =  9000

}