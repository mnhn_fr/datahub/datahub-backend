/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{BOOLEAN, DATE, INT, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.{BOOLEANDATATYPE, nameSpaceMap}

import scala.concurrent.ExecutionContext

class IdentificationConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "identification"
  override val types = s"${nameSpaceMap.get("mnhn").get}Identification"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("identificationID", Seq(s"${nameSpaceMap.get("dwc").get}identificationID"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("identificationOrder", Seq(s"${nameSpaceMap.get("mnhn").get}identificationOrder"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("dateIdentified", Seq(s"${nameSpaceMap.get("dwc").get}dateIdentified"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("typeStatus", Seq(s"${nameSpaceMap.get("dwc").get}typeStatus"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("identificationQualifier", Seq(s"${nameSpaceMap.get("dwc").get}identificationQualifier"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("identificationRemarks", Seq(s"${nameSpaceMap.get("dwc").get}identificationRemarks"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("typeStatusRemarks", Seq(s"${nameSpaceMap.get("mnhn").get}typeStatusRemarks"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("verbatimDateIdentified", Seq(s"${nameSpaceMap.get("mnhn").get}verbatimDateIdentified"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),


    EsGraphDBConnectorField("isCurrent", Seq(s"${nameSpaceMap.get("mnhn").get}isCurrent"), BOOLEAN, indexed = true, stored = true, keyword = false, multivalued = false, analyzed = false),

    //link to Person
    EsGraphDBConnectorField("identifiedBy", Seq(s"${nameSpaceMap.get("dwciri").get}identifiedBy"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = false),

    // link to Material

    EsGraphDBConnectorField("hasMaterial", Seq(s"${nameSpaceMap.get("mnhn").get}hasMaterial"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = false),

    // link to Taxon

    EsGraphDBConnectorField("toTaxon", Seq(s"${nameSpaceMap.get("dwciri").get}toTaxon"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = false),

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override val bulkUpdateBatchSize: Int =  9000

}