/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{DATE, INT, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap

import scala.concurrent.ExecutionContext

class GeolocationConnector(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "geolocation"
  override val types = s"${nameSpaceMap.get("mnhn").get}Geolocation"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("locationID", Seq(s"${nameSpaceMap.get("dwc").get}locationID"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("continent", Seq(s"${nameSpaceMap.get("dwc").get}continent"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    //EsGraphDBConnectorField("country", Seq(s"${nameSpaceMap.get("dwc")}country"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("countryCode", Seq(s"${nameSpaceMap.get("dwc").get}countryCode"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("municipality", Seq(s"${nameSpaceMap.get("dwc").get}municipality"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("locality", Seq(s"${nameSpaceMap.get("dwc").get}locality"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("sea", Seq(s"${nameSpaceMap.get("mnhn").get}sea"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("ocean", Seq(s"${nameSpaceMap.get("mnhn").get}ocean"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("locationRemarks", Seq(s"${nameSpaceMap.get("dwc").get}locationRemarks"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("islandGroup", Seq(s"${nameSpaceMap.get("dwc").get}islandGroup"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("island", Seq(s"${nameSpaceMap.get("dwc").get}island"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("originalName", Seq(s"${nameSpaceMap.get("mnhn").get}originalName"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("verbatimCountry", Seq(s"${nameSpaceMap.get("dwc").get}verbatimCountry"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("country", Seq(s"${nameSpaceMap.get("dwc").get}country"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("firstAdministrativeLevel", Seq(s"${nameSpaceMap.get("mnhn").get}firstAdministrativeLevel"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("secondAdministrativeLevel", Seq(s"${nameSpaceMap.get("mnhn").get}secondAdministrativeLevel"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    // geosparql
    EsGraphDBConnectorField("hasGeometry", Seq(s"${nameSpaceMap.get("geosparql").get}hasGeometry"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    
    // link with Geolocation
    EsGraphDBConnectorField("hasGeolocation", Seq(s"${nameSpaceMap.get("mnhn").get}hasGeolocation"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    // link with person
    EsGraphDBConnectorField("recordedBy", Seq(s"${nameSpaceMap.get("dwciri").get}recordedBy"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd
}