package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{FLOAT, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap

import scala.concurrent.ExecutionContext

class EntitySplittingConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector  {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "entity-splitting"
  override val types = s"${nameSpaceMap.get("mnhn").get}EntitySplitting"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  //nameSpaceMap

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("formerEntity", Seq(s"${nameSpaceMap.get("mnx").get}formerEntity"),URI, indexed = true, stored = true, keyword = true,  multivalued = true),
    EsGraphDBConnectorField("confidenceScore", Seq(s"${nameSpaceMap.get("mnx").get}confidenceScore"),FLOAT, indexed = true, stored = true, keyword = true,  multivalued = true),

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override val bulkUpdateBatchSize: Int = 1000
}
