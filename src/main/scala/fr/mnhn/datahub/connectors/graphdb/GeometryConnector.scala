/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{BOOLEAN, FLOAT, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.{FLOATDATATYPE, nameSpaceMap}

import scala.concurrent.ExecutionContext

class GeometryConnector(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "geometry"
  override val types = s"${nameSpaceMap.get("geosparql").get}Geometry"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("decimalLatitude", Seq(s"${nameSpaceMap.get("dwc").get}decimalLatitude"), FLOAT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("decimalLongitude", Seq(s"${nameSpaceMap.get("dwc").get}decimalLongitude"), FLOAT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("verbatimCoordinates", Seq(s"${nameSpaceMap.get("dwc").get}verbatimCoordinates"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("verbatimCoordinatesSystem", Seq(s"${nameSpaceMap.get("dwc").get}verbatimCoordinatesSystem"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("coordinatePrecision", Seq(s"${nameSpaceMap.get("dwc").get}coordinatePrecision"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("georeferenceSources", Seq(s"${nameSpaceMap.get("dwc").get}georeferenceSources"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("coordinatesEstimated", Seq(s"${nameSpaceMap.get("mnhn").get}coordinatesEstimated"), BOOLEAN, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("asGeoJSON", Seq(s"${nameSpaceMap.get("geosparql").get}asGeoJSON"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("alt", Seq(s"${nameSpaceMap.get("geo").get}alt"), FLOAT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),


    EsGraphDBConnectorField("minimumElevationInMeters", Seq(s"${nameSpaceMap.get("dwc").get}minimumElevationInMeters"), FLOAT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("maximumElevationInMeters", Seq(s"${nameSpaceMap.get("dwc").get}maximumElevationInMeters"), FLOAT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),


    EsGraphDBConnectorField("altitudeUnit", Seq(s"${nameSpaceMap.get("mnhn").get}altitudeUnit"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("altitudeAccuracy", Seq(s"${nameSpaceMap.get("mnhn").get}altitudeAccuracy"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("altitudeRemarks", Seq(s"${nameSpaceMap.get("mnhn").get}altitudeRemarks"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),


    EsGraphDBConnectorField("verbatimDepth", Seq(s"${nameSpaceMap.get("dwc").get}verbatimDepth"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("minimumDepthInMeters", Seq(s"${nameSpaceMap.get("dwc").get}minimumDepthInMeters"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("maximumDepthInMeters", Seq(s"${nameSpaceMap.get("dwc").get}maximumDepthInMeters"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("geodeticDatum", Seq(s"${nameSpaceMap.get("dwc").get}geodeticDatum"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd
}