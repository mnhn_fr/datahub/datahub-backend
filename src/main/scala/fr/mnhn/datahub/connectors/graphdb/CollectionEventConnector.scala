/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{DATE, INT, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap

import scala.concurrent.ExecutionContext

class CollectionEventConnector(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "collection-event"
  override val types = s"${nameSpaceMap.get("mnhn").get}CollectionEvent"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  //todo all the fields are not indexed

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("eventID", Seq(s"${nameSpaceMap.get("dwc").get}eventID"),  URI, indexed = true, stored = true, keyword = true),

    EsGraphDBConnectorField("dataOrigin", Seq(s"${nameSpaceMap.get("mnhn").get}dataOrigin"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("duplicateCount", Seq(s"${nameSpaceMap.get("mnhn").get}duplicateCount"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("duplicateDestination", Seq(s"${nameSpaceMap.get("mnhn").get}duplicateDestination"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("eventDate", Seq(s"${nameSpaceMap.get("dwc").get}eventDate"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("earliestDateCollected", Seq(s"${nameSpaceMap.get("dwc").get}EarliestDateCollected"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("LatestDateCollected", Seq(s"${nameSpaceMap.get("dwc").get}LatestDateCollected"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("verbatimEventDate", Seq(s"${nameSpaceMap.get("dwc").get}verbatimEventDate"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("fieldNumber", Seq(s"${nameSpaceMap.get("dwc").get}fieldNumber"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("host", Seq(s"${nameSpaceMap.get("mnhn").get}host"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("oldHarvestIdenfifier", Seq(s"${nameSpaceMap.get("mnhn").get}oldHarvestIdenfifier"),  URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("partCount", Seq(s"${nameSpaceMap.get("mnhn").get}partCount"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("eventRemarks", Seq(s"${nameSpaceMap.get("dwc").get}eventRemarks"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("stationNumber", Seq(s"${nameSpaceMap.get("mnhn").get}stationNumber"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("usage", Seq(s"${nameSpaceMap.get("mnhn").get}usage"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),


    // link with Geolocation
    EsGraphDBConnectorField("hasGeolocation", Seq(s"${nameSpaceMap.get("mnhn").get}hasGeolocation"),  URI, indexed = true, stored = true, keyword = true),
    // link with person
    EsGraphDBConnectorField("recordedBy", Seq(s"${nameSpaceMap.get("dwciri").get}recordedBy"), URI, indexed = true, stored = true, keyword = true),


    // link with Material
    EsGraphDBConnectorField("collectedMaterial", Seq(s"${nameSpaceMap.get("mnhn").get}collectedMaterial"), URI, indexed = true, stored = true, keyword = true),

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override def getConnectorRequest: String = createConnectorRequest()
}