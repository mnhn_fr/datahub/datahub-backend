/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{DATE, INT, STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap
import org.eclipse.rdf4j.model.vocabulary.DCTERMS

import scala.concurrent.ExecutionContext

class TaxonConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "taxon"
  override val types = s"${nameSpaceMap.get("mnhn").get}Taxon"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  //nameSpaceMap

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("taxonID", Seq(s"${nameSpaceMap.get("dwc").get}taxonID"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("date", Seq(s"${DCTERMS.NS}"), DATE, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("kingdom", Seq(s"${nameSpaceMap.get("dwc").get}kingdom"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("phylum", Seq(s"${nameSpaceMap.get("dwc").get}phylum"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("class", Seq(s"${nameSpaceMap.get("dwc").get}class"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("order", Seq(s"${nameSpaceMap.get("dwc").get}order"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("family", Seq(s"${nameSpaceMap.get("dwc").get}family"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("genus", Seq(s"${nameSpaceMap.get("dwc").get}genus"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("taxonomicStatus", Seq(s"${nameSpaceMap.get("dwc").get}taxonomicStatus"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("taxonRemarks", Seq(s"${nameSpaceMap.get("dwc").get}taxonRemarks"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("scientificNameAuthorship", Seq(s"${nameSpaceMap.get("dwc").get}scientificNameAuthorship"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("scientificName", Seq(s"${nameSpaceMap.get("dwc").get}scientificName"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("scientificNameWOAuthorship", Seq(s"${nameSpaceMap.get("mnhn").get}scientificNameWOAuthorship"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("numFamily", Seq(s"${nameSpaceMap.get("mnhn").get}numFamily"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("numFamilyAPG", Seq(s"${nameSpaceMap.get("mnhn").get}numFamilyAPG"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    //EsGraphDBConnectorField("numFamilyAPG", Seq(s"${nameSpaceMap.get("mnhn")}numFamilyAPG"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("numGenus", Seq(s"${nameSpaceMap.get("mnhn").get}numFamilyAPG"), INT, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    //skos:concept
    EsGraphDBConnectorField("taxonRank", Seq(s"${nameSpaceMap.get("dwc").get}taxonRank"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false), // skos taxrefrk:Species
    EsGraphDBConnectorField("hasReferenceName", Seq(s"${nameSpaceMap.get("taxrefprop").get}hasReferenceName"), URI, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false), // skos
    EsGraphDBConnectorField("hasTaxonAlignment", Seq(s"${nameSpaceMap.get("mnhn").get}hasTaxonAlignment"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = false), // skos

    //link with Person
    EsGraphDBConnectorField("nominalTaxonAuthor", Seq(s"${nameSpaceMap.get("mnhn").get}nominalTaxonAuthor"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = false),
    EsGraphDBConnectorField("infraspTaxonAuthor", Seq(s"${nameSpaceMap.get("mnhn").get}infraspTaxonAuthor"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = false),
    EsGraphDBConnectorField("infraspNominalTaxonAuthor", Seq(s"${nameSpaceMap.get("mnhn").get}infraspNominalTaxonAuthor"), URI, indexed = true, stored = true, keyword = true, multivalued = true, analyzed = false),

    // link withIdentification

    EsGraphDBConnectorField("taxonHasIdentification", Seq(s"${nameSpaceMap.get("mnhn").get}taxonHasIdentification"), URI, indexed = true, stored = true, keyword = true, multivalued = true),

    // taxonHasCitation
    EsGraphDBConnectorField("taxonHasCitation", Seq(s"${nameSpaceMap.get("mnhn").get}taxonHasCitation"), URI, indexed = true, stored = true, keyword = true, multivalued = true)

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override val bulkUpdateBatchSize: Int = 1000
}