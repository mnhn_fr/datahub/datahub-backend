package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{BOOLEAN, NESTED, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap

import scala.concurrent.ExecutionContext

class MaterialSearchConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector{
  override val indexName = ESConfiguration.prefix.getOrElse("") + "material-search"
  override val types = s"${nameSpaceMap.get("mnhn").get}Material"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("materialEntityID", Seq(s"${nameSpaceMap.get("dwc").get}materialEntityID"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("hasIdentification", Seq(s"${nameSpaceMap.get("mnhn").get}hasIdentification"),NESTED, indexed = true, stored = true, keyword = true,  multivalued = true, objectFields = Some( Seq(
      EsGraphDBConnectorField("isCurrent", Seq(s"${nameSpaceMap.get("mnhn").get}isCurrent"), BOOLEAN, indexed = false, stored = false, keyword = false, multivalued = false, analyzed = false),
      EsGraphDBConnectorField("taxonId", Seq(s"${nameSpaceMap.get("dwciri").get}toTaxon", s"${nameSpaceMap.get("dwc").get}taxonID"), URI, indexed = false, stored = false, keyword = true, multivalued = true, analyzed = false)
    )))
  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override val bulkUpdateBatchSize: Int =  9000
}
