package fr.mnhn.datahub.connectors.graphdb

import akka.actor.ActorSystem
import com.mnemotix.synaptix.gi.api.graphdb.EsGraphDBConnectorDataType.{STRING, URI}
import com.mnemotix.synaptix.gi.api.graphdb.{EsGraphDBConnectorField, GraphDBConnector}
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFClientWriteConnection}
import fr.mnhn.datahub.connectors.RDFSerializer.nameSpaceMap
import org.eclipse.rdf4j.model.vocabulary.{DCTERMS, RDFS}

import scala.concurrent.ExecutionContext

class DigitalizationConnector()(implicit val system: ActorSystem, val ec: ExecutionContext) extends GraphDBConnector {
  override val indexName = ESConfiguration.prefix.getOrElse("") + "digitalization"
  override val types = s"${nameSpaceMap.get("mnhn").get}Digitalization"
  override val readonly: Boolean = false
  override val isPercolator: Boolean = false

  override val esGraphDBConnectorFields: Seq[EsGraphDBConnectorField] = Seq(
    EsGraphDBConnectorField("entityId", Seq("$self"), URI, indexed = true, stored = true, keyword = true),

    EsGraphDBConnectorField("digitalizationProjectTitle", Seq(s"${nameSpaceMap.get("mnhn").get}digitalizationProjectTitle"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("digitalizationProjectId", Seq(s"${nameSpaceMap.get("mnhn").get}digitalizationProjectId"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("digitalizationEquipmentType", Seq(s"${nameSpaceMap.get("mnhn").get}digitalizationEquipmentType"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("identifier", Seq(s"${nameSpaceMap.get("dcterms").get}identifier"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("description", Seq(s"${nameSpaceMap.get("dcterms").get}description"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

    EsGraphDBConnectorField("image", Seq(s"http://schema.org/image"), URI, indexed = true, stored = true, keyword = true),
    EsGraphDBConnectorField("seeAlso", Seq(s"http://www.w3.org/2000/01/rdf-schema#seeAlso"), URI, indexed = true, stored = true, keyword = true),

    EsGraphDBConnectorField("institutionCode", Seq(s"${nameSpaceMap.get("dwc").get}institutionCode"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("collectionCode", Seq(s"${nameSpaceMap.get("dwc").get}collectionCode"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),
    EsGraphDBConnectorField("catalogNumber", Seq(s"${nameSpaceMap.get("dwc").get}catalogNumber"), STRING, indexed = true, stored = true, keyword = true, multivalued = false, analyzed = false),

  )

  override implicit val read: RDFClientReadConnection = RDFClient.getReadConnection(GdbConnectorConfig.repoName)
  override implicit val conn: RDFClientWriteConnection = RDFClient.getWriteConnection(GdbConnectorConfig.repoName)
  override val elasticsearchNode: String = GdbConnectorConfig.elasticsearchNode
  override val elasticsearchBasicAuthUser = ESConfiguration.user
  override val elasticsearchBasicAuthPassword = ESConfiguration.passwd

  override val bulkUpdateBatchSize: Int =  9000

}