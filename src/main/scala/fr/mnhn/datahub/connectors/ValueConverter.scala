/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

import org.eclipse.rdf4j.model.{Value, ValueFactory}

import java.net.URI
import java.util.Date

/**
 * A trait for converting a value of type A to an RDF value.
 *
 * This trait defines a method `createValue` that, when implemented, allows the conversion
 * of an object of type A to a corresponding RDF value.
 *
 * @tparam A The type of the object to be converted to an RDF value.
 */
trait ValueConverter[A] {
  /**
   * Converts a value of type A to an RDF value.
   *
   * This method is responsible for transforming an object of type A into an RDF value.
   * The specific implementation of this method is provided by concrete implementations
   * of the `ValueConverter` trait for a given type A.
   *
   * @param vf The `ValueFactory` used to create the RDF value.
   * @param value The object to be converted to an RDF value.
   * @return The RDF value representation of the given object.
   */
  def createValue(vf: ValueFactory, value: A): Value
}


/**
 * Companion object for the `ValueConverter` trait that provides implicit converters
 * for various types to RDF values.
 */
object ValueConverter {
  implicit val stringConverter: ValueConverter[String] = (vf: ValueFactory, value: String) => vf.createLiteral(value)
  implicit val intConverter: ValueConverter[Int] = (vf: ValueFactory, value: Int) => vf.createLiteral(value)
  implicit val dateConverter: ValueConverter[Date] = (vf: ValueFactory, value: Date) => vf.createLiteral(value)
  implicit val longConverter: ValueConverter[Long] = (vf: ValueFactory, value: Long) => vf.createLiteral(value)
  implicit val floatConverter: ValueConverter[Float] = (vf: ValueFactory, value: Float) => vf.createLiteral(value)
  implicit val shortConverter: ValueConverter[Short] = (vf: ValueFactory, value: Short) => vf.createLiteral(value)
  implicit val doubleConverter: ValueConverter[Double] = (vf: ValueFactory, value: Double) => vf.createLiteral(value)
  implicit val booleanConverter: ValueConverter[Boolean] = (vf: ValueFactory, value: Boolean) => vf.createLiteral(value)
  implicit val bigDecimalConverter: ValueConverter[java.math.BigDecimal] =
    (vf: ValueFactory, value: java.math.BigDecimal) => vf.createLiteral(value)
  implicit val bigIntegerConverter: ValueConverter[java.math.BigInteger] =
    (vf: ValueFactory, value: java.math.BigInteger) => vf.createLiteral(value)
  implicit val iriConverter: ValueConverter[URI] = (vf: ValueFactory, value: URI) => vf.createIRI(value.toString)
}