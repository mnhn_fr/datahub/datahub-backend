/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

import com.mnemotix.synaptix.rdf.client.RDFClient
import com.mnemotix.synaptix.rdf.client.models.{RDFClientWriteConnection, RDFModel}
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.{DC, DCAT, DCTERMS, FOAF, PROV, RDF, RDFS, SKOS, XSD}
import org.eclipse.rdf4j.model.{BNode, IRI, Literal, Model, Resource, Value, ValueFactory}
import org.eclipse.rdf4j.rio.{RDFFormat, Rio}

import java.io.{ByteArrayOutputStream, FileOutputStream}
import scala.collection.immutable.HashMap
import scala.concurrent.ExecutionContext

/**
 * A utility object that provides methods for creating RDF models, manipulating namespaces, and converting data types.
 */
object RDFSerializer {
  /** A value factory for creating RDF values */
  val vf: ValueFactory = SimpleValueFactory.getInstance

  /** Predefined datatypes for various RDF properties */
  lazy val STRINGIRIDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#string")
  lazy val INTIRIDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#int")
  lazy val DATEIRIDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#date")
  lazy val URLDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#danyURI")
  lazy val BOOLEANDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#boolean")
  lazy val FLOATDATATYPE = createIRI("http://www.w3.org/2001/XMLSchema#float")

  /** A map of common namespaces used for RDF vocabularies */
  val nameSpaceMap: Map[String, String] = Map(
    "bibo" -> "http://purl.org/ontology/bibo/",
    "bio" -> "http://purl.org/vocab/bio/0.1/",
    "dc" -> "http://purl.org/dc/elements/1.1/",
    "dcat" -> "http://www.w3.org/ns/dcat#",
    "dcmi" -> "http://purl.org/dc/dcmitype/",
    "dcterms" -> "http://purl.org/dc/terms/",
    "dbpedia" -> "http://dbpedia.org/ontology/",
    "dwc" -> "http://rs.tdwg.org/dwc/terms/",
    "dwciri" -> "http://rs.tdwg.org/dwc/iri/",
    "foaf" -> "http://xmlns.com/foaf/0.1/",
    "fnml" -> "http://semweb.mmlab.be/ns/fnml#",
    "fno" -> "https://w3id.org/function/ontology#",
    "grel" -> "http://users.ugent.be/~bjdmeest/function/grel.ttl#",
    "geo" -> "http://www.w3.org/2003/01/geo/wgs84_pos#",
    "geosparql" -> "http://www.opengis.net/ont/geosparql/",
    "hctl" -> "https://www.w3.org/2019/wot/hypermedia#",
    "htv" -> "http://www.w3.org/2011/http#",
    "isbd" -> "http://iflastandards.info/ns/isbd/elements/",
    "isni" -> "http://isni.org/ontology/",
    "marcrel" -> "http://id.loc.gov/vocabulary/relators/",
    "mnhn" -> "https://www.data.mnhn.fr/ontology/",
    "mnhnd" -> "https://www.data.mnhn.fr/data/",
    "mnx" -> "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
    "owl" -> "http://www.w3.org/2002/07/owl#",
    "prov" -> "http://www.w3.org/ns/prov#",
    "rdf" -> "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs" -> "http://www.w3.org/2000/01/rdf-schema#",
    "rdam" -> "http://rdaregistry.info/Elements/m/",
    "rdaelements" -> "http://rdvocab.info/Elements/",
    "rdau" -> "http://rdaregistry.info/Elements/u/",
    "ro" -> "http://purl.obolibrary.org/obo/",
    "rr" -> "http://www.w3.org/ns/r2rml#",
    "schema" -> "http://schema.org/",
    "skos" -> "http://www.w3.org/2004/02/skos/core#",
    "skosxl" -> "http://www.w3.org/2008/05/skos-xl#",
    "taxref" -> "http://taxref.mnhn.fr/lod/",
    "taxrefbgs" -> "http://taxref.mnhn.fr/lod/bioGeoStatus/",
    "taxrefhab" -> "http://taxref.mnhn.fr/lod/habitat/",
    "taxrefloc" -> "http://taxref.mnhn.fr/lod/loc/",
    "taxrefprop" -> "http://taxref.mnhn.fr/lod/property/",
    "taxrefrk" -> "http://taxref.mnhn.fr/lod/taxrank/",
    "taxrefstatus" -> "http://taxref.mnhn.fr/lod/status/",
    "td" -> "https://www.w3.org/2019/wot/td#",
    "tdwgutility" -> "http://rs.tdwg.org/dwc/terms/attributes/",
    "tn" -> "http://rs.tdwg.org/ontology/voc/TaxonName#",
    "void" -> "http://rdfs.org/ns/void#",
    "wd" -> "http://www.wikidata.org/entity/",
    "wdt" -> "http://www.wikidata.org/prop/direct/",
    "xsd" -> "http://www.w3.org/2001/XMLSchema#"
  )


  /**
   * Adds custom namespaces to the provided `ModelBuilder`.
   *
   * @param builder the `ModelBuilder` to which namespaces will be added
   * @param nameSpace a map of prefix to namespace URI pairs
   * @return the updated `ModelBuilder`
   */
  def nameSpace(builder: ModelBuilder, nameSpace: Map[String, String]): ModelBuilder = {
    def modelBuilderHelper(couples: Seq[(String, String)], builder: ModelBuilder): ModelBuilder = {
      couples match {
        case Nil => builder
        case x :: tail =>
          if (x._1 != "default") modelBuilderHelper(tail, builder.setNamespace(x._1, x._2))
          else modelBuilderHelper(tail, builder.setNamespace("", x._2))
      }
    }

    val list = nameSpace.toList
    modelBuilderHelper(list, builder)
  }

  /**
   * Returns a `Model` with predefined namespaces and additional custom namespaces.
   *
   * @param model an optional existing `Model` to build upon; if not provided, a new model is created
   * @return the constructed `Model`
   */
  def getModel(model: Option[Model] = None) = {

    val builder = model.map(new ModelBuilder(_)).getOrElse(new ModelBuilder())
      .setNamespace(XSD.NS)
      .setNamespace(RDF.NS)
      .setNamespace(RDFS.NS)
      .setNamespace(FOAF.NS)
      .setNamespace(PROV.NS)
      .setNamespace(SKOS.NS)
      .setNamespace(DC.NS)
      .setNamespace(DCTERMS.NS)
      .setNamespace(DCAT.NS)
    nameSpace(builder, nameSpaceMap)

  }

  /**
   * Merges two RDF models by combining their namespaces and triples.
   *
   * @param m1 the first `Model` to merge
   * @param m2 the second `Model` to merge
   * @return the merged `Model`
   */
  def merge(m1: Model, m2: Model): Model = {
    m1.getNamespaces.addAll(m2.getNamespaces)
    m1.addAll(m2)
    m1
  }

  /**
   * Returns an empty RDF model.
   *
   * @return a new empty `Model`
   */
  def emptyModel(): Model = new ModelBuilder().build()

  /**
   * Merges a sequence of RDF models into a single model.
   *
   * @param m a sequence of `Model`s to merge
   * @return the merged `Model`
   */
  def merge(m: Seq[Model]): Model = {
    m.foldLeft(emptyModel())(merge(_, _))
  }

  /**
   * Creates a blank node (BNode) in the RDF model.
   *
   * @return a new `BNode`
   */
  def createBNode(): BNode = {
    vf.createBNode()
  }

  /**
   * Creates an IRI (Internationalized Resource Identifier) from the given string.
   *
   * @param id the IRI string
   * @param prefix an optional prefix for the IRI (default is empty)
   * @return the created `IRI`
   */
  def createIRI(id: String, prefix: Option[String] = None): IRI = {
    vf.createIRI(prefix.getOrElse(""), id)
  }

  /**
   * Creates an IRI with the "mnx" namespace prefix.
   *
   * @param id the IRI string
   * @return the created `IRI` using the "mnx" namespace
   */
  def createMnxIRI(id: String): IRI = {
    vf.createIRI(nameSpaceMap("mnx"), id)
  }

  /**
   * Converts a value of type `A` into an RDF value.
   *
   * @param value the value to be converted
   * @param converter an implicit `ValueConverter` for type `A`
   * @tparam A the type of the value
   * @return the RDF `Value` representing the input
   */
  def createValue[A](value: A)(implicit converter: ValueConverter[A]): Value =
    converter.createValue(vf, value)

  /**
   * Creates a localized label for a resource in a specific language.
   *
   * @param label the label text
   * @param language the language code for the label (e.g., "en" for English)
   * @return the created `Literal` representing the localized label
   */
  def createLocalizedLabel(label: String, language: String): Literal = {
    vf.createLiteral(label, language)
  }

  /**
   * Converts an RDF `Model` into a TriG format string.
   *
   * @param model the `Model` to convert
   * @return a string representation of the model in TriG format
   */
  def toTriG(model: Model): String = {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    Rio.write(model, os, org.eclipse.rdf4j.rio.RDFFormat.TRIG)
    val bytes = os.toString
    os.close()
    bytes
  }

  /**
   * Saves an RDF `Model` to a file in TriG format.
   *
   * @param model the `Model` to save
   * @param fileName the name of the file (without extension)
   * @param dirLoc the directory where the file will be saved
   */
  def toFile(model: Model, fileName: String, dirLoc: String): Unit = {
    val file = new FileOutputStream(s"${dirLoc}${fileName}.trig")
    Rio.write(model, file, RDFFormat.TRIG)
  }

  def toGraphDB(model: Model)(implicit ec: ExecutionContext, conn: RDFClientWriteConnection) = {
    RDFClient.load(RDFModel(model))
  }

  /**
   * Converts an RDF `Model` into a JSON-LD format string.
   *
   * @param model the `Model` to convert
   * @return a string representation of the model in JSON-LD format
   */
  def toJsonLd(model: Model): String = {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    Rio.write(model, os, org.eclipse.rdf4j.rio.RDFFormat.JSONLD)
    val bytes = os.toString
    os.close()
    bytes
  }

  /**
   * An implicit class that adds a method to `ModelBuilder` for adding triples if an optional value is present.
   *
   * @param model the `ModelBuilder` to extend
   */
  implicit class addTripleIfPresent[T](model: ModelBuilder) {

    /**
     * Adds a triple to the model if the optional value is present.
     *
     * @param subject the subject resource for the triple
     * @param predicate the predicate for the triple
     * @param optionalValue the optional object value for the triple
     * @param converter an implicit `ValueConverter` for type `T`
     */
    def addTripleIfPresent(subject: Resource, predicate: String, optionalValue: Option[T])(implicit converter: ValueConverter[T]): Unit = {
      optionalValue.foreach { value =>
        model.subject(subject)
          .add(predicate, createValue(value))
      }
    }
  }
}
