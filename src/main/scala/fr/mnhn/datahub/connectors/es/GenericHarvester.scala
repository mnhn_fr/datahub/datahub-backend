/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.actor.ActorSystem
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.core.GenericService
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.mnemotix.synaptix.index.{IndexClient, IndexSearchException}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.{SearchHit, SearchResponse}
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{Await, ExecutionContext, Future}

/**
 * A service class that handles the harvesting of data from an Elasticsearch index and stores it in a cache.
 *
 * @param indexName The name of the Elasticsearch index from which to harvest data.
 * @param cacheDir The directory where the cache will be stored.
 * @param sortField The field used to sort the search results when querying Elasticsearch.
 * @param system The ActorSystem used for asynchronous operations.
 * @param ec The ExecutionContext used for executing asynchronous tasks.
 */
class GenericHarvester(indexName: String,
                       cacheDir: String,
                       sortField: String)(implicit system: ActorSystem, ec: ExecutionContext) extends GenericService {

  lazy val cache = new RocksDBStore(cacheDir)

  /**
   * Initializes the Elasticsearch client and the cache.
   */
  override def init(): Unit = {
    IndexClient.init()
    cache.init()
  }

  /**
   * Converts a SearchHit into a cache entry.
   *
   * @param searchHit The SearchHit to convert.
   * @return A tuple containing the ID and the serialized source of the SearchHit.
   */
  def searchHitToCacheEntry(searchHit: SearchHit) = {
    searchHit.id -> Json.prettyPrint(Json.parse(searchHit.sourceAsString)).getBytes
  }

  /**
   * Converts the response from a search query into cache entries and stores them in the cache.
   *
   * @param response The Future response from the Elasticsearch search query.
   * @return A Future indicating the success or failure of the cache insertion operation.
   */
  def searchResponseToCacheEntry(response: Future[Response[SearchResponse]]): Future[Boolean] = {
    (for {
      res <- response
      r = if (res.isSuccess) {
        val searchHits = res.result.hits.hits.toSeq
        val cacheEntries = searchHits.map(searchHitToCacheEntry)
        cache.bulkInsert(cacheEntries)
      } else {
        throw IndexSearchException(s"An error occurred during the elastic search request", Some(res.error.asException))
      }
    } yield r).flatten
  }

  /**
   * Processes the Elasticsearch index by scrolling through the results and storing them in the cache.
   *
   * @return A Future indicating the success or failure of the processing operation.
   */
  def process(): Future[Boolean] = {
    /**
     * A helper function that recursively scrolls through the Elasticsearch index and stores results in the cache.
     *
     * @param from The current offset of the scroll query.
     * @param size The size of each scroll query.
     * @param total The total number of results in the index.
     * @param scrollId The current scroll ID used for pagination.
     * @return A Future indicating the success or failure of the scroll operation.
     */
    def scrollHelper(from: Int, size: Int, total: Int, scrollId: Future[Option[String]]): Future[Boolean] = {
      if (from == 0) {
        if (total < 10000) {
          logger.info("total < 10000")
          val response = IndexClient.rawQuery(RawQuery(Seq(indexName), Json.parse("{\n    \"query\": {\n        \"match_all\": {}\n    }\n}").as[JsObject]))
          searchResponseToCacheEntry(response)
        }
        else {
          logger.info("from == 0")
          val response: Future[Response[SearchResponse]] = IndexClient.scrollQuery(Seq(indexName), Some("1m"), 10000, sortField)
          val scrollId: Future[Option[String]] = response.map {
            _.result.scrollId
          }
          searchResponseToCacheEntry(response)
          scrollHelper(from + size, size, total, scrollId)
        }
      }
      else if (from < total) {
        val response = for {
          sID <- scrollId
          res = if (sID.isDefined) {
            val response: Future[Response[SearchResponse]] = IndexClient.searchScroll(sID.get, None)

            val scrollId: Future[Option[String]] = response.map {
              _.result.scrollId
            }
            searchResponseToCacheEntry(response)
            if ((from + size) % 100000 == 0) logger.info(s"${from + size} / $total")
            scrollHelper(from + size, size, total, scrollId)
          }
          else {
            Future.successful(true)
          }

        } yield res
        response.flatten
      }
      else {
        Future.successful(true)
      }
    }
    val total = Await.result(IndexClient.count(indexName), 5.seconds)
    if(total.isSuccess) {
      val t = total.result.count.toInt
      logger.info(s"${total.result.count.toInt} in ${indexName}")
      scrollHelper(0, 10000, t, Future(Option.empty[String]))
    }
    else {
      logger.error("An error occured during the source creation process", Some(total.error.asException))
      throw IndexSearchException("An error occured during the source creation process", Some(total.error.asException))
    }
  }

  /**
   * Shuts down the cache.
   */
  override def shutdown(): Unit = cache.shutdown()
}