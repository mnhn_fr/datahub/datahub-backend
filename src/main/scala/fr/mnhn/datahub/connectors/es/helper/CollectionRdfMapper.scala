/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.Collection
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * Utility object for mapping `Collection` instances to RDF models or subgraphs.
 *
 * This object provides methods to construct RDF representations of collections,
 * including their relationships to institutions, collection groups, and their descriptive metadata.
 * It also includes implicit implementations for converting `Collection` objects and sequences
 * of them into RDF models and subgraphs.
 */
object CollectionRdfMapper {

  /**
   * Builds an RDF model for a given `Collection`.
   *
   * This method creates RDF triples to represent the specified `Collection` and adds them
   * to the provided `ModelBuilder`. The RDF representation includes:
   * - The collection's unique ID.
   * - Relationships to an institution and a collection group (if defined).
   * - Metadata such as name, code, and description.
   *
   * @param model The `ModelBuilder` to which RDF triples will be added.
   * @param collection The `Collection` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata.
   */
  def modelBuilding(model: ModelBuilder, collection: Collection, date: String) = {

    if (collection.collection_id.isDefined) {
      val collectionIRI = createIRI(s"https://www.data.mnhn.fr/data/collection/${collection.collection_id.get}")
      model.subject(collectionIRI)
        .add(RDF.TYPE, "mnhn:Collection")

      if (collection.institution_id.isDefined) {
        val institutionIRI = s"https://www.data.mnhn.fr/data/organization/${collection.institution_id.get}"
        model.subject(collectionIRI)
          .add("mnhn:hasInstitution", institutionIRI)

        model.subject(institutionIRI)
          .add(RDF.TYPE, "foaf:Organization")
      }

      if (collection.collection_group_id.isDefined) {
        val collectionGroupIRI = s"https://www.data.mnhn.fr/data/collection-group/${collection.collection_group_id.get}"
        model.subject(collectionIRI)
          .add("mnhn:hasCollectionGroup", collectionGroupIRI)
      }

      model.addTripleIfPresent(collectionIRI, "dwc:collectionID", collection.collection_id)
      model.addTripleIfPresent(collectionIRI, "rdfs:label", collection.name)
      model.addTripleIfPresent(collectionIRI, "dwc:collectionCode", collection.code)
      model.addTripleIfPresent(collectionIRI, "dcterms:description", collection.description)
    }

  }

  /**
   * Implicit conversion from a sequence of `Collection` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `Collection` instances
   * into an RDF model. Each collection in the sequence is processed and represented within the model.
   *
   * @example
   * {{{
   * val collections: Seq[Collection] = ...
   * val rdfModel: Model = CollectionRdfMapper.collectionsToRdfModel.toRdfModel(collections)
   * }}}
   */
  implicit val collectionsToRdfModel: ToRdfModel[Seq[Collection]] = new ToRdfModel[Seq[Collection]] {
    override def toRdfModel(objs: Seq[Collection]): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }

  /**
   * Implicit conversion from a single `Collection` object to an RDF model.
   *
   * This implicit implementation converts a single `Collection` instance into an RDF model.
   * The resulting model includes the collection's metadata and relationships.
   *
   * @example
   * {{{
   * val collection: Collection = ...
   * val rdfModel: Model = CollectionRdfMapper.collectionToRdfModel.toRdfModel(collection)
   * }}}
   */
  implicit val collectionToRdfModel: ToRdfModel[Collection] = new ToRdfModel[Collection] {
    override def toRdfModel(obj: Collection): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
     modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion from a `Collection` object to a subgraph.
   *
   * This implicit implementation converts a `Collection` instance into a `Subgraph`.
   * The subgraph includes:
   * - The collection's unique identifier, resource URI, and graph URI.
   * - Serialized RDF data in TriG format.
   * - Metadata such as the datatype and a cryptographic hash of the serialized model.
   *
   * @example
   * {{{
   * val collection: Collection = ...
   * val subgraph: Subgraph = CollectionRdfMapper.collectionToSubgraph.toSubgraph(collection)
   * }}}
   */
  implicit val collectionToSubgraph: ToSubgraph[Collection] = new ToSubgraph[Collection] {
    override def toSubgraph(obj: Collection): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}