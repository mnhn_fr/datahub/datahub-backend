/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import ai.x.play.json.Jsonx
import ai.x.play.json.Encoders._
import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.time.LocalDate
import java.util.Date

case class MaterialAttributes(
                       autopsy_date: Option[String],
                      birth_date: Option[String],
                      bone_condition: Option[String],
                      captivity: Option[String],
                      capticity_place: Option[String],
                      collection_name: Option[String], // dwciri:inCollection/rdfs:label
                      death_date: Option[String],
                      exsiccata_authors: Option[String], // mnhn:exsiccataAuthor
                      exsiccata_number: Option[String], // mnhn:exsiccataNumber
                      exsiccata_prefix: Option[String], // mnhn:exsiccataPrefix
                      exsiccata_suffix: Option[String], // mnhn:exsiccataSuffix
                      exsiccata_title: Option[String], //mnhn:exsiccataTitle
                      fossil: Option[Boolean],
                      individual_count: Option[Int],
                      karyotype_2n: Option[Int],
                      karyotype_nf: Option[Int],
                      karyotype_nfa: Option[Int],
                      nature: Option[String], // mnhn:materialNature
                      remarks: Option[String],
                      skin_condition: Option[String],
                      state_of_preservation: Option[String],
                      technical_observations: Option[String],
                      type_of_preservation: Option[String]
                     )

object MaterialAttributes {
  implicit val format = Jsonx.formatCaseClassUseDefaults[MaterialAttributes]

  implicit val defaultMaterialAttributes: Default[MaterialAttributes] = new Default[MaterialAttributes] {
    override def defaultValue: MaterialAttributes = MaterialAttributes(None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String], // dwciri:inCollection/rdfs:label
      None: Option[String],
      None: Option[String], // mnhn:exsiccataAuthor
      None: Option[String], // mnhn:exsiccataNumber
      None: Option[String], // mnhn:exsiccataPrefix
      None: Option[String], // mnhn:exsiccataSuffix
      None: Option[String], //mnhn:exsiccataTitle
      None: Option[Boolean],
      None: Option[Int],
      None: Option[Int],
      None: Option[Int],
      None: Option[Int],
      None: Option[String], // mnhn:materialNature
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String])
  }
}

case class Material(attributes:Option[MaterialAttributes],
                    catalog_number: Option[String],
                    code: Option[String],
                    collection_code: Option[String],
                    collection_event_id: Option[String],
                    collection_id: Option[Int],
                    created_at: Option[Date],
                    description: Option[String],
                    domain_id: Option[Int],
                    entry_number: Option[String],
                    institution_code: Option[String],
                    is_deleted: Option[Boolean],
                    is_private: Option[Boolean],
                    label: Option[String],
                    last_modified_at: Option[String],
                    material_id: Option[String],
                    name: Option[String]
                   ) extends Persistable {
  override def id(): String = if (material_id.isDefined) CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/material/${material_id.get}", LocalDate.now().toString) else  CryptoUtils.md5sum("")
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String = if (material_id.isDefined) s"https://www.data.mnhn.fr/data/material/${material_id.get}" else ""

  override def datatype: String = "material"
}

object Material {
 implicit val format = Json.format[Material]

  implicit val defaultValueForMaterial: Default[Material] = new Default[Material] {
    override def defaultValue: Material = Material(
      None: Option[MaterialAttributes],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[Int],
      None: Option[Date],
      None: Option[String],
      None: Option[Int],
      None: Option[String],
      None: Option[String],
      None: Option[Boolean],
      None: Option[Boolean],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String]
    )
  }
}
