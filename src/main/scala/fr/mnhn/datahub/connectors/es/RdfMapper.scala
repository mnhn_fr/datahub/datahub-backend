/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Source}
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.core.GenericService
import fr.mnhn.datahub.connectors.{Default, RDFMapperException, RDFSerializer, ToRdfModel}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.{Json, Reads}

import java.util.concurrent.atomic.AtomicInteger
import scala.collection.immutable.Seq
import scala.util.{Failure, Success, Try}

/**
 * A class responsible for mapping RDF models and extracting data from a RocksDB cache.
 * The RDF mapping process is done using a generic method, and data from the cache is parsed and transformed into RDF models.
 *
 * @param cacheDir The directory path to the cache where RDF models are stored.
 */
class RdfMapper(cacheDir: String) extends GenericService {

  lazy val cache = new RocksDBStore(cacheDir)

  /**
   * Maps an object of type `A` to an RDF model using the implicit `ToRdfModel` typeclass.
   *
   * @param A The type of the object to map.
   * @param obj The object to convert to an RDF model.
   * @param toRdfModel Implicit parameter of type `ToRdfModel[A]` to handle the conversion.
   * @return The RDF model corresponding to the input object.
   */
  private def rdfMapper[A](obj: A)(implicit toRdfModel: ToRdfModel[A]): Model = {
    obj.toRdfModel
  }

  /**
   * Extracts data from the RocksDB cache and maps it to objects of type `A`.
   * If parsing fails, the default value is returned.
   *
   * @param A The type of the object to parse.
   * @param read Implicit `Reads[A]` to handle the deserialization of the data.
   * @param default Implicit `Default[A]` to provide a default value in case of parsing errors.
   * @return A source of objects of type `A` parsed from the cache.
   */
  def cacheExtractor[A](implicit read: Reads[A], default: Default[A]): Source[A, NotUsed] = {
    Source.fromIterator(() => cache.iterator().iterator).map { iter =>
      Try {
        Json.parse(new String((iter.value), "UTF-8")).as[A]
      }
      match {
        case Success(parsedValue) => parsedValue
        case Failure(exception) => {
          logger.error(s"we have an error trying to parse ${Json.parse(new String((iter.value)))}", exception)
          default.defaultValue
        }
      }
    }
  }

  /**
   * Processes the data in the cache by grouping the entries into batches of size `n`.
   * Each batch is then transformed into an RDF model using the implicit `ToRdfModel` typeclass.
   *
   * @param n The batch size to group the data.
   * @param toRdfModel Implicit `ToRdfModel[Seq[A]]` to convert a sequence of objects into an RDF model.
   * @param read Implicit `Reads[A]` to handle the deserialization of the data from the cache.
   * @param default Implicit `Default[A]` to provide a default value in case of parsing errors.
   * @tparam A The type of the objects being processed.
   * @return A source of RDF models processed from the cache.
   * @throws RDFMapperException If an error occurs during the RDF mapping process.
   */
  def process[A](n: Int)(implicit toRdfModel: ToRdfModel[Seq[A]], read: Reads[A], default: Default[A]): Source[Model, NotUsed] = {
    lazy val i = new AtomicInteger()
    try {
      cacheExtractor[A].via(Flow[A].grouped(n).map{objs =>
        logger.info(s"new size : ${i.getAndAdd(objs.size)}")
        objs.toRdfModel
      }
      )
    } catch {
      case t: Throwable => throw RDFMapperException("An error occurred during the rdf mapping process", Some(t))
    }
  }

  override def init(): Unit = {
    cache.init()
  }

  override def shutdown(): Unit = {
    cache.shutdown()
  }
}