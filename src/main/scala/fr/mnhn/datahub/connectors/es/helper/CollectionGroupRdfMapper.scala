/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.CollectionGroup
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * A utility object for mapping `CollectionGroup` instances to RDF models or subgraphs.
 *
 * This object provides methods to build RDF representations of collection groups,
 * including their attributes, metadata, and relationships. It also includes
 * implicit implementations for converting `CollectionGroup` objects and sequences
 * of them into RDF models and subgraphs.
 */
object CollectionGroupRdfMapper {
  /**
   * Builds an RDF model for a given `CollectionGroup`.
   *
   * This method constructs RDF triples for the specified `CollectionGroup` and adds them
   * to the provided `ModelBuilder`. The RDF representation includes attributes such as
   * the group's name and description.
   *
   * @param model The `ModelBuilder` to which RDF triples will be added.
   * @param collectionGroup The `CollectionGroup` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata.
   */
  def modelBuilding(model: ModelBuilder, collectionGroup: CollectionGroup, date: String) = {
    if (collectionGroup.collection_group_id.isDefined) {
      val collectionGroupIRI = createIRI(s"https://www.data.mnhn.fr/data/collection-group/${collectionGroup.collection_group_id.get}")
      model.subject(collectionGroupIRI)
        .add(RDF.TYPE, "mnhn:CollectionGroup")

      model.addTripleIfPresent(collectionGroupIRI, "rdfs:label", collectionGroup.name)
      model.addTripleIfPresent(collectionGroupIRI, "dcterms:description", collectionGroup.description)
    }
  }

  /**
   * Implicit conversion from a sequence of `CollectionGroup` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `CollectionGroup` instances
   * into an RDF model. Each group is processed and represented within the resulting model.
   */
  implicit val collectionGroupsToRdfModel: ToRdfModel[Seq[CollectionGroup]] = new ToRdfModel[Seq[CollectionGroup]] {
    override def toRdfModel(objs: Seq[CollectionGroup]): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }

  /**
   * Implicit conversion from a single `CollectionGroup` object to an RDF model.
   *
   * This implicit implementation converts a single `CollectionGroup` instance into an RDF model.
   * The resulting model includes all relevant attributes and metadata of the group.
   */
  implicit val collectionGroupToRdfModel: ToRdfModel[CollectionGroup] = new ToRdfModel[CollectionGroup] {
    override def toRdfModel(obj: CollectionGroup): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion from a `CollectionGroup` object to a subgraph.
   *
   * This implicit implementation converts a `CollectionGroup` instance into a `Subgraph`.
   * The subgraph includes metadata such as the group's unique identifier, resource URI,
   * graph URI, and serialized model string in TriG format.
   */
  implicit val collectionGroupToSubgraph: ToSubgraph[CollectionGroup] = new ToSubgraph[CollectionGroup] {
    override def toSubgraph(obj: CollectionGroup): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}