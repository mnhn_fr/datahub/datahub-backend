/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.util.Date

case class Attributes(collected_by: Option[String], data_origin: Option[String], duplicate_count: Option[Int], duplicate_destination: Option[String],
                      event_date: Option[Date], event_date_begin: Option[Date], event_date_end: Option[Date], event_date_verbatim: Option[String],
                      field_number: Option[String], harvest_identifier: Option[String], host: Option[String], old_harvest_identifier: Option[String],
                      part_count: Option[Int], remarks: Option[String], station_number: Option[String], usage: Option[String]
                     )


object Attributes {
  implicit lazy val format = Json.format[Attributes]

  implicit val defaultValueForAttributes: Default[Attributes] = new Default[Attributes] {
    override def defaultValue: Attributes = Attributes(None,None, None: Option[Int], None: Option[String],
      None: Option[Date], None: Option[Date], None: Option[Date], None: Option[String],
      None: Option[String], None: Option[String], None: Option[String], None: Option[String],
      None: Option[Int], None: Option[String], None: Option[String], None: Option[String])
  }
}

