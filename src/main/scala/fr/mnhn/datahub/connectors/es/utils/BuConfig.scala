/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.utils

import com.typesafe.config.ConfigFactory

/**
 * The `BuConfig` object is responsible for loading and providing configuration values
 * for various application modules, including geolocation, material, collection events,
 * collection groups, coordinates, collections, domains, identification, institutions,
 * material relationships, and taxon-related data. The configuration values are read
 * from the application's configuration file, typically using the Typesafe Config library.
 *
 * Each module's configuration contains information such as RDF directory paths,
 * cache directory paths, and index names, which are used throughout the application
 * for various data processing and storage tasks.
 *
 * <p>Modules and their configuration keys:
 * <ul>
 *   <li>Geolocation: `geolocation.rdf.dir`, `geolocation.cache.dir`, `geolocation.index.name`</li>
 *   <li>Material: `material.rdf.dir`, `material.cache.dir`, `material.index.name`</li>
 *   <li>CollectionEvent: `collectionEvent.rdf.dir`, `collectionEvent.cache.dir`, `collectionEvent.index.name`</li>
 *   <li>CollectionGroup: `collectionGroup.rdf.dir`, `collectionGroup.cache.dir`, `collectionGroup.index.name`</li>
 *   <li>Coordinates: `coordinates.rdf.dir`, `coordinates.cache.dir`, `coordinates.index.name`</li>
 *   <li>Collection: `collection.rdf.dir`, `collection.cache.dir`, `collection.index.name`</li>
 *   <li>Domain: `domain.rdf.dir`, `domain.cache.dir`, `domain.index.name`</li>
 *   <li>Identification: `identification.rdf.dir`, `identification.cache.dir`, `identification.index.name`</li>
 *   <li>Institution: `institution.rdf.dir`, `institution.cache.dir`, `institution.index.name`</li>
 *   <li>MaterialRelationship: `materialRelationship.rdf.dir`, `materialRelationship.cache.dir`, `materialRelationship.index.name`</li>
 *   <li>Taxon: `taxon.rdf.dir`, `taxon.cache.dir`, `taxon.index.name`</li>
 * </ul>
 *
 * The configuration values are lazily loaded, meaning that they are only read from
 * the configuration file when they are first accessed.
 */

object BuConfig {

  //geolocation
  lazy val confGeolocation = Option(ConfigFactory.load().getConfig("geolocation")).getOrElse(ConfigFactory.empty())
  lazy val geolocationrdfDir = confGeolocation.getString("rdf.dir")
  lazy val geolocationcacheDir = confGeolocation.getString("cache.dir")
  lazy val geolocationindexName = confGeolocation.getString("index.name")

  //material
  lazy val confMaterial = Option(ConfigFactory.load().getConfig("material")).getOrElse(ConfigFactory.empty())
  lazy val materialrdfDir = confMaterial.getString("rdf.dir")
  lazy val materialcacheDir = confMaterial.getString("cache.dir")
  lazy val materialindexName = confMaterial.getString("index.name")

  //collectionEvent
  lazy val confCollectionEvent = Option(ConfigFactory.load().getConfig("collectionEvent")).getOrElse(ConfigFactory.empty())
  lazy val collectionEventrdfDir = confCollectionEvent.getString("rdf.dir")
  lazy val collectionEventcacheDir = confCollectionEvent.getString("cache.dir")
  lazy val collectionEventindexName = confCollectionEvent.getString("index.name")

  //collectionGroup
  lazy val confCollectionGroup = Option(ConfigFactory.load().getConfig("collectionGroup")).getOrElse(ConfigFactory.empty())
  lazy val collectionGrouprdfDir = confCollectionGroup.getString("rdf.dir")
  lazy val collectionGroupcacheDir = confCollectionGroup.getString("cache.dir")
  lazy val collectionGroupindexName = confCollectionGroup.getString("index.name")

  //coordinates
  lazy val confCoordinates = Option(ConfigFactory.load().getConfig("coordinates")).getOrElse(ConfigFactory.empty())
  lazy val coordinatesrdfDir = confCoordinates.getString("rdf.dir")
  lazy val coordinatescacheDir = confCoordinates.getString("cache.dir")
  lazy val coordinatesindexName = confCoordinates.getString("index.name")

  //collection
  lazy val confCollection = Option(ConfigFactory.load().getConfig("collection")).getOrElse(ConfigFactory.empty())
  lazy val collectionrdfDir = confCollection.getString("rdf.dir")
  lazy val collectioncacheDir = confCollection.getString("cache.dir")
  lazy val collectionindexName = confCollection.getString("index.name")

  //domain
  lazy val confDomain = Option(ConfigFactory.load().getConfig("domain")).getOrElse(ConfigFactory.empty())
  lazy val domainrdfDir = confDomain.getString("rdf.dir")
  lazy val domaincacheDir = confDomain.getString("cache.dir")
  lazy val domainindexName = confDomain.getString("index.name")

  //identification
  lazy val confIdentification = Option(ConfigFactory.load().getConfig("identification")).getOrElse(ConfigFactory.empty())
  lazy val identificationrdfDir = confIdentification.getString("rdf.dir")
  lazy val identificationcacheDir = confIdentification.getString("cache.dir")
  lazy val identificationindexName = confIdentification.getString("index.name")

  //institution
  lazy val confInstitution = Option(ConfigFactory.load().getConfig("institution")).getOrElse(ConfigFactory.empty())
  lazy val institutionrdfDir = confInstitution.getString("rdf.dir")
  lazy val institutioncacheDir = confInstitution.getString("cache.dir")
  lazy val institutionindexName = confInstitution.getString("index.name")
  //materialRelationship
  lazy val confMaterialRelationship = Option(ConfigFactory.load().getConfig("materialRelationship")).getOrElse(ConfigFactory.empty())
  lazy val materialRelationshiprdfDir = confMaterialRelationship.getString("rdf.dir")
  lazy val materialRelationshipcacheDir = confMaterialRelationship.getString("cache.dir")
  lazy val materialRelationshipindexName = confMaterialRelationship.getString("index.name")

  //taxon
  lazy val confTaxon = Option(ConfigFactory.load().getConfig("taxon")).getOrElse(ConfigFactory.empty())
  lazy val taxonrdfDir = confTaxon.getString("rdf.dir")
  lazy val taxoncacheDir = confTaxon.getString("cache.dir")
  lazy val taxonindexName = confTaxon.getString("index.name")

  //

}
