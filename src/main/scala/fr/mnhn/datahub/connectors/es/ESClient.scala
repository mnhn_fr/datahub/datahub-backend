/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Source
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.mnemotix.synaptix.index.{IndexClient, IndexSearchException}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.{SearchHit, SearchResponse}
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{Json, Reads}

import scala.concurrent.duration.{Duration, DurationInt}
import scala.concurrent.{Await, Future}

/**
 * ESClient is a client for interacting with an Elasticsearch service, providing methods to
 * initialize the client, perform queries, and handle scrolling over large result sets.
 *
 * @param system The actor system used for managing concurrent operations.
 */
class ESClient(implicit val system: ActorSystem) extends LazyLogging {
  /**
   * Initializes the Elasticsearch client.
   * This method sets up the index client to be ready for making requests.
   */
  implicit val ec = system.getDispatcher

  val max_result_window = ESConfiguration.maxResultWindow

  def init(): Unit = {
    IndexClient.init()
  }

  /**
   * Lists the names of all available indices in the Elasticsearch service.
   *
   * @return A future containing a sequence of index names.
   */

  def listIndexName() = IndexClient.listIndices().map(_.result.map(_.index))

  /**
   * Merges a sequence of futures containing data of type A into a single future sequence.
   *
   * @param c A sequence of futures, each containing a sequence of type A.
   * @tparam A The type of data contained in the sequences.
   * @return A future containing a merged sequence of type A.
   */
  private def mergeFut[A](c: Seq[Future[Seq[A]]]): Future[Seq[A]] = c.reduceLeft((a, b) => a.flatMap(xa => b.map(xb => xa ++ xb)))

  /**
   * Merges a sequence of futures containing search hits into a single future sequence of search hits.
   *
   * @param c A sequence of futures, each containing a sequence of SearchHit.
   * @return A future containing a merged sequence of SearchHit.
   */
  private def mergeSearchHitFut(c: Seq[Future[Seq[SearchHit]]]): Future[Seq[SearchHit]] = c.reduceLeft((a, b) => a.flatMap(xa => b.map(xb => xa ++ xb)))


  /**
   * Merges two sequences of search hits into a single sequence.
   *
   * @param s1 The first sequence of SearchHit.
   * @param s2 The second sequence of SearchHit.
   * @return A merged sequence of SearchHit.
   */
  private def mergeSearchHits(s1: Seq[SearchHit], s2: Seq[SearchHit]): Seq[SearchHit] = s1 ++ s2

  /**
   * A helper function for scrolling through large result sets from Elasticsearch, retrieving
   * results in pages and handling pagination using the scroll API.
   *
   * @param indexName The names of the indices to search in.
   * @param sortField The field by which the results should be sorted.
   * @param total The total number of results to retrieve.
   * @return A future containing the sequence of SearchHit results.
   */
  private def scrollHelper(indexName: Seq[String], sortField: String, total: Int): Future[Seq[SearchHit]] = {
    def scrollPage(from: Int, size: Int, scrollId: Option[String], searchHits: Seq[SearchHit]): Future[Seq[SearchHit]] = {
      if (from < total) {
        val responseFuture: Future[Response[SearchResponse]] =
          if (from == 0) IndexClient.scrollQuery(indexName, Some("1m"), size, sortField)
          else IndexClient.searchScroll(scrollId.get, None)

        responseFuture.flatMap { response =>
          if (response.isSuccess) {
            val newHits = response.result.hits.hits.toSeq
            val newSearchHits = searchHits ++ newHits
            if ((from + size) % 100000 == 0) logger.info(s"${from + size} / $total")
            scrollPage(from + size, size, Some(response.result.scrollId.getOrElse("")), newSearchHits)
          } else {
            throw IndexSearchException("An error occurred during the elastic search request", Some(response.error.asException))
          }
        }
      } else {
        Future.successful(searchHits)
      }
    }

    (0 until total by max_result_window).foldLeft(Future.successful(Seq.empty[SearchHit])) {
      case (accFuture, from) =>
        accFuture.flatMap { accHits =>
          scrollPage(from, max_result_window, None, accHits)
        }
    }
  }

  /**
   * Converts the Elasticsearch search response to a sequence of objects of type A.
   *
   * @param futureResponse The future containing the search response.
   * @tparam A The type of objects to convert the search hits to.
   * @return A future containing a sequence of objects of type A.
   */
  private def toObject[A](futureResponse: Future[Response[SearchResponse]])(implicit read: Reads[A]): Future[Seq[A]] = {
    futureResponse.map { response =>
      try {
        if (response.isSuccess) {
          response.result.hits.hits.toSeq.map { hit =>
            val json = Json.parse(hit.sourceAsString)
            json.as[A]
          }
        }
        else Seq.empty[A]
      }
      catch {
        case exception: Exception => {
          throw IndexSearchException(s"An error occurred during the elastic search request", Some(exception))
          List.empty[A]
        }
      }
    }
  }

  /**
   * Extracts a sequence of objects of type A asynchronously from Elasticsearch, using the scroll API.
   *
   * @param indexName The names of the indices to search in.
   * @param sortField The field by which the results should be sorted.
   * @param total The total number of results to retrieve.
   * @tparam A The type of objects to extract.
   * @return A future containing a sequence of objects of type A.
   */
  def extractAsync[A](indexName: Seq[String], sortField: String, total: Int)(implicit read: Reads[A]): Future[Seq[A]] = {
    def scrollHelper(from: Int, size: Int, scrollId: Future[Option[String]], searchResponse: Future[Seq[A]]): Future[Seq[A]] = {
      if (from == 0) {

        val response = IndexClient.scrollQuery(indexName, Some("1m"), max_result_window, sortField)
        println(Await.result(response, Duration.Inf))
        val scrollId: Future[Option[String]] = response.map {_.result.scrollId}
       scrollHelper(from + size, size, scrollId, toObject(response))
      }
      else if (from < total) {
        val response = for {
          sID <- scrollId
          res = if (sID.isDefined) {
            val response = IndexClient.searchScroll(sID.get, None)
            val scrollId: Future[Option[String]] = response.map {_.result.scrollId}
            val obj = toObject(response)
            if ((from + size) % 100000 == 0) logger.info(s"${from + size} / $total")
            scrollHelper(from + size, size, scrollId, mergeFut(Seq(obj, searchResponse)))
          }
          else {
            searchResponse
          }

        } yield res
        response.flatten
      }
      else searchResponse
    }

    scrollHelper(0, max_result_window, Future(Option.empty[String]), Future(Seq.empty[A]))
  }

  /**
   * Extracts search hits asynchronously from Elasticsearch using the scroll API.
   *
   * @param indexName The names of the indices to search in.
   * @param sortField The field by which the results should be sorted.
   * @param total The total number of results to retrieve.
   * @return A future containing a sequence of SearchHit.
   */
  def extractSearchHitAsync(indexName: Seq[String], sortField: String, total: Int): Future[Seq[SearchHit]] = {
    def scrollHelper(from: Int, size: Int, scrollId: Future[Option[String]], searchHits: Future[Seq[SearchHit]]): Future[Seq[SearchHit]] = {
      if (from == 0) {
        val response: Future[Response[SearchResponse]] = IndexClient.scrollQuery(indexName, Some("1m"), max_result_window, sortField)
        val scrollId: Future[Option[String]] = response.map {
          _.result.scrollId
        }

        val newSearchHits: Future[Seq[SearchHit]] = for {
          res <- response
          r = if (res.isSuccess) {
            res.result.hits.hits.toSeq
          } else {
            throw IndexSearchException(s"An error occurred during the elastic search request", Some(res.error.asException))
          }
        } yield r
        scrollHelper(from + size, size, scrollId, newSearchHits)
      }
      else if (from < total) {
        val response: Future[Future[Seq[SearchHit]]] = for {
          sID <- scrollId
          res = if (sID.isDefined) {
            val response: Future[Response[SearchResponse]] = IndexClient.searchScroll(sID.get, None)

            val scrollId: Future[Option[String]] = response.map {
              _.result.scrollId
            }

            val newSearchHits: Future[Seq[SearchHit]] = for {
              res <- response
              r = if (res.isSuccess) {
                res.result.hits.hits.toSeq
              } else {
                throw IndexSearchException(s"An error occurred during the elastic search request", Some(res.error.asException))
              }
            } yield r
            if ((from + size) % 100000 == 0) logger.info(s"${from + size} / $total")
            scrollHelper(from + size, size, scrollId, mergeSearchHitFut(Seq(newSearchHits, searchHits)))
          }
          else {
            searchHits
          }

        } yield res
        response.flatten
      }
      else {
        searchHits
      }
    }
    scrollHelper(0, max_result_window, Future(Option.empty[String]), Future(Array.empty[SearchHit]))
  }

  /**
   * Creates a Source that asynchronously streams objects of type A from Elasticsearch.
   *
   * @param index The names of the indices to search in.
   * @param sortField The field by which the results should be sorted.
   * @tparam A The type of objects to stream.
   * @return A Source of asynchronously fetched objects of type A.
   */
  def sourceFromES[A](index: Seq[String], sortField: String)(implicit read: Reads[A]): Source[Seq[A], NotUsed] = {
    val total = Await.result(IndexClient.count(index), 5.seconds)
    if (total.isSuccess) {
      logger.info(s"${total.result.count.toInt} in ${index.mkString(", ")}")
      Source.future(extractAsync(index, sortField, total.result.count.toInt))
    }
    else {
      logger.error("An error occured during the source creation process", Some(total.error.asException))
      throw IndexSearchException("An error occured during the source creation process", Some(total.error.asException))
    }
  }

  /**
   * Creates a Source that asynchronously streams search hits from Elasticsearch.
   *
   * @param indexName The names of the indices to search in.
   * @param sortField The field by which the results should be sorted.
   * @return A Source of asynchronously fetched SearchHits.
   */
  def searchHitSourceFromES(indexName: Seq[String], sortField: String) = {
    val totalFuture = IndexClient.count(indexName)
    val total = Await.result( totalFuture, 5.seconds)

    if (total.isSuccess) {
      logger.info(s"${total.result.count.toInt} in ${indexName.mkString(", ")}")
      Source.future(scrollHelper(indexName, sortField, total.result.count.toInt))
    }
    else {
      throw IndexSearchException("An error occured during the source creation process", Some(total.error.asException))
      }
    }


  /**
   * Shuts down the Elasticsearch client, cleaning up resources.
   */
  def shutdown() = IndexClient.shutdown()
}