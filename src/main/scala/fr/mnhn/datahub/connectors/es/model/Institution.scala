/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.time.{Instant, LocalDate}
import java.util.Date

case class Institution( attributes: Option[Attributes],created_at: Date, institution_id: Int, code: Option[String], description: Option[String], name: String) extends Persistable {
  override def id(): String = CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/organization/${institution_id}", LocalDate.now().toString)
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String = s"https://www.data.mnhn.fr/data/organization/${institution_id}"

  override def datatype: String = "organization"
}
object Institution {
  implicit val format = Json.format[Institution]

  implicit val defaultValueForInstitution: Default[Institution] = new Default[Institution] {
    override def defaultValue: Institution = Institution(None,
      Date.from(Instant.now): Date, 0: Int, None: Option[String], None: Option[String], "defaultValue": String)
  }
}
