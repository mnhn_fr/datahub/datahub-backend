/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.time.{Instant, LocalDate}
import java.util.Date

case class AttributesCollectionGroup(collected_by: Option[String], // dwciri:collectedBy (mnhn:Person)
                      data_origin: Option[String], // mnhn:dataOrigin (xsd:string)
                      duplicate_count: Option[Float], // mnhn:duplicateCount (xsd:int)
                      duplicate_destination: Option[String], // mnhn:duplicateDestination (xsd:string)
                      event_date: Option[String], // dwc:enventDate (xsd:dateTime)
                      event_date_begin: Option[String], // dwc:EarliestDateCollected (xsd:dateTime)
                      event_date_end: Option[String], // dwc:LatestDateCollected (xsd:dateTime)
                      event_date_verbatim: Option[String],
                      field_number: Option[String],
                      harvest_identifier: Option[String],
                      host: Option[String], // mnhn:host (xsd:string)
                      old_harvest_identifier: Option[String], // mnhn:oldHarvestIdentifier (xsd:string)
                      part_count: Option[Float], // mnhn:partCount (xsd:integer)
                      remarks: Option[String], // dwc:eventRemarks (xsd:string)
                      station_number: Option[String],
                      usage: Option[String] // mnhn:usage (xsd:string)
                     )
object AttributesCollectionGroup {
  implicit lazy val format = Json.format[AttributesCollectionGroup]

  implicit val defaultForAttributesCollectionGroup: Default[AttributesCollectionGroup] = new Default[AttributesCollectionGroup] {
    override def defaultValue: AttributesCollectionGroup = AttributesCollectionGroup(None: Option[String], // dwciri:collectedBy (mnhn:Person)
      None: Option[String], // mnhn:dataOrigin (xsd:string)
      None: Option[Float], // mnhn:duplicateCount (xsd:int)
      None: Option[String], // mnhn:duplicateDestination (xsd:string)
      None: Option[String], // dwc:enventDate (xsd:dateTime)
      None: Option[String], // dwc:EarliestDateCollected (xsd:dateTime)
      None: Option[String], // dwc:LatestDateCollected (xsd:dateTime)
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String], // mnhn:host (xsd:string)
      None: Option[String], // mnhn:oldHarvestIdentifier (xsd:string)
      None: Option[Float], // mnhn:partCount (xsd:integer)
      None: Option[String], // dwc:eventRemarks (xsd:string)
      None: Option[String],
      None: Option[String] // mnhn:usage (xsd:string)
    )
  }
}

case class CollectionGroup(code: Option[String], collection_group_id: Option[Int], coordinates_id: Option[Int], created_at: Date, description: Option[String],
                           geolocation_id: Option[String], is_delete: Option[Boolean], label: Option[String], last_modified_at: Option[Date], name: Option[String],
                           attributes: Option[AttributesCollectionGroup]
                          ) extends Persistable {
  override def id(): String = if (collection_group_id.isDefined) CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/collection-group/${collection_group_id.get}", LocalDate.now().toString) else CryptoUtils.md5sum("")
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String = if (collection_group_id.isDefined) s"https://www.data.mnhn.fr/data/collection-group/${collection_group_id.get}" else ""

  override def datatype: String = "collection-group"
}
object CollectionGroup {
  implicit lazy val format = Json.format[CollectionGroup]

  implicit val defaultForCollectionGroup: Default[CollectionGroup] = new Default[CollectionGroup] {
    override def defaultValue: CollectionGroup = CollectionGroup(None, None: Option[Int], None: Option[Int], Date.from(Instant.now()): Date, None: Option[String],
      None: Option[String], None: Option[Boolean], None: Option[String], None: Option[Date], None: Option[String],
      None: Option[AttributesCollectionGroup] )
  }
}