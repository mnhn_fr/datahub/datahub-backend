/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.time.LocalDate
import java.util.Date

case class TaxonAttributes(
                       authors: Option[String], // mnhn:taxonAuthor
                       authors_infrasp: Option[String], // mnhn:infraspTaxonAuthor
                       authors_infrasp_nominal_taxon: Option[String], // mnhn:infraspNominalTaxonAuthor
                       authors_nominal_taxon: Option[String], // mnhn:nominalTaxonAuthor
                       date_taxon: Option[String],
                       num_family: Option[Int], //mnhn:numFamily, xsd:int
                       num_family_apg: Option[Int], // mnhn:numFamilyAPG, xsd:int
                       num_genus: Option[Int], // mnhn:numGenus, xsd:int
                       remarks: Option[String],
                       scientific_name_authorship: Option[String], // dwc:scientificNameAuthorship
                       scientific_name_with_authorship: Option[String], // dwc:scientificName
                       scientific_name_without_authorship: Option[String] // mnhn:scientificNameWOAuthorship
                     )

object TaxonAttributes {
  implicit lazy val format = Json.format[TaxonAttributes]

  implicit val defaultTaxonAttributes: Default[TaxonAttributes] = new Default[TaxonAttributes] {
    override def defaultValue: TaxonAttributes = TaxonAttributes(None: Option[String], // mnhn:taxonAuthor
      None: Option[String], // mnhn:infraspTaxonAuthor
      None: Option[String], // mnhn:infraspNominalTaxonAuthor
      None: Option[String], // mnhn:nominalTaxonAuthor
      None: Option[String],
      None: Option[Int], //mnhn:numFamily, xsd:int
      None: Option[Int], // mnhn:numFamilyAPG, xsd:int
      None: Option[Int], // mnhn:numGenus, xsd:int
      None: Option[String],
      None: Option[String], // dwc:scientificNameAuthorship
      None: Option[String], // dwc:scientificName
      None: Option[String]
    )
  }
}

case class Ranks(
                `class`: Option[String],
                code: Option[String],
                family: Option[String],
                genus: Option[String],
                infra_class: Option[String],
                infra_order:Option[String],
                infra_specific_epithet:Option[String],
                kingdom: Option[String],
                level: Option[String],
                name: Option[String],
                order: Option[String],
                phylum: Option[String],
                species: Option[String],
                specific_epithet: Option[String],
                rank_id: Option[String],
                sub_class: Option[String],
                sub_family: Option[String],
                sub_genus: Option[String],
                sub_order: Option[String],
                sub_phylum: Option[String],
                super_family: Option[String],
                super_order: Option[String]
                )

object Ranks {
  implicit lazy val format = Json.format[Ranks]

  implicit val defaultVlaueForRanks: Default[Ranks]=new Default[Ranks] {
    override def defaultValue: Ranks = Ranks(
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
        None: Option[String]
    )
  }
}

case class Taxon(
                 attributes: TaxonAttributes,
                 code: Option[String],
                 created_at: Option[Date],
                 description:Option[String],
                 is_deleted: Option[Boolean],
                 deleted_at: Option[Date],
                 label: Option[String],
                 last_modified_at: Option[String],
                 name: Option[String],
                 ranks: Ranks,
                 taxon_id: String) extends Persistable {
  override def id(): String = CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/taxon/${taxon_id}", LocalDate.now().toString)
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String = s"https://www.data.mnhn.fr/data/taxon/${taxon_id}"

  override def datatype: String = "taxon"
}

object Taxon {
  implicit lazy val format = Json.format[Taxon]

  implicit val defaultValueTaxon: Default[Taxon] = new Default[Taxon] {
    override def defaultValue: Taxon = Taxon(TaxonAttributes(None: Option[String], // mnhn:taxonAuthor
      None: Option[String], // mnhn:infraspTaxonAuthor
      None: Option[String], // mnhn:infraspNominalTaxonAuthor
      None: Option[String], // mnhn:nominalTaxonAuthor
      None: Option[String],
      None: Option[Int], //mnhn:numFamily, xsd:int
      None: Option[Int], // mnhn:numFamilyAPG, xsd:int
      None: Option[Int], // mnhn:numGenus, xsd:int
      None: Option[String],
      None: Option[String], // dwc:scientificNameAuthorship
      None: Option[String], // dwc:scientificName
      None: Option[String]
    ),
      None,
      None: Option[Date],
      None: Option[String],
      None: Option[Boolean],
      None: Option[Date],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      Ranks(
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String],
        None: Option[String]
      ),
      "defaultValue": String
    )
  }
}
