/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.Geolocation
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * Utility object for mapping `Geolocation` instances to RDF models or subgraphs.
 *
 * This object provides methods for creating RDF representations of geolocations, including
 * their unique identifiers, attributes, and hierarchical metadata. It also includes implicit
 * implementations to convert `Geolocation` objects and sequences of them into RDF models and subgraphs.
 */
object GeolocationRdfMapper {
  /**
   * Builds an RDF model for a given `Geolocation` instance.
   *
   * This method adds RDF triples representing the specified `Geolocation` to the provided `ModelBuilder`.
   * Each geolocation is represented as a `mnhn:Geolocation` and includes:
   * - A unique IRI based on its ID.
   * - Creation date and location ID metadata.
   * - Attributes such as continent, country, locality, and remarks (if defined).
   *
   * @param model The `ModelBuilder` to which RDF triples are added.
   * @param geolocation The `Geolocation` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata.
   */
  def modelBuilding(model: ModelBuilder, geolocation: Geolocation, date: String) = {
    if (geolocation.geolocation_id.isDefined) {
      val geolocationIRI = createIRI(s"https://www.data.mnhn.fr/data/geolocation/${geolocation.geolocation_id.get}")
      model.subject(geolocationIRI)
        .add(RDF.TYPE, "mnhn:Geolocation")

      model.addTripleIfPresent(geolocationIRI, "dcterms:created", geolocation.created_at)
      model.addTripleIfPresent(geolocationIRI, "dwc:locationID", geolocation.geolocation_id)

      if (geolocation.attributes.isDefined) {
        model.addTripleIfPresent(geolocationIRI, "dwc:continent", geolocation.attributes.get.continent)
        model.addTripleIfPresent(geolocationIRI, "dwc:country", geolocation.attributes.get.country)
        model.addTripleIfPresent(geolocationIRI, "dwc:countryCode", geolocation.attributes.get.country_code)
        model.addTripleIfPresent(geolocationIRI, "dwc:island", geolocation.attributes.get.island)
        model.addTripleIfPresent(geolocationIRI, "dwc:islandGroup", geolocation.attributes.get.island_group)

        model.addTripleIfPresent(geolocationIRI, "dwc:locality", geolocation.attributes.get.locality)
        model.addTripleIfPresent(geolocationIRI, "dwc:municipality", geolocation.attributes.get.municipality)
        model.addTripleIfPresent(geolocationIRI, "mnhn:sea", geolocation.attributes.get.sea)
        model.addTripleIfPresent(geolocationIRI, "dwc:locationRemarks", geolocation.attributes.get.remarks)

        model.addTripleIfPresent(geolocationIRI, "dwc:county", geolocation.attributes.get.country)
        model.addTripleIfPresent(geolocationIRI, "dwc:stateProvince", geolocation.attributes.get.state_province)
        model.addTripleIfPresent(geolocationIRI, "mnhn:ocean", geolocation.attributes.get.ocean)
        model.addTripleIfPresent(geolocationIRI, "mnhn:originalName", geolocation.attributes.get.original_name)
      }
    }
  }

  /**
   * Implicit conversion from a sequence of `Geolocation` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `Geolocation` instances
   * into an RDF model. Each geolocation in the sequence is mapped to RDF triples
   * and included in the model.
   *
   * @example
   * {{{
   * val geolocations: Seq[Geolocation] = ...
   * val rdfModel: Model = GeolocationRdfMapper.geolocationsToRdfModel.toRdfModel(geolocations)
   * }}}
   */
  implicit val geolocationsToRdfModel: ToRdfModel[Seq[Geolocation]] = new ToRdfModel[Seq[Geolocation]] {
    override def toRdfModel(objs: Seq[Geolocation]): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }
  /**
   * Implicit conversion from a single `Geolocation` object to an RDF model.
   *
   * This implicit implementation converts a single `Geolocation` instance into an RDF model.
   * The resulting RDF model includes triples representing the geolocation's metadata and attributes.
   *
   * @example
   * {{{
   * val geolocation: Geolocation = ...
   * val rdfModel: Model = GeolocationRdfMapper.geolocationToRdfModel.toRdfModel(geolocation)
   * }}}
   */
  implicit val geolocationToRdfModel: ToRdfModel[Geolocation] = new ToRdfModel[Geolocation] {
    override def toRdfModel(obj: Geolocation): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion from a `Geolocation` object to a subgraph.
   *
   * This implicit implementation converts a `Geolocation` instance into a `Subgraph`.
   * The subgraph includes:
   * - The geolocation's unique identifier, resource URI, and graph URI.
   * - Serialized RDF data in TriG format.
   * - Metadata such as the datatype, creation date, and a cryptographic hash of the serialized model.
   *
   * @example
   * {{{
   * val geolocation: Geolocation = ...
   * val subgraph: Subgraph = GeolocationRdfMapper.geolocationToSubgraph.toSubgraph(geolocation)
   * }}}
   */
  implicit val geolocationToSubgraph: ToSubgraph[Geolocation] = new ToSubgraph[Geolocation] {
    override def toSubgraph(obj: Geolocation): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}