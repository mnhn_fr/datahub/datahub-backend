/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.time.LocalDate
import java.util.Date

case class IdentificationAttributes(date_identified: Option[String], // dwc:dateIdentified
                      date_identified_begin: Option[String],
                      date_identified_end: Option[String],
                      identification_qualifier: Option[String], // dwc:identificationQualifier
                      identified_by: Option[String], // dwciri:identifiedBy
                      is_current: Boolean, // mnhn:isCurrent
                      remarks: Option[String], // mnhn:typeStatusRemarks (xsd:string)
                      type_status: Option[String], // dwc:typeStatus
                      type_status_remarks: Option[String],
                      verbatim_date_identified: Option[String]
                     )

object IdentificationAttributes {
  implicit lazy val format = Json.format[IdentificationAttributes]

  implicit val defaultForCollection: Default[IdentificationAttributes] = new Default[IdentificationAttributes] {
    override def defaultValue: IdentificationAttributes = IdentificationAttributes(None: Option[String], // dwc:dateIdentified
      None: Option[String],
      None: Option[String],
      None: Option[String], // dwc:identificationQualifier
      None: Option[String], // dwciri:identifiedBy
      true: Boolean, // mnhn:isCurrent
      None: Option[String], // mnhn:typeStatusRemarks (xsd:string)
      None: Option[String], // dwc:typeStatus
      None: Option[String],
      None: Option[String]
    )
  }
}

case class Identification(code: Option[String],
                           created_at: Option[String],
                          deleted_at: Option[String],
                          description: Option[String],
                          identification_id: Option[String], // dwc:identificationID (xsd:string)
                          is_deleted: Option[Boolean],
                          label: Option[String],
                          last_modified_at: Option[String],
                          material_id: Option[String], // mnhn:hasMaterial
                          name: Option[String],
                          taxon_id: Option[String], // dwciri:toTaxon
                          attributes: IdentificationAttributes
                         ) extends Persistable {
  override def id(): String = if (identification_id.isDefined) CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/identification/${identification_id.get}", LocalDate.now().toString) else CryptoUtils.md5sum("")
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String = if (identification_id.isDefined) s"https://www.data.mnhn.fr/data/identification/${identification_id.get}" else ""

  override def datatype: String = "identification"
}

object Identification {
  implicit lazy val format = Json.format[Identification]

  implicit val defaultForIdentification: Default[Identification] = new Default[Identification] {
    override def defaultValue: Identification = Identification(None,
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String], // dwc:identificationID (xsd:string)
      None: Option[Boolean],
      None: Option[String],
      None: Option[String],
      None: Option[String], // mnhn:hasMaterial
      None: Option[String],
      None: Option[String],
      IdentificationAttributes(None: Option[String], // dwc:dateIdentified
        None: Option[String],
        None: Option[String],
        None: Option[String], // dwc:identificationQualifier
        None: Option[String], // dwciri:identifiedBy
        true: Boolean, // mnhn:isCurrent
        None: Option[String], // mnhn:typeStatusRemarks (xsd:string)
        None: Option[String], // dwc:typeStatus
        None: Option[String],
        None: Option[String]
      )
    )
  }
}

