/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.Done
import akka.actor.ActorSystem
import akka.stream.alpakka.slick.scaladsl.{Slick, SlickSession}
import akka.stream.scaladsl.{Flow, Sink}
import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.{GraphManagementRepository, Insertable}
import com.mnemotix.synaptix.core.utils.RandomNameGenerator
import com.mnemotix.synaptix.rdf.client.models.RDFClientWriteConnection
import com.typesafe.scalalogging.LazyLogging
import fr.mnhn.datahub.connectors.RDFSerializer
import org.eclipse.rdf4j.model.Model
import slick.dbio.DBIO

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.{ExecutionContext, Future}

/**
 * A class that handles RDF data dumping to a file or a database.
 *
 * This class provides functionality to dump RDF models into files and, in the future, to insert them into a database.
 * It uses an atomic counter for naming output files and supports parallelism for database inserts.
 *
 * @param system The ActorSystem used for managing actors in asynchronous operations.
 * @param executionContext The ExecutionContext used for executing asynchronous tasks.
 */
class RdfDumper(implicit system: ActorSystem,
                //session: SlickSession,
                executionContext: ExecutionContext) extends LazyLogging {

  /** Atomic counter used to generate unique names for output files. */

  val i = new AtomicInteger()

  /**
   * Initializes the RdfDumper by setting up necessary resources (such as database tables, if available).
   *
   * Currently, this method does nothing, but it is a placeholder for future database setup when the GraphManagementRepository is available.
   *
   * @return A Future representing the completion of the initialization process.
   */
  // todo when we have the db
 // val graphManagementRepository = new GraphManagementRepository()
  def init(): Future[Unit] = {
  //  session.db.run(graphManagementRepository.createTables)
    Future.unit
  }

  /**
   * Returns a Sink that dumps RDF models to files.
   * The file names are based on a prefix name (if provided) and a unique incrementing counter.
   *
   * @param dirLoc The directory location where the RDF models will be dumped.
   * @param prefixName An optional prefix for the file name. If not provided, a random name is generated.
   * @return A Sink that processes a stream of RDF models and writes them to files.
   */
  def toFileDumper(dirLoc: String, prefixName: Option[String]=None): Sink[Model, Future[Done]] = {
    Sink.foreach[Model](model => {
//      val model = RDFSerializer.merge(m)
      RDFSerializer.toFile(model, s"${prefixName.getOrElse(RandomNameGenerator.haiku)}-${i.getAndIncrement()}", dirLoc)
    })
  }

  def toGraphDB()(implicit conn: RDFClientWriteConnection) = {
    Sink.foreach[Model](model => {
      RDFSerializer.toGraphDB(model)
    })
  }

  /**
   *
   * @param paral parallelism
   * @param f function that insert in database
   * @tparam T type of the object that we are inserting
   * @return
   */

  /*
  def postgresDumper[T](paral: Option[Int] = None)(implicit insertable: Insertable[T]): Sink[Seq[T], Future[Done]] = {
    Slick.sink(parallelism = paral.getOrElse(4), (group: Seq[T]) => {
      logger.info(s"Received ${group.size} elements for processing.")
      group.map(insertable.insertOrUpdate(_)).reduceLeft(_.andThen(_))
    })
  }

   */
}