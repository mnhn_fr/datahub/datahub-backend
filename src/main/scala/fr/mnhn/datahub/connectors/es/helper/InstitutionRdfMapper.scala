/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.Institution
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * Utility object for mapping `Institution` instances to RDF models or subgraphs.
 *
 * This object provides methods for creating RDF representations of institutions, including their metadata
 * such as names, codes, and descriptions. It also includes implicit implementations to convert `Institution`
 * objects and sequences of them into RDF models and subgraphs.
 */
object InstitutionRdfMapper {
  /**
   * Builds an RDF model for a given `Institution` instance.
   *
   * This method adds RDF triples representing the specified `Institution` to the provided `ModelBuilder`.
   * Each institution is represented as a `foaf:Organization` and includes:
   * - A unique IRI based on its `institution_id`.
   * - Attributes such as name (`foaf:name`), code (`dwc:institutionCode`), and description (`dcterms:description`).
   *
   * @param model The `ModelBuilder` to which RDF triples are added.
   * @param institution The `Institution` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata.
   */
  def modelBuilding(model: ModelBuilder, institution: Institution, date: String) = {
    val institutionIRI = createIRI(s"https://www.data.mnhn.fr/data/organization/${institution.institution_id}")
    model.subject(institutionIRI)
      .add(RDF.TYPE, "foaf:Organization")
      .add("dwc:institutionID", institution.institution_id)
      .add("foaf:name", institution.name)

    model.addTripleIfPresent(institutionIRI, "dwc:institutionCode", institution.code)
    model.addTripleIfPresent(institutionIRI, "dcterms:description", institution.description)
  }

  /**
   * Implicit conversion from a sequence of `Institution` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `Institution` instances into an RDF model.
   * Each institution in the sequence is mapped to RDF triples and included in the model.
   *
   * @example
   * {{{
   * val institutions: Seq[Institution] = ...
   * val rdfModel: Model = InstitutionRdfMapper.institutionsToRdfModel.toRdfModel(institutions)
   * }}}
   */
  implicit val institutionsToRdfModel: ToRdfModel[Seq[Institution]] = new ToRdfModel[Seq[Institution]] {
    override def toRdfModel(objs: Seq[Institution]): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()

    }
  }

  /**
   * Implicit conversion from a single `Institution` object to an RDF model.
   *
   * This implicit implementation converts a single `Institution` instance into an RDF model.
   * The resulting RDF model includes triples representing the institution's metadata.
   *
   * @example
   * {{{
   * val institution: Institution = ...
   * val rdfModel: Model = InstitutionRdfMapper.institutionToRdfModel.toRdfModel(institution)
   * }}}
   */
  implicit val institutionToRdfModel: ToRdfModel[Institution] = new ToRdfModel[Institution] {
    override def toRdfModel(obj: Institution): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }


  /**
   * Implicit conversion from an `Institution` object to a subgraph.
   *
   * This implicit implementation converts an `Institution` instance into a `Subgraph`.
   * The subgraph includes:
   * - The institution's unique identifier, resource URI, and graph URI.
   * - Serialized RDF data in TriG format.
   * - Metadata such as the datatype, creation date, and a cryptographic hash of the serialized model.
   *
   * @example
   * {{{
   * val institution: Institution = ...
   * val subgraph: Subgraph = InstitutionRdfMapper.institutionToSubgraph.toSubgraph(institution)
   * }}}
   */
  implicit val institutionToSubgraph: ToSubgraph[Institution] = new ToSubgraph[Institution] {
    override def toSubgraph(obj: Institution): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}