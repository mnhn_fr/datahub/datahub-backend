/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.connectors.RDFSerializer.{DATEIRIDATATYPE, addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.Identification
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.net.URI
import java.time.LocalDate

/**
 * Utility object for mapping `Geolocation` instances to RDF models or subgraphs.
 *
 * This object provides methods for creating RDF representations of geolocations, including
 * their unique identifiers, attributes, and hierarchical metadata. It also includes implicit
 * implementations to convert `Geolocation` objects and sequences of them into RDF models and subgraphs.
 */
object IdentificationRdfMapper {
  /**
   * Builds an RDF model for a given `Geolocation` instance.
   *
   * This method adds RDF triples representing the specified `Geolocation` to the provided `ModelBuilder`.
   * Each geolocation is represented as a `mnhn:Geolocation` and includes:
   * - A unique IRI based on its ID.
   * - Creation date and location ID metadata.
   * - Attributes such as continent, country, locality, and remarks (if defined).
   *
   * @param model The `ModelBuilder` to which RDF triples are added.
   * @param geolocation The `Geolocation` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata.
   *
   *              "created_at" : "2023-09-27T12:51:00.245918",
   *              "identification_id" : "432939af-3e5e-5877-bb5b-2e485ba24816",
   *              "material_id" : "15ced028-5c6d-5b68-a810-d53dc53d302c",
   *              "taxon_id" : "45e6da38-37f2-596a-89fa-eac4ead95141",
   *              "is_current" : true,
   *              "date_identified" : "1936",
   *              "identified_by" : "H. Perrier de la Bâthie"
   *
   *
   *              created_at: String,
   *              identification_id: String, // dwc:identificationID (xsd:string)
   *              material_id: String, // mnhn:hasMaterial
   *              taxon_id: String, // dwciri:toTaxon
   *              is_current: Boolean, // mnhn:isCurrent
   *              date_identified: String, // dwc:dateIdentified
   *              identified_by: Seq[String], // dwciri:identifiedBy
   *              remarks: Option[String], // mnhn:typeStatusRemarks (xsd:string)
   *              type_status: Option[String], // dwc:typeStatus
   *              identification_qua: Option[String] // dwc:identificationQualifier
   *
   */
  def modelBuilding(model: ModelBuilder, identification: Identification, date: String) = {
    if (identification.identification_id.isDefined) {
      val identificationUri = createIRI(s"https://www.data.mnhn.fr/data/identification/${identification.identification_id.get}")
      model.subject(identificationUri)
        .add(RDF.TYPE, "mnhn:Identification")
        .add("mnhn:isCurrent", identification.attributes.is_current)
      model.addTripleIfPresent(identificationUri, "mnhn:hasMaterial",identification.material_id.map(material_id => new URI(s"https://www.data.mnhn.fr/data/material/$material_id"))  )
      model.addTripleIfPresent(identificationUri, "dwciri:toTaxon", identification.taxon_id.map(taxon_id => new URI(s"https://www.data.mnhn.fr/data/taxon/$taxon_id")) )
      val materialUri = identification.material_id.map(material_id =>  createIRI(s"https://www.data.mnhn.fr/data/material/${material_id}"))
      val taxonUri =  identification.taxon_id.map(taxon_id => createIRI(s"https://www.data.mnhn.fr/data/taxon/${taxon_id}"))
      if (materialUri.isDefined) {
        model.subject(materialUri.get)
          .add(RDF.TYPE, "mnhn:Material")
      }

      if (taxonUri.isDefined) {
        model.subject(taxonUri.get)
          .add(RDF.TYPE, "mnhn:Taxon")
      }

      model.addTripleIfPresent(identificationUri, "dwc:identification", identification.identification_id)
      model.addTripleIfPresent(identificationUri, "dcterms:created", identification.created_at)
      model.addTripleIfPresent(identificationUri, "dwc:dateIdentified", identification.attributes.date_identified)
      model.addTripleIfPresent(identificationUri, "mnhn:verbatimDateIdentified", identification.attributes.verbatim_date_identified)
      model.addTripleIfPresent(identificationUri, "dwc:typeStatus", identification.attributes.type_status)
      model.addTripleIfPresent(identificationUri, "dwc:identificationQualifier", identification.attributes.identification_qualifier)
      model.addTripleIfPresent(identificationUri, "dwc:identificationRemarks", identification.attributes.remarks)

      /*
      mnhn:identificationOrder 1; # from? (pas dans mapping)
       */
      identification.attributes.identified_by.map { person =>
        val personIri = createIRI(s"https://www.data.mnhn.fr/data/person/${StringUtils.slugify(person.trim)}")
        model.subject(personIri).add(RDF.TYPE, "mnhn:Person")
        model.subject(identificationUri)
          .add("dwciri:identifiedBy", personIri)
        model.subject(personIri)
          .add("foaf:name", person)
      }
    }
  }

  /**
   * Implicit conversion from a sequence of `Geolocation` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `Geolocation` instances
   * into an RDF model. Each geolocation in the sequence is mapped to RDF triples
   * and included in the model.
   *
   * @example
   * {{{
   * val geolocations: Seq[Geolocation] = ...
   * val rdfModel: Model = GeolocationRdfMapper.geolocationsToRdfModel.toRdfModel(geolocations)
   * }}}
   */
  implicit val identificationsToRdfModel: ToRdfModel[Seq[Identification]] = new ToRdfModel[Seq[Identification]] {
    override def toRdfModel(objs: Seq[Identification]): Model = {
      val builder = new ModelBuilder().namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap)
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }

  /**
   * Implicit conversion from a single `Geolocation` object to an RDF model.
   *
   * This implicit implementation converts a single `Geolocation` instance into an RDF model.
   * The resulting RDF model includes triples representing the geolocation's metadata and attributes.
   *
   * @example
   * {{{
   * val geolocation: Geolocation = ...
   * val rdfModel: Model = GeolocationRdfMapper.geolocationToRdfModel.toRdfModel(geolocation)
   * }}}
   */
  implicit val identificationToRdfModel: ToRdfModel[Identification] = new ToRdfModel[Identification] {
    override def toRdfModel(obj: Identification): Model = {
      val builder = new ModelBuilder().namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap)
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion from a `Geolocation` object to a subgraph.
   *
   * This implicit implementation converts a `Geolocation` instance into a `Subgraph`.
   * The subgraph includes:
   * - The geolocation's unique identifier, resource URI, and graph URI.
   * - Serialized RDF data in TriG format.
   * - Metadata such as the datatype, creation date, and a cryptographic hash of the serialized model.
   *
   * @example
   * {{{
   * val geolocation: Geolocation = ...
   * val subgraph: Subgraph = GeolocationRdfMapper.geolocationToSubgraph.toSubgraph(geolocation)
   * }}}
   */
  implicit val identificationToSubgraph: ToSubgraph[Identification] = new  ToSubgraph[Identification] {
    override def toSubgraph(obj: Identification): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}