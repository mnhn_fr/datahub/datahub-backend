/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.CollectionEvent
import fr.mnhn.datahub.connectors.utils.DateUtils
import fr.mnhn.datahub.connectors.utils.NameUtils.splitByDelimiters
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * A utility object for mapping `CollectionEvent` instances to RDF models or subgraphs.
 *
 * This object provides methods to build RDF representations of collection events,
 * including their attributes, relationships, and associated metadata. It also includes
 * implicit implementations for converting `CollectionEvent` objects and sequences
 * of them into RDF models and subgraphs.
 */
object CollectionEventRdfMapper {
  /**
   * Builds an RDF model for a given `CollectionEvent`.
   *
   * This method constructs triples for the specified `CollectionEvent` and adds them
   * to the provided `ModelBuilder`. The RDF representation includes event attributes
   * such as event dates, geolocations, collected by, and additional metadata.
   *
   * @param model The `ModelBuilder` to which RDF triples will be added.
   * @param collectionEvent The `CollectionEvent` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata.
   */
  def modelBuilding(model: ModelBuilder, collectionEvent: CollectionEvent, date: String) = {

    val collectionEventIRI = createIRI(s"https://www.data.mnhn.fr/data/collection-event/${collectionEvent.collection_event_id}")
    model.subject(collectionEventIRI)
      .add(RDF.TYPE, "mnhn:CollectionEvent")

    model.subject(collectionEventIRI).add("dwc:eventID", collectionEvent.collection_event_id)
    model.addTripleIfPresent(collectionEventIRI, "dwc:eventDate", collectionEvent.attributes.event_date)

    model.addTripleIfPresent(collectionEventIRI, "dwc:EarliestDateCollected", collectionEvent.attributes.event_date_begin)
    model.addTripleIfPresent(collectionEventIRI, "dwc:LatestDateCollected", collectionEvent.attributes.event_date_end)
    model.addTripleIfPresent(collectionEventIRI, "dwc:verbatimEventDate", collectionEvent.attributes.event_date_verbatim)
    model.addTripleIfPresent(collectionEventIRI, "dwc:fieldNumber", collectionEvent.attributes.field_number)


    model.addTripleIfPresent(collectionEventIRI, "dcterms:created", collectionEvent.created_at)

    model.addTripleIfPresent(collectionEventIRI, "mnhn:dataOrigin", collectionEvent.attributes.data_origin)
    model.addTripleIfPresent(collectionEventIRI, "mnhn:duplicateCount", collectionEvent.attributes.duplicate_count.map(_.toInt))
    model.addTripleIfPresent(collectionEventIRI, "mnhn:duplicateDestination", collectionEvent.attributes.duplicate_destination)
    model.addTripleIfPresent(collectionEventIRI, "mnhn:host", collectionEvent.attributes.host)

    model.addTripleIfPresent(collectionEventIRI, "mnhn:harvestIdentifier", collectionEvent.attributes.harvest_identifier)
    model.addTripleIfPresent(collectionEventIRI, "mnhn:oldHarvestIdentifier", collectionEvent.attributes.old_harvest_identifier)

    model.addTripleIfPresent(collectionEventIRI, "mnhn:partCount", collectionEvent.attributes.part_count.map(_.toInt))
    model.addTripleIfPresent(collectionEventIRI, "dwc:eventRemarks", collectionEvent.attributes.remarks)
    model.addTripleIfPresent(collectionEventIRI, "mnhn:stationNumber", collectionEvent.attributes.station_number)

    model.addTripleIfPresent(collectionEventIRI, "mnhn:usage", collectionEvent.attributes.usage)


    model.addTripleIfPresent(collectionEventIRI, "dwc:LatestDateCollected", collectionEvent.attributes.event_date_end)
    model.addTripleIfPresent(collectionEventIRI, "dwc:EarliestDateCollected", collectionEvent.attributes.event_date_begin)
    model.addTripleIfPresent(collectionEventIRI, "dwc:eventRemarks", collectionEvent.attributes.remarks)




    collectionEvent.attributes.event_date.foreach { event_date =>
      model.subject(collectionEventIRI)
        .add("dwc:enventDate", event_date)
    }

    collectionEvent.attributes.event_date_begin.foreach { event_date_begin =>
      model.subject(collectionEventIRI)
        .add("dwc:EarliestDateCollected", event_date_begin)
    }

    collectionEvent.attributes.event_date_end.foreach { event_date_end =>
      model.subject(collectionEventIRI)
        .add("dwc:LatestDateCollected", event_date_end)
    }

    collectionEvent.geolocation_id.foreach { geolocation_id =>
      val geolocationIri = createIRI(s"https://www.data.mnhn.fr/data/geolocation/$geolocation_id")
      model.subject(collectionEventIRI).add("mnhn:hasGeolocation",  geolocationIri)
      model.subject(geolocationIri).add(RDF.TYPE, "mnhn:Geolocation")
    }

    val delimiters = List(",", "&", ";")
    collectionEvent.attributes.collected_by.foreach { collected_by =>
      val resultArray = splitByDelimiters(collected_by, delimiters)
      resultArray.foreach { rsl =>
        val uriPerson = createIRI(s"https://www.data.mnhn.fr/data/person/${StringUtils.slugify(rsl.trim)}")
        model.subject(collectionEventIRI).add("dwciri:recordedBy", uriPerson)
        model.subject(uriPerson).add(RDF.TYPE, "mnhn:Person")
          .add("foaf:name", rsl)
          .add("prov:wasDerivedFrom", collected_by)
      }
    }
  }

  /**
   * Implicit conversion from a sequence of `CollectionEvent` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `CollectionEvent` instances
   * into an RDF model. Each event is processed and represented within the resulting model.
   */
  implicit val collectionEventsToRdfModel: ToRdfModel[Seq[CollectionEvent]] = new ToRdfModel[Seq[CollectionEvent]] {
    override def toRdfModel(objs: Seq[CollectionEvent]): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }

  /**
   * Implicit conversion from a single `CollectionEvent` object to an RDF model.
   *
   * This implicit implementation converts a single `CollectionEvent` instance into an RDF model.
   * The resulting model includes all relevant attributes and relationships of the event.
   */
  implicit val collectionEventToRdfModel: ToRdfModel[CollectionEvent] = new ToRdfModel[CollectionEvent] {
    override def toRdfModel(obj: CollectionEvent): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion from a `CollectionEvent` object to a subgraph.
   *
   * This implicit implementation converts a `CollectionEvent` instance into a `Subgraph`.
   * The subgraph includes metadata such as the event's unique identifier, resource URI,
   * graph URI, and serialized model string in TriG format.
   */
  implicit val collectionEventToSubgraph: ToSubgraph[CollectionEvent] = new ToSubgraph[CollectionEvent] {
    override def toSubgraph(obj: CollectionEvent): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}