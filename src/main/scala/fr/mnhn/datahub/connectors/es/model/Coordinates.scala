/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import ai.x.play.json.Jsonx
import ai.x.play.json.Encoders._
import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.{Format, JsError, JsResult, JsValue, Json}

import java.time.LocalDate
import java.util.Date

sealed trait GeoJsonGeometry

case class GeoJsonLineString(coordinates: List[List[Double]]) extends GeoJsonGeometry
case class GeoJsonPoint(coordinates: List[Double]) extends GeoJsonGeometry

object GeoJsonGeometry {
  implicit val geoJsonLineStringFormat  = Json.format[GeoJsonLineString]
  implicit val geoJsonPointFormat: Format[GeoJsonPoint] = Json.format[GeoJsonPoint]

  implicit val geoJsonGeometryFormat: Format[GeoJsonGeometry] = new Format[GeoJsonGeometry] {
    def reads(json: JsValue): JsResult[GeoJsonGeometry] = {
      (json \ "type").validate[String].flatMap {
        case "LineString" => json.validate[GeoJsonLineString]
        case "Point" => json.validate[GeoJsonPoint]
        case _ => JsError("Invalid geometry type")
      }
    }

    def writes(geometry: GeoJsonGeometry): JsValue = geometry match {
      case lineString: GeoJsonLineString => Json.toJson(lineString)
      case point: GeoJsonPoint => Json.toJson(point)
    }
  }

}

case class AttributesCoordinates(altitude: Option[Float], //geo:alt (xsd:float)
                      altitude_accuracy: Option[String], //
                      altitude_max: Option[Float],
                      altitude_min: Option[Float],
                      altitude_remarks: Option[Float],
                      altitude_unit: Option[String],
                      coordinates_accuracy: Option[String],
                      coordinates_estimated: Option[Boolean],
                      coordinates_geojson: Option[GeoJsonGeometry],
                      coordinates_source: Option[String],
                      coordinates_verbatim: Option[String],
                      depth: Option[Float],
                      depth_max: Option[Float],
                      depth_min: Option[Float],
                      latitude: Option[Float],
                      latitude_degrees: Option[Float],
                      latitude_minutes: Option[Float],
                      latitude_orientation: Option[String],
                      latitude_seconds: Option[Float],
                      longitude: Option[Float],
                      longitude_degrees: Option[Float],
                      longitude_minutes: Option[Float],
                      longitude_orientation: Option[String],
                      longitude_seconds: Option[Float]
                     )

object AttributesCoordinates {
  implicit val attributesClassFormatter = Jsonx.formatCaseClassUseDefaults[AttributesCoordinates]

  implicit val defaultForAttributesCoordinates = new Default[AttributesCoordinates] {
    override def defaultValue: AttributesCoordinates = AttributesCoordinates(None: Option[Float], //geo:alt (xsd:float)
      None: Option[String], //
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[String],
      None: Option[String],
      None: Option[Boolean],
      None,
      None: Option[String],
      None: Option[String],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[String],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[String],
      None: Option[Float])
  }
}

case class Coordinates(
                    attributes: AttributesCoordinates,
                    code: Option[String],
                    coordinates_id: Option[String],
                    created_at: Option[Date],
                    description: Option[String],
                    is_deleted: Option[Boolean],
                    label: Option[String],
                    last_modified_at: Option[Date],
                    name: Option[String]
                   ) extends Persistable {
  override def id(): String = if (coordinates_id.isDefined) CryptoUtils.md5sum(s"https://www.datahub.mnhn.fr/data/geometry/${coordinates_id.get}", LocalDate.now().toString) else CryptoUtils.md5sum("")
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String =  if (coordinates_id.isDefined) s"https://www.datahub.mnhn.fr/data/geometry/${coordinates_id.get}" else ""

  override def datatype: String = "geometry"
}
object Coordinates {
 implicit val coordinatesClassFormatter = Jsonx.formatCaseClassUseDefaults[Coordinates]

  implicit val defaultForCoordinates = new Default[Coordinates] {
    override def defaultValue: Coordinates = {
    Coordinates(AttributesCoordinates(None: Option[Float], //geo:alt (xsd:float)
      None: Option[String], //
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[String],
      None: Option[String],
      None: Option[Boolean],
      None,
      None: Option[String],
      None: Option[String],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[String],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[Float],
      None: Option[String],
      None: Option[Float]),
      None,
      None,
      None: Option[Date],
      None: Option[String],
      None: Option[Boolean],
      None: Option[String],
      None: Option[Date],
      None: Option[String]
     )
    }
  }
}