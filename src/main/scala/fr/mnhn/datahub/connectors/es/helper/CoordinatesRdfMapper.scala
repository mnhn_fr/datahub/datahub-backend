/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.Coordinates
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * Utility object for mapping `Coordinates` instances to RDF models or subgraphs.
 *
 * This object provides methods to construct RDF representations of geographic coordinates,
 * including their attributes such as latitude, longitude, altitude, depth, precision, and source.
 * It also includes implicit implementations for converting `Coordinates` objects and sequences
 * of them into RDF models and subgraphs.
 */
object CoordinatesRdfMapper {
  /**
   * Builds an RDF model for a given `Coordinates` instance.
   *
   * This method creates RDF triples to represent the specified `Coordinates` and adds them
   * to the provided `ModelBuilder`. The RDF representation includes:
   * - The coordinates' unique ID.
   * - Geographic attributes such as latitude, longitude, precision, and source.
   * - Altitude and depth details, including minimum, maximum, accuracy, and units.
   * - Other metadata such as creation date and estimated coordinates flag.
   *
   * @param model The `ModelBuilder` to which RDF triples will be added.
   * @param coordinates The `Coordinates` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata.
   */
  def modelBuilding(model: ModelBuilder, coordinates: Coordinates, date: String) = {
    if (coordinates.coordinates_id.isDefined) {
      val geometryIRI = createIRI(s"https://www.data.mnhn.fr/data/geometry/${coordinates.coordinates_id.get}")
      model.subject(geometryIRI)
        .add(RDF.TYPE, "geosparql:Geometry")
      model.addTripleIfPresent(geometryIRI, "dcterms:created", coordinates.created_at)
      model.addTripleIfPresent(geometryIRI, "dwc:decimalLatitude", coordinates.attributes.latitude)
      model.addTripleIfPresent(geometryIRI, "dwc:decimalLongitude", coordinates.attributes.longitude)
      model.addTripleIfPresent(geometryIRI, "dwc:coordinatePrecision", coordinates.attributes.coordinates_accuracy)
      model.addTripleIfPresent(geometryIRI, "dwc:georeferenceSources", coordinates.attributes.coordinates_source)
      model.addTripleIfPresent(geometryIRI, "dwc:verbatimCoordinates", coordinates.attributes.coordinates_verbatim)
      model.addTripleIfPresent(geometryIRI, "dwc:coordinatePrecision", coordinates.attributes.coordinates_accuracy)
      model.addTripleIfPresent(geometryIRI, "dwc:georeferenceSources", coordinates.attributes.coordinates_source)
      model.addTripleIfPresent(geometryIRI, "mnhn:coordinatesEstimated", coordinates.attributes.coordinates_estimated)
      //model.addTripleIfPresent(geometryIRI, "geosparql:asGeoJSON",  coordinates.attributes.coordinates_geojson.map(_.coordinates.mkString(", ")))
      model.addTripleIfPresent(geometryIRI, "dwc:minimumElevationInMeters", coordinates.attributes.altitude_min)
      model.addTripleIfPresent(geometryIRI, "dwc:maximumElevationInMeters", coordinates.attributes.altitude_max)
      model.addTripleIfPresent(geometryIRI, "mnhn:altitudeUnit", coordinates.attributes.altitude_unit)
      model.addTripleIfPresent(geometryIRI, "mnhn:altitudeAccuracy", coordinates.attributes.altitude_accuracy)
      model.addTripleIfPresent(geometryIRI, "mnhn:altitudeRemarks", coordinates.attributes.altitude_remarks)
      model.addTripleIfPresent(geometryIRI, "geo:alt", coordinates.attributes.altitude)
      model.addTripleIfPresent(geometryIRI, "dwc:verbatimDepth", coordinates.attributes.depth)
      model.addTripleIfPresent(geometryIRI, "dwc:minimumDepthInMeters", coordinates.attributes.depth_min)
      model.addTripleIfPresent(geometryIRI, "dwc:maximumDepthInMeters", coordinates.attributes.depth_max)
    }
  }

  /**
   * Implicit conversion from a sequence of `Coordinates` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `Coordinates` instances
   * into an RDF model. Each coordinates object in the sequence is processed and represented within the model.
   *
   * @example
   * {{{
   * val coordinatesSeq: Seq[Coordinates] = ...
   * val rdfModel: Model = CoordinatesRdfMapper.coordinatesToRdfModel.toRdfModel(coordinatesSeq)
   * }}}
   */
  implicit val coordinatesToRdfModel: ToRdfModel[Seq[Coordinates]] = new ToRdfModel[Seq[Coordinates]] {
    override def toRdfModel(objs: Seq[Coordinates]): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }

  /**
   * Implicit conversion from a single `Coordinates` object to an RDF model.
   *
   * This implicit implementation converts a single `Coordinates` instance into an RDF model.
   * The resulting model includes the coordinates' metadata, geographic attributes, and relationships.
   *
   * @example
   * {{{
   * val coordinates: Coordinates = ...
   * val rdfModel: Model = CoordinatesRdfMapper.coordinateToRdfModel.toRdfModel(coordinates)
   * }}}
   */
  implicit val coordinateToRdfModel: ToRdfModel[Coordinates] = new ToRdfModel[Coordinates] {
    override def toRdfModel(obj: Coordinates): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion from a `Coordinates` object to a subgraph.
   *
   * This implicit implementation converts a `Coordinates` instance into a `Subgraph`.
   * The subgraph includes:
   * - The coordinates' unique identifier, resource URI, and graph URI.
   * - Serialized RDF data in TriG format.
   * - Metadata such as the datatype, creation date, and a cryptographic hash of the serialized model.
   *
   * @example
   * {{{
   * val coordinates: Coordinates = ...
   * val subgraph: Subgraph = CoordinatesRdfMapper.coordinateToSubgraph.toSubgraph(coordinates)
   * }}}
   */
  implicit val coordinateToSubgraph: ToSubgraph[Coordinates] = new ToSubgraph[Coordinates] {
    override def toSubgraph(obj: Coordinates): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}