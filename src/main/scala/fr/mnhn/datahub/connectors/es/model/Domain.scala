/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.time.LocalDate
import java.util.Date
case class Domain(code: Option[String],
                  created_at: Option[Date],
                  description: Option[String],
                  domain_id: Option[Int],
                  label: Option[String],
                  name: Option[String]
                 ) extends Persistable {
  override def id(): String = if (domain_id.isDefined) CryptoUtils.md5sum(s"https://www.datahub.mnhn.fr/data/domain/${domain_id.get}", LocalDate.now().toString) else CryptoUtils.md5sum("")
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String = if (domain_id.isDefined) s"https://www.datahub.mnhn.fr/data/domain/${domain_id.get}" else ""

  override def datatype: String = "domain"
}
object Domain {
  implicit val format = Json.format[Domain]

  implicit val defaultDomain: Default[Domain] = new Default[Domain] {
    override def defaultValue: Domain = Domain(
     None, None, None, None, None, None
    )
  }
}