/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.time.LocalDate
import java.util.Date

case class GeolocationAttributes(
                       continent: Option[String], // dwc:continent
                       country: Option[String], // dwc:country
                       country_code: Option[String], // dwc:countryCode
                       county: Option[String],
                       first_administrative_level: Option[String], // geonames:parentADM1
                       island:Option[String], //dwc:island
                       island_group:Option[String], // dwc:islandGroup
                       locality: Option[String], // dwc:locality
                       municipality: Option[String], // dwc:municipality
                       ocean: Option[String], // mnhn:ocean
                       original_name: Option[String], // mnhn:originalName
                       remarks: Option[String], // dwc:locationRemarks
                       sea: Option[String], // mnhn:sea
                       second_administrative_level: Option[String], // geonames:parentADM2
                       state_province: Option[String], // dwc:stateProvince
                       verbatim_country: Option[String], // dwc:verbatimCountry
                       verbatim_locality: Option[String]  // dwc:verbatimLocality
                     )

object GeolocationAttributes {
  implicit lazy val format = Json.format[GeolocationAttributes]

  implicit val defaultGeolocationAttributes: Default[GeolocationAttributes] = new Default[GeolocationAttributes] {
    override def defaultValue: GeolocationAttributes = GeolocationAttributes(
      None: Option[String], // dwc:continent
      None: Option[String], // dwc:country
      None: Option[String], // dwc:countryCode
      None: Option[String],
      None: Option[String], // geonames:parentADM1
      None: Option[String], //dwc:island
      None: Option[String], // dwc:islandGroup
      None: Option[String], // dwc:locality
      None: Option[String], // dwc:municipality
      None: Option[String], // mnhn:ocean
      None: Option[String], // mnhn:originalName
      None: Option[String], // dwc:locationRemarks
      None: Option[String], // mnhn:sea
      None: Option[String], // geonames:parentADM2
      None: Option[String], // dwc:stateProvince
      None: Option[String], // dwc:verbatimCountry
      None: Option[String] // dwc:verbatimLocality
    )
  }
}

case class Geolocation(
                      attributes: Option[GeolocationAttributes],
                      code: Option[String],
                      created_at: Option[Date],
                      deleted_at: Option[Date],
                      description: Option[String],
                      geolocation_id: Option[String],
                      is_deleted: Option[Boolean],
                      label: Option[String],
                      last_modified_at: Option[Date],
                      name: Option[String]
                      // geonames: <http://www.geonames.org/ontology#>
                      ) extends Persistable {
  override def id(): String = if (geolocation_id.isDefined) CryptoUtils.md5sum(s"https://www.datahub.mnhn.fr/data/geolocation/${geolocation_id.get}", LocalDate.now().toString) else CryptoUtils.md5sum("")
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String = if (geolocation_id.isDefined) s"https://www.data.mnhn.fr/data/geolocation/${geolocation_id.get}" else ""

  override def datatype: String = "geolocation"
}
object Geolocation {
  implicit lazy val format = Json.format[Geolocation]

  implicit val defaultGeolocation: Default[Geolocation] = new Default[Geolocation] {
    override def defaultValue: Geolocation = Geolocation(
     None,
      None,
  None: Option[Date],
  None: Option[Date],
  None: Option[String],
  None: Option[String],
  None: Option[Boolean],
  None: Option[String],
  None: Option[Date],
  None: Option[String]
    )
  }
}

