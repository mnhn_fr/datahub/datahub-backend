/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.Taxon
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.{DCTERMS, FOAF, RDF}

import java.time.LocalDate

/**
 * The `TaxonRdfMapper` object is responsible for mapping `Taxon` instances to RDF models and subgraphs.
 * It includes methods for building RDF representations of taxon, as well as implicit conversions for generating
 * RDF models and subgraphs from `Taxon` objects.
 */
object TaxonRdfMapper {
  /**
   * Builds an RDF model representation for a given `Taxon`.
   *
   * @param model the `ModelBuilder` instance used to construct the RDF model.
   * @param taxon the `Taxon` object to be represented in RDF.
   * @param date the current date, typically used for metadata purposes.
   */
  def modelBuilding(model: ModelBuilder, taxon: Taxon, date: String) = {
    val taxonUri = createIRI(s"https://www.data.mnhn.fr/data/taxon/${taxon.taxon_id}")
    model.subject(taxonUri)
      .add(RDF.TYPE, "mnhn:Taxon")
      .add("dwc:taxonID", taxon.taxon_id)
      .add("dcterms:created", DateUtils.currentDate())


    taxon.attributes.authors.foreach { authors =>
      //authors.map(person => createIRI(s"https://www.data.mnhn.fr/data/person/${StringUtils.slugify(person.trim)}"))
      val uri = createIRI(s"https://www.data.mnhn.fr/data/person/${StringUtils.slugify(authors.trim)}")
      model.subject(uri).add(RDF.TYPE, "mnhn:Person").add(FOAF.NAME, authors).add(DCTERMS.IDENTIFIER, StringUtils.slugify(authors.trim))
      model.subject(taxonUri).add("mnhn:taxonAuthor", uri)
    }
    taxon.attributes.authors_infrasp.foreach { authors =>
      val uri = createIRI(s"https://www.data.mnhn.fr/data/person/${StringUtils.slugify(authors.trim)}")
      model.subject(uri).add(RDF.TYPE, "mnhn:Person").add(FOAF.NAME, authors).add(DCTERMS.IDENTIFIER, StringUtils.slugify(authors.trim))
      model.subject(taxonUri).add("mnhn:infraspTaxonAuthor", uri)
    }
    taxon.attributes.authors_nominal_taxon.foreach { authors =>
      val uri = createIRI(s"https://www.data.mnhn.fr/data/person/${StringUtils.slugify(authors.trim)}")
      model.subject(uri).add(RDF.TYPE, "mnhn:Person").add(FOAF.NAME, authors).add(DCTERMS.IDENTIFIER, StringUtils.slugify(authors.trim))
      model.subject(taxonUri).add("mnhn:nominalTaxonAuthor", uri)
    }
    taxon.attributes.authors_infrasp_nominal_taxon.foreach{ authors_infrasp_nominal_taxon =>
      val uri = createIRI(s"https://www.data.mnhn.fr/data/person/${StringUtils.slugify(authors_infrasp_nominal_taxon)}")
      model.subject(uri).add(RDF.TYPE, "mnhn:Person").add(FOAF.NAME, authors_infrasp_nominal_taxon).add(DCTERMS.IDENTIFIER, StringUtils.slugify(authors_infrasp_nominal_taxon.trim))
      model.subject(taxonUri).add("mnhn:infraspNominalTaxonAuthor", uri)
    }

    model.addTripleIfPresent(taxonUri, "mnhn:numFamily", taxon.attributes.num_family.map(_.toInt))
    model.addTripleIfPresent(taxonUri, "mnhn:numFamilyAPG", taxon.attributes.num_family_apg.map(_.toInt))
    model.addTripleIfPresent(taxonUri, "mnhn:numGenus", taxon.attributes.num_genus.map(_.toInt))
    model.addTripleIfPresent(taxonUri, "dwc:scientificNameAuthorship", taxon.attributes.scientific_name_authorship)
    model.addTripleIfPresent(taxonUri, "dwc:scientificName", taxon.attributes.scientific_name_with_authorship)
    model.addTripleIfPresent(taxonUri, "mnhn:scientificNameWOAuthorship", taxon.attributes.scientific_name_without_authorship)
    model.addTripleIfPresent(taxonUri, "dcterms:date", taxon.attributes.date_taxon)
    model.addTripleIfPresent(taxonUri, "dwc:taxonRemarks", taxon.attributes.remarks)

    model.addTripleIfPresent(taxonUri, "dwc:family", taxon.ranks.family)
    model.addTripleIfPresent(taxonUri, "dwc:genus", taxon.ranks.genus)
    model.addTripleIfPresent(taxonUri, "dwc:kingdom", taxon.ranks.kingdom)
    model.addTripleIfPresent(taxonUri, "dwc:order", taxon.ranks.order)
    model.addTripleIfPresent(taxonUri, "dwc:species", taxon.ranks.species)



    // taxon rank
    taxon.ranks.rank_id.foreach { rankID =>
      val rankIRI = createIRI(s"https://www.data.mnhn.fr/data/taxon/rank/${rankID.trim}")
      model.subject(rankIRI)
        .add(RDF.TYPE, "skos:Concept")
      model.addTripleIfPresent(rankIRI, "skos:notation", taxon.ranks.code)
      model.addTripleIfPresent(rankIRI, "skos:prefLabel", taxon.ranks.code)
      model.addTripleIfPresent(rankIRI, "mnhn:taxonRankLevel", taxon.ranks.level)
    }
  }

  /**
   * Implicit conversion to generate an RDF model from a sequence of `Taxon` objects.
   */
  implicit val taxonsToRdfModel: ToRdfModel[Seq[Taxon]] = new ToRdfModel[Seq[Taxon]] {
    /**
     * Converts a sequence of `Taxon` objects to an RDF model.
     *
     * @param objs the sequence of `Taxon` objects.
     * @return the constructed RDF model.
     */
    override def toRdfModel(objs: Seq[Taxon]): Model = {
      val builder = new ModelBuilder().namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap)
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }

  /**
   * Implicit conversion to generate an RDF model from a single `Taxon` object.
   */
  implicit val taxonToRdfModel: ToRdfModel[Taxon] = new ToRdfModel[Taxon] {
    /**
     * Converts a `Taxon` object to an RDF model.
     *
     * @param obj the `Taxon` object.
     * @return the constructed RDF model.
     */
    override def toRdfModel(obj: Taxon): Model = {
      val builder = new ModelBuilder().namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap)
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion to generate a `Subgraph` from a `Taxon` object.
   */

  implicit val taxonToSubgraph: ToSubgraph[Taxon] = new ToSubgraph[Taxon] {
    /**
     * Converts a `Taxon` object to a `Subgraph`.
     *
     * @param obj the `Taxon` object.
     * @return the constructed `Subgraph`.
     */
    override def toSubgraph(obj: Taxon): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}