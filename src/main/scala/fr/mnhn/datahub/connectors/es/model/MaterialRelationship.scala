/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import fr.mnhn.datahub.connectors.RDFSerializer.createIRI
import play.api.libs.json.Json

import java.time.LocalDate

case class MaterialRelationship(attributes: Option[Attributes],
                                code: Option[String],
                                description: Option[String],
                                label: Option[String],
                                name: Option[String],
                                object_id: String,
                                subject_id: String,
                                `type`: String
                               ) extends Persistable {
  override def id(): String = CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/material-relationship/${object_id}-${subject_id}", LocalDate.now().toString)
  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"
  override def resourceUri(): String = s"https://www.data.mnhn.fr/data/material-relationship/${object_id}-${subject_id}"

  override def datatype: String = "material-relationship"
}
object MaterialRelationship {
  implicit val format = Json.format[MaterialRelationship]

  implicit val defaultValueFOrMaterialRelationship: Default[MaterialRelationship] = new Default[MaterialRelationship] {
    override def defaultValue: MaterialRelationship = MaterialRelationship(None: Option[Attributes],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      None: Option[String],
      "defaultValue": String,
      "defaultValue": String,
      "defaultValue": String
    )
  }
}