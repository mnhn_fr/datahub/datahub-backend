/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{DATEIRIDATATYPE, addTripleIfPresent, createBNode, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.Material
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * Utility object for mapping `Material` instances to RDF models or subgraphs.
 *
 * This object provides methods to build RDF representations of materials, including metadata
 * such as material IDs, attributes, collection details, and domain associations. It also provides
 * implicit conversions for generating RDF models and subgraphs from `Material` instances or sequences of them.
 */

object MaterialRdfMapper {

  /**
   * Builds an RDF model for a given `Material` instance.
   *
   * This method generates RDF triples representing the specified `Material` and adds them to the provided `ModelBuilder`.
   * The material is represented as an `mnhn:Material` and may include:
   * - Unique IRI based on `material_id`.
   * - Metadata such as domain, collection, attributes, and collection event details.
   *
   * Attributes include:
   * - Material-specific properties: `catalogNumber`, `isPrivate`, `entryNumber`, etc.
   * - Collection-related properties: `collectionCode`, `hasDomain`, `inCollection`, etc.
   * - Attributes such as `birthDate`, `deathDate`, `boneCondition`, `stateOfPreservation`, etc.
   *
   * @param model The `ModelBuilder` to which RDF triples are added.
   * @param material The `Material` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata.
   */
  def modelBuilding(model: ModelBuilder, material: Material, date: String) = {
    if (material.material_id.isDefined) {
      val materialUri = createIRI(s"https://www.data.mnhn.fr/data/material/${material.material_id.get}")

      model.subject(materialUri)
        .add(RDF.TYPE, "mnhn:Material")

      model.addTripleIfPresent(materialUri, "dwc:materialEntityID", material.material_id)
      model.addTripleIfPresent(materialUri, "dwc:occurrenceID", material.material_id)
      model.addTripleIfPresent(materialUri, "dwc:datasetID", material.domain_id)
      model.addTripleIfPresent(materialUri, "dcterms:created", material.created_at)
      model.addTripleIfPresent(materialUri, "dwc:catalogNumber", material.catalog_number)
      model.addTripleIfPresent(materialUri, "mnhn:isPrivate", material.is_private)

      if (material.attributes.isDefined) {
        model.addTripleIfPresent(materialUri, "rdfs:label", material.attributes.get.collection_name)
        model.addTripleIfPresent(materialUri, "mnhn:materialNature", material.attributes.get.nature)
        model.addTripleIfPresent(materialUri, "mnhn:boneCondition", material.attributes.get.bone_condition)
        model.addTripleIfPresent(materialUri, "mnhn:captivity", material.attributes.get.captivity)
        model.addTripleIfPresent(materialUri, "mnhn:captivityPlace", material.attributes.get.capticity_place)
        model.addTripleIfPresent(materialUri, "mnhn:subCollectionName", material.attributes.get.collection_name)
        model.addTripleIfPresent(materialUri, "mnhn:fossil", material.attributes.get.fossil)
        model.addTripleIfPresent(materialUri, "dwc:individualCount", material.attributes.get.individual_count)
        model.addTripleIfPresent(materialUri, "mnhn:karyotype2N", material.attributes.get.karyotype_2n)
        model.addTripleIfPresent(materialUri, "mnhn:karyotypeNf", material.attributes.get.karyotype_nf)
        model.addTripleIfPresent(materialUri, "mnhn:karyotypeNfa", material.attributes.get.karyotype_nfa)
        model.addTripleIfPresent(materialUri, "mnhn:exsiccataAuthor", material.attributes.get.exsiccata_authors)
        model.addTripleIfPresent(materialUri, "mnhn:exsiccataNumber", material.attributes.get.exsiccata_number)
        model.addTripleIfPresent(materialUri, "mnhn:exsiccataPrefix", material.attributes.get.exsiccata_prefix)
        model.addTripleIfPresent(materialUri, "mnhn:exsiccataSuffix", material.attributes.get.exsiccata_suffix)
        model.addTripleIfPresent(materialUri, "mnhn:exsiccataTitle", material.attributes.get.exsiccata_title)
        model.addTripleIfPresent(materialUri, "mnhn:technicalObservations", material.attributes.get.technical_observations)
        model.addTripleIfPresent(materialUri, "dwc:preparations", material.attributes.get.type_of_preservation)
        model.addTripleIfPresent(materialUri, "dwc:materialEntityRemarks", material.attributes.get.remarks)
        model.addTripleIfPresent(materialUri, "dwc:occurrenceRemarks", material.attributes.get.remarks)
        model.addTripleIfPresent(materialUri, "mnhn:skinCondition", material.attributes.get.skin_condition)
        model.addTripleIfPresent(materialUri, "mnhn:stateOfPreservation", material.attributes.get.state_of_preservation)
        // autopsyDate
        material.attributes.get.autopsy_date.foreach { autopsy_date =>
          model.subject(materialUri)
            .add("mnhn:autopsyDate", autopsy_date)
        }

        // birth_date

        material.attributes.get.birth_date.foreach { birth_date =>
          model.subject(materialUri)
            .add("mnhn:birthDate", birth_date)
        }

        // death_date

        material.attributes.get.death_date.foreach { death_date =>
          model.subject(materialUri)
            .add("mnhn:deathDate", death_date)
        }

      }

      //model.addTripleIfPresent(materialUri, "dwciri:inCollection", material.institution_code)


      model.addTripleIfPresent(materialUri, "mnhn:entryNumber", material.entry_number)
      model.addTripleIfPresent(materialUri, "dwc:collectionCode", material.collection_code)


      //mnhn:stateOfPreservation

      // date



      //mnhn:entryDate



      //domain
      material.domain_id.foreach { domain_id =>
        val domainIRI = createIRI(s"https://www.data.mnhn.fr/data/domain/${domain_id}")

        model.subject(materialUri)
          .add("mnhn:hasDomain", domainIRI)
        model.subject(domainIRI)
          .add(RDF.TYPE, "skos:Concept")
      }

      // collection

      if (material.collection_id.isDefined) {
        val collectionIRI = createIRI(s"https://www.data.mnhn.fr/data/collection/${material.collection_id.get}")

        model.subject(collectionIRI)
          .add(RDF.TYPE, "mnhn:Collection")

        model.subject(materialUri)
          .add("dwciri:inCollection", collectionIRI)
        model.addTripleIfPresent(collectionIRI, "dwc:collectionID", material.collection_id)
        model.addTripleIfPresent(collectionIRI, "dwc:collectionCode", material.collection_code)

        material.institution_code.foreach { institution_code =>
          val institutionIRI = createIRI(s"https://www.data.mnhn.fr/data/organization/$institution_code")
          model.subject(collectionIRI).add("mnhn:hasInstitution", institutionIRI)
          model.subject(institutionIRI).add(RDF.TYPE, "foaf:Organization").add("dwc:institutionCode", institution_code)
        }

      }

      // collection Event

      material.collection_event_id.foreach { collection_event_id =>
        val collectionEventIRI = createIRI(s"https://www.data.mnhn.fr/data/collection-event/${collection_event_id}")

        model.subject(collectionEventIRI)
          .add(RDF.TYPE, "mnhn:CollectionEvent")

        model.subject(materialUri)
          .add("mnhn:hasCollectionEvent", collectionEventIRI)
      }
    }
  }


  /**
   * Implicit conversion from a sequence of `Material` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `Material` instances into an RDF model.
   * Each material in the sequence is mapped to RDF triples and included in the model.
   *
   * @example
   * {{{
   * val materials: Seq[Material] = ...
   * val rdfModel: Model = MaterialRdfMapper.materialsToRdfModel.toRdfModel(materials)
   * }}}
   */
  implicit val materialsToRdfModel: ToRdfModel[Seq[Material]] = new ToRdfModel[Seq[Material]] {
    override def toRdfModel(objs: Seq[Material]): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }

  /**
   * Implicit conversion from a single `Material` object to an RDF model.
   *
   * This implicit implementation converts a single `Material` instance into an RDF model.
   * The resulting RDF model includes triples representing the material's metadata and attributes.
   *
   * @example
   * {{{
   * val material: Material = ...
   * val rdfModel: Model = MaterialRdfMapper.materialToRdfModel.toRdfModel(material)
   * }}}
   */
  implicit val materialToRdfModel: ToRdfModel[Material] = new ToRdfModel[Material] {
    override def toRdfModel(obj: Material): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion from a `Material` object to a subgraph.
   *
   * This implicit implementation converts a `Material` instance into a `Subgraph`.
   * The subgraph includes:
   * - The material's unique identifier, resource URI, and graph URI.
   * - Serialized RDF data in TriG format.
   * - Metadata such as the datatype, creation date, and a cryptographic hash of the serialized model.
   *
   * @example
   * {{{
   * val material: Material = ...
   * val subgraph: Subgraph = MaterialRdfMapper.materialToSubgraph.toSubgraph(material)
   * }}}
   */
  implicit val materialToSubgraph: ToSubgraph[Material] = new ToSubgraph[Material] {
    override def toSubgraph(obj: Material): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}