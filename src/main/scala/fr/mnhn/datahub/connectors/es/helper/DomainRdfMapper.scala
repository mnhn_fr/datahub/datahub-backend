/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.Domain
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * Utility object for mapping `Domain` instances to RDF models or subgraphs.
 *
 * This object provides methods for creating RDF representations of domains, including
 * their unique identifiers, labels, codes, and definitions. It also includes implicit
 * implementations to convert `Domain` objects and sequences of them into RDF models and subgraphs.
 */

object DomainRdfMapper {
  /**
   * Builds an RDF model for a given `Domain` instance.
   *
   * This method adds RDF triples representing the specified `Domain` to the provided `ModelBuilder`.
   * Each domain is represented as a `skos:Concept` and includes:
   * - A unique IRI based on its ID.
   * - Notation, preferred label, and definition metadata.
   *
   * @param model The `ModelBuilder` to which RDF triples are added.
   * @param domain The `Domain` instance to be mapped.
   * @param date The current date, used for time-sensitive metadata (if applicable).
   */
  def modelBuilding(model: ModelBuilder, domain: Domain, date: String) = {
    domain.domain_id.foreach { domain_id =>
      val domainIRI = createIRI(s"https://www.data.mnhn.fr/data/domain/${domain.domain_id.get}")
      model.subject(domainIRI)
        .add(RDF.TYPE, "skos:Concept")
      model.addTripleIfPresent(domainIRI, "skos:notation", domain.code)
      model.addTripleIfPresent(domainIRI, "skos:prefLabel", domain.label)
      model.addTripleIfPresent(domainIRI, "skos:definition", domain.description)
    }
  }

  /**
   * Implicit conversion from a sequence of `Domain` objects to an RDF model.
   *
   * This implicit implementation converts a sequence of `Domain` instances
   * into an RDF model. Each domain in the sequence is mapped to RDF triples
   * and included in the model.
   *
   * @example
   * {{{
   * val domains: Seq[Domain] = ...
   * val rdfModel: Model = DomainRdfMapper.domainsToRdfModel.toRdfModel(domains)
   * }}}
   */
  implicit val domainsToRdfModel: ToRdfModel[Seq[Domain]] = new ToRdfModel[Seq[Domain]] {
    override def toRdfModel(objs: Seq[Domain]): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }
  /**
   * Implicit conversion from a single `Domain` object to an RDF model.
   *
   * This implicit implementation converts a single `Domain` instance into an RDF model.
   * The resulting RDF model includes triples representing the domain's metadata and relationships.
   *
   * @example
   * {{{
   * val domain: Domain = ...
   * val rdfModel: Model = DomainRdfMapper.domainToRdfModel.toRdfModel(domain)
   * }}}
   */

  implicit val domainToRdfModel: ToRdfModel[Domain] = new ToRdfModel[Domain] {
    override def toRdfModel(obj: Domain): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion from a `Domain` object to a subgraph.
   *
   * This implicit implementation converts a `Domain` instance into a `Subgraph`.
   * The subgraph includes:
   * - The domain's unique identifier, resource URI, and graph URI.
   * - Serialized RDF data in TriG format.
   * - Metadata such as the datatype, creation date, and a cryptographic hash of the serialized model.
   *
   * @example
   * {{{
   * val domain: Domain = ...
   * val subgraph: Subgraph = DomainRdfMapper.domainToSubgraph.toSubgraph(domain)
   * }}}
   */
  implicit val domainToSubgraph: ToSubgraph[Domain] = new ToSubgraph[Domain] {
    override def toSubgraph(obj: Domain): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}