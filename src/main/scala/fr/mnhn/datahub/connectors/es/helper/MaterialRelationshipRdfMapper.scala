/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.helper

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import fr.mnhn.datahub.connectors.es.model.{Material, MaterialRelationship}
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * This object is responsible for mapping `MaterialRelationship` instances to RDF models and subgraphs.
 * It includes methods for building RDF representations of material relationships and implicit conversions
 * for generating RDF models and subgraphs from `MaterialRelationship` objects.
 */

object MaterialRelationshipRdfMapper {

  /**
   * Builds an RDF model representation for a `MaterialRelationship`.
   *
   * @param model  the `ModelBuilder` instance used to construct the RDF model.
   * @param material  the `MaterialRelationship` object to be represented in RDF.
   * @param date  the current date, typically used for metadata purposes.
   */
  def modelBuilding(model: ModelBuilder, material: MaterialRelationship, date: String) = {
    val materialObjectUri = createIRI(s"https://www.data.mnhn.fr/data/material/${material.object_id}")
    val materialSubjectUri = createIRI(s"https://www.data.mnhn.fr/data/material/${material.subject_id}")
    val materialRelationshipUri = createIRI(s"https://www.data.mnhn.fr/data/material-relationship/${material.object_id}-${material.subject_id}")

    val materialTypeUri = createIRI(s"https://www.data.mnhn.fr/data/material-relationship-type/${material.`type`}")

    model.subject(materialRelationshipUri)
      .add("mnhn:relationObject", materialObjectUri)
      .add("mnhn:relationSubject", materialSubjectUri)
      .add("mnhn:relationType", materialTypeUri)


    model.subject(materialObjectUri)
      .add(RDF.TYPE, "mnhn:Material")
    model.subject(materialSubjectUri)
      .add(RDF.TYPE, "mnhn:Material")

    model.addTripleIfPresent(materialTypeUri, "skos:notation", material.code)
    model.addTripleIfPresent(materialTypeUri, "skos:prefLabel", material.label)
    model.addTripleIfPresent(materialTypeUri, "skos:definition", material.description)
  }


  implicit val materialRelationShipsToRdfModel: ToRdfModel[Seq[MaterialRelationship]] = new ToRdfModel[Seq[MaterialRelationship]] {
    override def toRdfModel(objs: Seq[MaterialRelationship]): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      objs.foreach(obj => modelBuilding(model, obj, DateUtils.currentDate()))
      model.build()
    }
  }

  /**
   * Implicit conversion to generate an RDF model from a sequence of `MaterialRelationship` objects.
   */
  implicit val materialRelationShipToRdfModel: ToRdfModel[MaterialRelationship] = new ToRdfModel[MaterialRelationship] {
    /**
     * Converts a sequence of `MaterialRelationship` objects to an RDF model.
     *
     * @param objs  the sequence of `MaterialRelationship` objects.
     * @return  the constructed RDF model.
     */
    override def toRdfModel(obj: MaterialRelationship): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/baseunifiee/NG")
      modelBuilding(model, obj, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion to generate a `Subgraph` from a `MaterialRelationship` object.
   */

  implicit val materialRelationshipToSubgraph: ToSubgraph[MaterialRelationship] = new ToSubgraph[MaterialRelationship] {
    override def toSubgraph(obj: MaterialRelationship): Subgraph = {
      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(obj.id(), obj.graphUri(), obj.resourceUri(), obj.datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}