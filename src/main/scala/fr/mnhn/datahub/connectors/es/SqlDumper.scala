/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.alpakka.slick.scaladsl.SlickSession
import akka.stream.scaladsl.{Sink, Source}
import com.mnemotix.sql.{Insertable, Persistable}
import com.mnemotix.sql.model.Subgraph
import com.mnemotix.synaptix.cache.{CacheEntry, RocksDBStore}
import com.mnemotix.synaptix.core.GenericService
import fr.mnhn.datahub.connectors.ToSubgraph
import fr.mnhn.datahub.connectors.ToSubgraph.ToSubGraphModelOps
import play.api.libs.json.{Json, Reads}

import scala.concurrent.{ExecutionContext, Future}

/**
 *
 * @param cacheDir
 * @param system
 * @param ec
 * @param session
 */
class SqlDumper(cacheDir: String)(implicit system: ActorSystem, ec: ExecutionContext, session: SlickSession) extends GenericService {
  lazy val cache = new RocksDBStore(cacheDir)
  lazy val rdfDumper = new RdfDumper()

  override def init(): Unit = {
    rdfDumper.init()
    cache.init()
  }

  def dumpSubgraph[A]()(implicit insertable: Insertable[Subgraph], read:Reads[A], toSubgraph: ToSubgraph[A]): Future[Done] = {
    val source: Source[CacheEntry, NotUsed] = Source.fromIterator(() => cache.iterator())
    val sink: Sink[Seq[Subgraph], Future[Done]] = {
      //rdfDumper.postgresDumper()
    ???
    }

    source.map { iter =>
      val obj = Json.parse(new String(iter.value)).as[A]
      val subgraph = obj.toSubgraph
      subgraph
    }.grouped(20).runWith(sink)
  }

  override def shutdown(): Unit = cache.shutdown()
}