/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.es.model

import com.mnemotix.sql.Persistable
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.time.LocalDate
import java.util.Date

case class AttributesCollection(continent: Option[String], country: Option[String], is_deleted: Option[String], county: Option[String],
                      first_administrative_level: Option[String], island: Option[String], island_group: Option[String],
                      locality: Option[String], municipality: Option[String], ocean: Option[String], original_name: Option[String],
                      remarks: Option[String], sea: Option[String], second_administrative_level: Option[String], state_province: Option[String],
                      verbatim_country: Option[String], verbatim_locality: Option[String]
                     )


object AttributesCollection {
  implicit lazy val format = Json.format[AttributesCollection]

  implicit val defaultForAttributesCollection: Default[AttributesCollection] = new Default[AttributesCollection] {
    override def defaultValue: AttributesCollection = AttributesCollection(None: Option[String], None: Option[String], None: Option[String], None: Option[String],
      None: Option[String], None: Option[String], None: Option[String],
      None: Option[String], None: Option[String], None: Option[String], None: Option[String],
      None: Option[String], None: Option[String], None: Option[String], None: Option[String],
      None: Option[String], None: Option[String]
    )
  }
}

case class Collection(attributes: Option[AttributesCollection],
                      code: Option[String],
                      collection_group_id: Option[Int],
                      collection_id: Option[Int],
                      created_at: Option[Date],
                      description: Option[String],
                      institution_id: Option[Int],
                      label: Option[String],
                      name: Option[String]
                     ) extends Persistable {
  override def id(): String = if (collection_id.isDefined) CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/collection/${collection_id.get}", LocalDate.now().toString)  else ""

  override def graphUri(): String = "https://www.datahub.mnhn.fr/data/baseunifiee/NG"

  override def resourceUri(): String = if (collection_id.isDefined) s"https://www.data.mnhn.fr/data/collection/${collection_id.get}" else ""

  override def datatype: String = "collection"
}

object Collection {
  implicit lazy val format = Json.format[Collection]

  implicit val defaultForCollection: Default[Collection] = new Default[Collection] {
    override def defaultValue = Collection(
  None, None: Option[String],
      None: Option[Int],
      None: Option[Int],
      None: Option[Date],
      None: Option[String],
      None: Option[Int],
      None: Option[String],
      None: Option[String]
    )
  }
}