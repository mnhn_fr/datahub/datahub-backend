package fr.mnhn.datahub.connectors

import com.mnemotix.synaptix.core.ServiceListener

class HarvestingServiceListener extends ServiceListener {
  override def onStartup(): Unit = println("Startup")
  override def onShutdown(): Unit = println("Shutdown")
  override def onAbort(): Unit = println("Abort")
  override def onDone(): Unit = println("Done")
  override def onTerminate(): Unit = println("Terminate")
  override def onProgress(processedItems: Int, totalItems: Option[Int], message: Option[String]): Unit = super.onProgress(processedItems, totalItems, message)
  override def onError(errorMessage: String, cause: Option[Throwable]): Unit = super.onError(errorMessage, cause)
  override def onUpdate(message: String): Unit = println(message)
}
