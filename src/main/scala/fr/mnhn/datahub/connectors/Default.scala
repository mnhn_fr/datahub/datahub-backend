/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

/**
 * A trait that provides a default value for a given type `A`.
 * This trait is intended to be used to define default values for case class objects or other types.
 *
 * The implementation of this trait should define how to obtain the default value for the type `A`.
 *
 * @tparam A The type for which a default value is provided.
 */
trait Default[A] {

  /**
   * Returns the default value for the type `A`.
   *
   * @return The default value of type `A`.
   */
  def defaultValue: A
}