/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

/**
 * Abstract base class for exceptions related to the OAI (Open Archives Initiative) connector.
 * This class extends the standard `Exception` class and allows for custom messages and causes.
 *
 * @param message The error message describing the exception.
 * @param cause The underlying cause of the exception, represented as a `Throwable`.
 */
abstract class OaiConnectorException(message: String, cause: Throwable) extends Exception(message, cause)

/**
 * Exception thrown during the harvesting process within the OAI connector.
 * This class extends `OaiConnectorException` and adds specific functionality for harvesting-related errors.
 *
 * @param message The error message describing the harvesting-related exception.
 * @param cause The underlying cause of the exception, represented as a `Throwable`.
 */
case class HarvestingException(message: String, cause: Throwable) extends OaiConnectorException(message, cause)

/**
 * Exception thrown during the mapping process within the OAI connector.
 * This class extends `OaiConnectorException` and adds specific functionality for mapping-related errors.
 *
 * @param message The error message describing the mapping-related exception.
 * @param cause The underlying cause of the exception, represented as a `Throwable`.
 */
case class MappingException(message: String, cause: Throwable) extends OaiConnectorException(message, cause)

