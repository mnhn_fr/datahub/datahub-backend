/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

import fr.mnhn.datahub.connectors.rest.model.{BoldRootInterface, DigitalizationsRootInterface, GenBankRootInterface, Page, SpecimenRootInterface}
import play.api.libs.json.Json

/**
 * Trait for extracting page information from an entity of type A.
 *
 * @tparam A the type of the entity from which the page information is extracted
 */
trait MnhnEnpointPageExtracter[A] {
  /**
   * Extracts the page information from the given entity.
   *
   * @param value the entity of type A from which to extract the page information
   * @return the extracted page information
   */
  def extractPage(value: A): Page
}

object MnhnEnpointPageExtracter {
  /**
   * Implicit implementation for extracting the page information from a DigitalizationsRootInterface.
   */
  implicit val DigitalizationPagextracter: MnhnEnpointPageExtracter[DigitalizationsRootInterface] = (value: DigitalizationsRootInterface) => {
    value.page
  }

  /**
   * Implicit implementation for extracting the page information from a SpecimenRootInterface.
   */
  implicit val MoleculairesPagextracter: MnhnEnpointPageExtracter[SpecimenRootInterface] = (value: SpecimenRootInterface) => {
    value.page
  }
}

/**
 * Trait for converting an entity of type A into JSON strings.
 *
 * @tparam A the type of the entity to be converted
 */
trait MnhnEndpointResultConverter[A] {
  /**
   * Converts the given entity into a single JSON string.
   *
   * @param value the entity of type A to be converted
   * @return the resulting JSON string
   */
  def createJsonString(value: A): String

  /**
   * Converts the given entity into a sequence of JSON strings.
   *
   * @param value the entity of type A to be converted
   * @return the resulting sequence of JSON strings
   */
  def createJsonStringSeq(value: A): Seq[String]
}

/**
 * Trait for converting an entity of type A into JSON strings with an optional ID.
 *
 * @tparam A the type of the entity to be converted
 */
trait MoleculaireAPIResultConverter[A] {
  /**
   * Converts the given entity into a single JSON string with the optional ID.
   *
   * @param value the entity of type A to be converted
   * @param id an optional ID to be included in the JSON string
   * @return the resulting JSON string
   */
  def createJsonString(value: A, id: Option[String]): String

  /**
   * Converts the given entity into a sequence of JSON strings with the optional ID.
   *
   * @param value the entity of type A to be converted
   * @param id an optional ID to be included in the JSON string
   * @return the resulting sequence of JSON strings
   */
  def createJsonStringSeq(value: A, id: Option[String]): Seq[String]
}

object MnhnEndpointResultConverter {
  /**
   * Implicit implementation for converting a DigitalizationsRootInterface into JSON strings.
   */
  implicit val DigitalizationConverter: MnhnEndpointResultConverter[DigitalizationsRootInterface] = new MnhnEndpointResultConverter[DigitalizationsRootInterface] {
    override def createJsonString(value: DigitalizationsRootInterface): String = {
      Json.prettyPrint(Json.toJson(value._embedded.get.digitalizations))
    }

    override def createJsonStringSeq(value: DigitalizationsRootInterface): Seq[String] = {
      value._embedded.get.digitalizations.map(d => Json.prettyPrint(Json.toJson(d)))
    }
  }

  /**
   * Implicit implementation for converting a SpecimenRootInterface into JSON strings.
   */
  implicit val MoleculaireConverter: MnhnEndpointResultConverter[SpecimenRootInterface] = new MnhnEndpointResultConverter[SpecimenRootInterface] {
    override def createJsonString(value: SpecimenRootInterface): String = {
      Json.prettyPrint(Json.toJson(value._embedded.get.specimens.getOrElse(Seq.empty)))
    }

    override def createJsonStringSeq(value: SpecimenRootInterface): Seq[String] = {
      value._embedded.get.specimens.getOrElse(Seq.empty).map(d => Json.prettyPrint(Json.toJson(d)))
    }
  }

  /**
   * Implicit implementation for converting a GenBankRootInterface into JSON strings with an optional ID.
   */
  implicit val GenBankConverter: MoleculaireAPIResultConverter[GenBankRootInterface] = new MoleculaireAPIResultConverter[GenBankRootInterface] {
    override def createJsonString(value: GenBankRootInterface, id: Option[String]): String = {
      val genbank = value._embedded.map { embedded =>
        embedded.sequences.map { sequences =>
          sequences.map { sequence =>
            sequence.copy(specimenId = id)
          }
        }
      }
      Json.prettyPrint(Json.toJson(genbank.flatten.getOrElse(Seq.empty)))
    }

    override def createJsonStringSeq(value: GenBankRootInterface, id: Option[String]): Seq[String] = {
      val genbank = value._embedded.map { embedded =>
        embedded.sequences.map { sequences =>
          sequences.map { sequence =>
            sequence.copy(specimenId = id)
          }
        }
      }
      genbank.flatten.getOrElse(Seq.empty).map { d =>
        Json.prettyPrint(Json.toJson(d))
      }
    }
  }

  /**
   * Implicit implementation for converting a BoldRootInterface into JSON strings with an optional ID.
   */
  implicit val BoldRootInterfaceConverter: MoleculaireAPIResultConverter[BoldRootInterface] = new MoleculaireAPIResultConverter[BoldRootInterface] {
    override def createJsonString(value: BoldRootInterface, id: Option[String]): String = {
      val bold = value.boldRecords.map { boldRecords =>
        boldRecords.map { boldRecord =>
          boldRecord.copy(specimenId = id)
        }
      }
      Json.prettyPrint(Json.toJson(bold.getOrElse(Seq.empty)))
    }

    override def createJsonStringSeq(value: BoldRootInterface, id: Option[String]): Seq[String] = {
      val bold = value.boldRecords.getOrElse(Seq.empty).map { boldRecord =>
        boldRecord.copy(specimenId = id)
      }
      bold.map(d => Json.prettyPrint(Json.toJson(d)))
    }
  }
}