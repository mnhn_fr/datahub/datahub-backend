/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

import org.eclipse.rdf4j.model.Model

/**
 * A trait for converting an object of type A to an RDF model.
 *
 * This trait defines a method `toRdfModel` that, when implemented, allows the conversion
 * of an object of type A to a corresponding RDF model representation.
 *
 * @tparam A The type of the object to be converted to an RDF model.
 */
trait ToRdfModel[A] {
  /**
   * Converts an object of type A to an RDF model.
   *
   * This method is responsible for transforming an object of type A into an RDF model.
   * The specific implementation of this method is provided by concrete implementations
   * of the `ToRdfModel` trait for a given type A.
   *
   * @param obj The object to be converted to an RDF model.
   * @return The RDF model representation of the given object.
   */
  def toRdfModel(obj: A): Model
}

/**
 * Companion object for the `ToRdfModel` trait that provides an implicit class
 * to enable the conversion of objects to RDF models through the `toRdfModel` method.
 */
object ToRdfModel {

  /**
   * Implicit class that adds the `toRdfModel` method to instances of type A.
   *
   * This class allows objects of type A to directly call the `toRdfModel` method,
   * provided an implicit `ToRdfModel[A]` instance is available in scope.
   *
   * @param obj The object of type A to be converted to an RDF model.
   * @tparam A The type of the object to be converted.
   */
  implicit class ToRdfModelOps[A](obj: A)(implicit toRdfModelInstance: ToRdfModel[A]) {

    /**
     * Converts the current object to an RDF model.
     *
     * This method delegates the conversion process to the implicit `ToRdfModel[A]`
     * instance available in scope, invoking its `toRdfModel` method.
     *
     * @return The RDF model representation of the current object.
     */
    def toRdfModel: Model = toRdfModelInstance.toRdfModel(obj)
  }
}