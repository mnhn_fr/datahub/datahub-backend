package fr.mnhn.datahub.connectors.rest.model

import play.api.libs.json.Json

case class Href (
                  href: Option[String],
                  templated:Option[Boolean]
                )

object Href {
  implicit lazy val format = Json.format[Href]
}