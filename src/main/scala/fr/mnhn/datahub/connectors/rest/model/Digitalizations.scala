package fr.mnhn.datahub.connectors.rest.model

import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

case class Digitalizations (
                             id: Option[Int],
                             specimen: Option[Specimens],
                             projectId: Option[Int],
                             projectUrl: Option[String],
                             projectTitle: Option[String],
                             description: Option[String],
                             rawDataUrl: Option[String],
                             rawDataStatus: Option[String],
                             sliceUrl: Option[String],
                             thumbnailUrl: Option[String],
                             thumbnailMimeType: Option[String],
                             equipmentType: Option[String],
                             operator: Option[String],
                             resolution: Option[String],
                             isPrivate: Option[Boolean],
                             creationDate: Option[String],
                             modifiedDate: Option[String],
                             _links: Option[DigitalizationLinks]
                           )
object Digitalizations {
  implicit lazy val format = Json.format[Digitalizations]


  implicit val defaultForDigitalizations: Default[Digitalizations] = new Default[Digitalizations] {
    override def defaultValue: Digitalizations = Digitalizations(
      None, None,None, None,None, None,None, None,None, None,None, None,None, None,None, None,None, None
    )
  }
}
