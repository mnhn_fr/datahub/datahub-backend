package fr.mnhn.datahub.connectors.rest.model

import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

import java.util.Date

case class Specimens (
                       id: Option[Int],
                       collectionCode: Option[String],
                       institutionCode: Option[String],
                       catalogNumber: Option[String],
                       sampleId: Option[String],
                       boldProcessId: Option[String],
                       boldCreationDate: Option[Date],
                       boldReturnDate: Option[Date],
                       created: Option[Date],
                       _links: Option[LinksGen]
                     )

object Specimens {
  implicit lazy val format = Json.format[Specimens]

  implicit val defaultForMoleculaires: Default[Specimens] = new Default[Specimens] {
    override def defaultValue: Specimens = Specimens(
      None, None,None, None,None, None,None, None,None, None
    )
  }
}