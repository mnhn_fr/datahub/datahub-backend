package fr.mnhn.datahub.connectors.rest.model

import play.api.libs.json.Json

case class Links(
                 first: Option[Href],
                 self: Option[Href],
                 next: Option[Href],
                 last: Option[Href]
               )

object Links {
  implicit lazy val format = Json.format[Links]
}
