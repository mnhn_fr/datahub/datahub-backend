package fr.mnhn.datahub.connectors.rest.hal

import akka.Done
import com.mnemotix.synaptix.jobs.api.SynaptixJob
import fr.mnhn.datahub.connectors.oai.OaiHarvestingConfiguration
import fr.mnhn.datahub.connectors.utils.CachedHalRestClient

import scala.concurrent.Future

/**
 * HalRestHarvesterJob is a job that interacts with the HAL REST API to harvest metadata records and
 * store them in a cache. It extends the `SynaptixJob` class and provides the functionality to initialize,
 * process, and shutdown the client interacting with the HAL REST service.
 *
 * This job leverages the `CachedHalRestClient` to fetch metadata records from the HAL REST API,
 * specifically targeting records with the `level0_domain_s:sdv` field. The harvested records are cached
 * for further processing or exporting.
 *
 * The job performs the following steps:
 * 1. Initializes the client and cache before processing starts.
 * 2. Harvests records from the HAL REST API and caches them.
 * 3. Shuts down the cache after processing completes.
 *
 * @param client The `CachedHalRestClient` used to connect to the HAL REST API and fetch metadata records.
 * @param prefix A string representing the job's prefix, set to "hal" in this case.
 * @param label A string representing the job's label, set to "HAL REST Harvester Job" in this case.
 * @param beforeProcess Initializes the cache and prepares the client to fetch data.
 * @param afterProcess Shuts down the cache after the job completes.
 * @param process Harvests records from the HAL REST API and caches them.
 */
class HalRestHarvesterJob extends SynaptixJob {
  override val prefix: String = "hal"
  override val label: String = "HAL REST Harvester Job"

  /**
   * The `CachedHalRestClient` used to communicate with the HAL REST API and harvest metadata records.
   */
  lazy val client = new CachedHalRestClient("HAL REST Client", "https://api.archives-ouvertes.fr/search/", RestHarvestingConfiguration.halCacheDir)

  override def beforeProcess(): Unit = client.init()

  override def afterProcess(): Unit = client.cache.shutdown()

  /**
   * Harvests records from the HAL REST API with the query filter `fq="level0_domain_s:sdv"` and caches them.
   * This method performs the core functionality of fetching the records and storing them in the cache.
   * It returns a `Future[Done]` to indicate the completion status of the processing.
   *
   * @return A `Future[Done]` representing the completion of the harvesting and caching process.
   */
  override def process(): Future[Done] = {
    client.cacheAllRecords(fq="level0_domain_s:sdv").map { _ => Done.done() }
  }
}