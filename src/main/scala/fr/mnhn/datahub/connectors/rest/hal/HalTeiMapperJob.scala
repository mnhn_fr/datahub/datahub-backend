package fr.mnhn.datahub.connectors.rest.hal

import com.mnemotix.oai.api.Mapper
import com.mnemotix.synaptix.cache.{CacheEntry, RocksDBStore}
import com.mnemotix.synaptix.core.GenericService
import com.mnemotix.synaptix.core.utils.CryptoUtils.md5sum
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.connectors.RDFSerializer
import fr.mnhn.datahub.connectors.RDFSerializer._
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.{ModelBuilder, Values}
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import scala.xml._

/**
 * HalTeiMapperJob is a job that processes HAL metadata stored in a RocksDB cache and maps the
 * data into RDF format. It extends the `Mapper` trait and implements the `GenericService` trait
 * to provide functionalities for initialization, processing, and shutdown operations.
 *
 * The job processes metadata entries stored in the cache, iterating over the records and generating
 * RDF triples for each entry. The resulting RDF data is saved as TriG files in the output directory.
 * The mapping logic is based on the structure and content of the metadata, such as publications, authors,
 * and projects, which are mapped to respective RDF types and predicates.
 *
 * The job performs the following tasks:
 * 1. Initializes the RocksDB cache before processing starts.
 * 2. Iterates over the cached records, processes each record, and maps it to RDF.
 * 3. Outputs the mapped RDF data into TriG files, controlling the merge size.
 * 4. Shuts down the cache after processing completes.
 *
 * @param cache The `RocksDBStore` used to read cached metadata entries from the storage directory.
 * @param builder The `ModelBuilder` used to build the RDF model for each record.
 * @param init Initializes the cache and prepares it for processing.
 * @param shutdown Shuts down the cache after processing completes.
 * @param process Iterates over the cached records, processes them, and outputs the RDF data.
 */

class HalTeiMapperJob extends Mapper with GenericService {

  lazy val cache: RocksDBStore = new RocksDBStore(RestHarvestingConfiguration.halCacheDir)
  var builder: ModelBuilder = _

  override def init(): Unit = cache.init()

  override def shutdown(): Unit = cache.shutdown()

  /**
   * Processes metadata records from the cache, maps them to RDF, and writes the output as TriG files.
   * The records are retrieved in pages, and the RDF data is merged into files of controlled size.
   *
   * @return Unit.
   */
  def process(): Unit = {
    val it = cache.iterator()
    Files.createDirectories(Paths.get(s"${RestHarvestingConfiguration.halOutputDir}"))
    var count = 0
    while (it.hasNext) {
      // take de N first elements from the iterator to control final files merge size.
      val pageIterator = it.take(RestHarvestingConfiguration.halMergeSize);
      // reinitialize the builder
      builder = getModel().namedGraph(createIRI("https://www.data.mnhn.fr/data/hal#NG"))
      count += RestHarvestingConfiguration.halMergeSize
      while(pageIterator.hasNext){
        val entry: CacheEntry = pageIterator.next()
        // map the cache entry to RDF and add statements to the shared model builder
        map(entry.key, new String(entry.value, "UTF-8").split(cache.RECORD_SEPERATOR).map(scala.xml.XML.loadString))
      }
      val filepath = Paths.get(s"${RestHarvestingConfiguration.halOutputDir}/$count.trig")
      logger.debug(s"File created ${filepath.toAbsolutePath.toString}")
      Files.write(filepath, RDFSerializer.toTriG(builder.build()).getBytes(StandardCharsets.UTF_8))
//      println(RDFSerializer.toTriG(builder.build()))
    }
    it.close()
  }

  def uri(key: String): String = "https://www.datapoc.mnhn.fr/data/document/hal-museum/" + StringUtils.slugify(key.trim)

  def personUri(key: String): String = "https://www.datapoc.mnhn.fr/data/person/" + md5sum(StringUtils.slugify(key.trim),"hal-museum")

  /**
   * Maps a cache entry to an RDF model. The entry is processed based on its prefix (e.g., "struct", "projanr")
   * to determine the appropriate mapping function (e.g., publication, structure, project).
   *
   * @param key The key for the cache entry.
   * @param data A sequence of XML nodes representing the data to be mapped.
   * @return A `Model` representing the mapped RDF data.
   */
  override def map(key: String, data: Seq[NodeSeq]): Model = {
    val builder = getModel().namedGraph(createIRI("https://www.data.mnhn.fr/data/hal#NG"))
    val keyPrefix = key.split("-")(0)
    data.foreach { node =>
      keyPrefix match {
        case "struct" => mapStructure(key, node)
        case "projanr" => mapProject(key, node)
        case "projeurop" => mapProject(key, node)
        case _ => mapPublication(key, node)
      }
    }
    builder.build()
  }

  /**
   * Maps a publication node to RDF, extracting various metadata fields and adding them to the RDF model.
   *
   * @param key The key for the cache entry.
   * @param node The XML node representing the publication data.
   * @return A `ModelBuilder` instance with the mapped RDF data.
   */
  private def mapPublication(key: String, node: NodeSeq): ModelBuilder = {
    val halUri = (node \ "publicationStmt" \ "idno").find(n => (n \ "@type").text == "halUri").map(_.text).get.trim
    val titleFr = (node \ "titleStmt" \ "title").find(n => (n \ "@{http://www.w3.org/XML/1998/namespace}lang").text == "fr").map(_.text.trim)
    val titleEn = (node \ "titleStmt" \ "title").find(n => (n \ "@{http://www.w3.org/XML/1998/namespace}lang").text == "en").map(_.text.trim)
    val abstractFr: Option[String] = (node \ "profileDesc" \ "abstract").find(n => (n \ "@{http://www.w3.org/XML/1998/namespace}lang").text == "fr").map { n => n.text.trim }
    val abstractEn: Option[String] = (node \ "profileDesc" \ "abstract").find(n => (n \ "@{http://www.w3.org/XML/1998/namespace}lang").text == "en").map { n => n.text.trim }
    val date = (node \ "editionStmt" \ "edition" \ "date").find(n => (n \ "@type").text == "whenReleased").map(_.text).get.trim
    val created = (node \ "editionStmt" \ "edition" \ "date").find(n => (n \ "@type").text == "whenProduced").map(_.text).get.trim
//    val licence:Option[String] = Option((node \ "publicationStmt" \ "availability" \ "licence" \ "@target").text.trim)
    val language = (node \ "profileDesc" \ "langUsage" \ "language" \ "@ident").text
    val fileLocations : Seq[String] = (node \ "editionStmt" \ "edition" \ "ref" \\ "@target").map(_.text)

    val subjects:Seq[(String, String)] = (node \ "profileDesc" \ "textClass" \ "keywords" \\ "term").map(n => (n.text, (n \ "@{http://www.w3.org/XML/1998/namespace}lang").text))
    val authors:NodeSeq = (node \ "sourceDesc" \ "biblStruct" \ "analytic" \\ "author")

    builder.subject(createIRI(halUri))
      .add(RDF.TYPE, "mnhn:Document")
      .add("dcterms:date", date)
      .add("dcterms:created", created)
      .add("prov:hadPrimarySource", createIRI(halUri))
      .add("mnx:sourceDataset", createIRI("https://www.datahub.mnhn.fr/data/dataset/hal-mnhn"))
      .add("dcterms:language", language)

//    builder = licence.map{l =>
//      println(l)
//      builder.add("dcterms:license", createIRI(l))
//    }.getOrElse(builder)

    builder = titleFr.map{t =>
      builder.add("dcterms:title", createLocalizedLabel(t, "fr"))
    }.getOrElse(builder)

    builder = titleEn.map{t =>
      builder.add("dcterms:title", createLocalizedLabel(t, "en"))
    }.getOrElse(builder)

    builder = abstractFr.map{d =>
      builder.add("dcterms:description", createLocalizedLabel(d, "fr"))
    }.getOrElse(builder)

    builder = abstractEn.map{d =>
      builder.add("dcterms:description", createLocalizedLabel(d, "en"))
    }.getOrElse(builder)

    fileLocations.foreach{l =>
      builder.add("mnx:fileLocation", l)
    }

    subjects.foreach{s =>
      builder.add("dcterms:subject", createLocalizedLabel(s._1, s._2))
    }
    mapAuthors(key, authors)
    builder
  }

  /**
   * Maps author data to RDF, extracting relevant information (e.g., name, ORCID, affiliation)
   * and adding it to the RDF model.
   *
   * @param key The key for the cache entry.
   * @param authors A sequence of XML nodes representing author data.
   * @return A `ModelBuilder` instance with the mapped author data.
   */
  private def mapAuthors(key: String, authors: NodeSeq): ModelBuilder = {
    authors.foreach{ author =>
      val firstName = (author \ "persName" \ "forename").text
      val lastName = (author \ "persName" \ "surname").text
      val halauthorid = (author \ "idno").find(n => (n \ "@type").text == "halauthorid").map(_.text).get
      val orcid = (author \ "idno").find(n => (n \ "@type").text == "ORCID").map(_.text)
//      val idref = (author \ "idno").find(n => (n \ "@type").text == "IDREF").map(_.text)
//      val emailmd5 = (author \ "email").find(n => (n \ "@type").text == "md5").map(_.text)
//      val emailDomain = (author \ "email").find(n => (n \ "@type").text == "domain").map(_.text)
      val affiliations:Seq[String] = (author \\ "affiliation").map(n => (n \ "@ref").text)

      builder.subject(createIRI(s"https://www.datahub.mnhn.fr/data/person/hal/$halauthorid"))
        .add(RDF.TYPE, "mnhn:Person")
        .add("foaf:firstName", firstName)
        .add("foaf:lastName", lastName)

      builder = orcid.map{u =>
        builder.add("mnx:orcidURI", createIRI(u))
      }.getOrElse(builder)

      affiliations.foreach{aff =>
        val bnode = Values.bnode()
        builder.subject(createIRI(s"https://www.datahub.mnhn.fr/data/person/hal/$halauthorid")).add("mnx:hasAffiliation", bnode)
        builder.subject(bnode)
          .add(RDF.TYPE, "mnx:Affiliation")
          .add("mnx:hasOrg", createIRI(s"https://www.datahub.mnhn.fr/data/organization/$aff"))
      }
    }
    builder
  }

  /**
   * Maps structure data to RDF, extracting relevant metadata and adding it to the RDF model.
   *
   * @param key The key for the cache entry.
   * @param node The XML node representing the structure data.
   * @return A `ModelBuilder` instance with the mapped structure data.
   */
  private def mapStructure(key: String, node: NodeSeq): ModelBuilder = {
//    println(node)
    val id = (node \ "@{http://www.w3.org/XML/1998/namespace}id").text.trim
    val names:Seq[String] = (node \\ "orgName").map(_.text)
    builder.subject(createIRI(s"https://www.datahub.mnhn.fr/data/organization/$id"))
    names.foreach(builder.add("foaf:name", _))
    builder
  }

  /**
   * Maps project data to RDF (currently a placeholder for future project mapping).
   *
   * @param key The key for the cache entry.
   * @param node The XML node representing the project data.
   * @return A `ModelBuilder` instance with the mapped project data.
   */
  private def mapProject(key: String, node: NodeSeq): ModelBuilder = {
    builder
  }
}
