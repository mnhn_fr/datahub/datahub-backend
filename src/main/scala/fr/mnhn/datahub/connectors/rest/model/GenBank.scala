package fr.mnhn.datahub.connectors.rest.model

import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.{Json, OFormat}

case class EmbeddedSequences (
                       sequences: Option[Seq[SequencesGenBank]]
                     )

object EmbeddedSequences {
  implicit lazy val embeddedSequences = Json.format[EmbeddedSequences]
}

case class GenBankRootInterface (
                            _embedded: Option[EmbeddedSequences],
                             _links: Option[Links],
                            page: Option[Page]
                          )

object GenBankRootInterface {
  implicit val format: OFormat[GenBankRootInterface] = Json.format[GenBankRootInterface]

  implicit lazy val genBankDefault = new Default[GenBankRootInterface] {
    override def defaultValue: GenBankRootInterface = GenBankRootInterface(None, None, None)
  }
}