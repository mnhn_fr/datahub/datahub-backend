package fr.mnhn.datahub.connectors.rest.hal

import com.typesafe.config.{Config, ConfigFactory}
import fr.mnhn.datahub.connectors.oai.OaiHarvestingConfiguration.conf

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 29/09/2020
 */

object RestHarvestingConfiguration {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("rest")).getOrElse(ConfigFactory.empty())
  lazy val halCacheDir: String = conf.getString("hal.cache.directory")
  lazy val halOutputDir: String = conf.getString("hal.output.directory")
  lazy val halMergeSize: Int = conf.getInt("hal.merge.size")
}
