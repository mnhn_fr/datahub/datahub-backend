package fr.mnhn.datahub.connectors.rest.model

import play.api.libs.json.Json

case class LinksGen(self: Option[Href],bold: Option[Href], GenBank: Option[Href], samples: Option[Href], sequences: Option[Href])

object LinksGen {
  implicit val format  = Json.format[LinksGen]
}

/*
    "_links": {
        "self": {
            "href": "http://datahub-test-2.arzt.mnhn.fr:8092/specimens/27"
        },
        "bold": {
            "href": "http://datahub-test-2.arzt.mnhn.fr:8092/specimens/27/bold"
        },
        "samples": {
            "href": "http://datahub-test-2.arzt.mnhn.fr:8092/samples?collectionCode=IM&catalogNumber=2007-32544"
        },
        "sequences": {
            "href": "http://datahub-test-2.arzt.mnhn.fr:8092/sequences?collectionCode=IM&catalogNumber=2007-32544{&sampleId}",
            "templated": true
        }
    }

 */