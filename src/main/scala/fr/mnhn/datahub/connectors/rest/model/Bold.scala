package fr.mnhn.datahub.connectors.rest.model

import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.{Json, OFormat}

case class Sequences (
                       id: Option[Int],
                       markerCode: Option[String],
                       genbankAccession: Option[String],
                       nucleotides: Option[String],
                       _links: Option[LinksGen]
                     )

object Sequences {
  implicit val format: OFormat[Sequences] = Json.format[Sequences]
}


case class BoldRecords (
                         id: Option[Int],
                         processId: Option[String],
                         binUri: Option[String],
                         sampleId: Option[String],
                         catalogNumber: Option[String],
                         collectionCode: Option[String],
                         institution: Option[String],
                         fieldNumber: Option[String],
                         sequences: Option[Seq[Sequences]],
                           specimenId:  Option[String]

  )

object BoldRecords {
  implicit val format  = Json.format[BoldRecords]


      implicit val defaultForBoldRecords: Default[BoldRecords] = new Default[BoldRecords] {
    override def defaultValue: BoldRecords = BoldRecords(
      None, None,None, None,None, None,None, None,None, None
    )
  }

}

case class BoldRootInterface (
                           processId: Option[String],
                           boldRecords: Option[Seq[BoldRecords]],
                           _links: Option[LinksGen]
                         )

object BoldRootInterface {
  implicit val format: OFormat[BoldRootInterface] = Json.format[BoldRootInterface]

  implicit val defaultBoldRootInterface = new Default[BoldRootInterface] {

    override def defaultValue: BoldRootInterface = BoldRootInterface(None, None, None)
  }
}