package fr.mnhn.datahub.connectors.rest.troisdtheque

import com.typesafe.config.ConfigFactory

object TroisDThequeConfig {

  lazy val confGeolocation = Option(ConfigFactory.load().getConfig("troisdtheque")).getOrElse(ConfigFactory.empty())
  lazy val rdfDir = confGeolocation.getString("rdf.dir")
  lazy val cacheDir = confGeolocation.getString("cache.dir")

}
