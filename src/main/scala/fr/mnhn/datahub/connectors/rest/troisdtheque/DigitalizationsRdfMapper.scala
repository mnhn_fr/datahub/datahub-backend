/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.rest.troisdtheque

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.rest.model.Digitalizations
import fr.mnhn.datahub.connectors.utils.DateUtils
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.net.URI
import java.time.LocalDate

/**
 * The `DigitalizationsRdfMapper` object is responsible for mapping `Digitalizations` objects
 * to RDF models and serializing them into different formats. It provides several implicit
 * conversions to RDF models and subgraphs, enabling seamless transformation of `Digitalizations`
 * data into RDF representations.
 *
 * It also includes a method for building RDF models for a sequence of `Digitalizations` or
 * a single `Digitalization`. The RDF model includes various properties related to digitalization
 * projects, such as project ID, equipment type, project title, description, and associated specimen
 * metadata. The object leverages the `ModelBuilder`, `RDFSerializer`, and `ToRdfModel` classes
 * to generate and serialize RDF data.
 *
 * The object provides the following functionalities:
 *
 * 1. **modelBuilding**: Builds an RDF model from a sequence of `Digitalizations`.
 *    It iterates over each `Digitalization`, adding triples to the model for relevant attributes.
 *
 * 2. **Implicit Conversions**:
 *    - `digitalizationsToRdfModel`: Converts a sequence of `Digitalizations` to an RDF model.
 *    - `digitalizationToRdfModel`: Converts a single `Digitalization` to an RDF model.
 *    - `digitalizationToSubgraph`: Converts a single `Digitalization` to a subgraph representation.
 *
 * 3. **RDF Serialization**: The generated RDF models are serialized into TriG format, with
 *    support for namespace management and SHA-256 hashing for content verification.
 *
 * @object DigitalizationsRdfMapper
 *
 * Provides utility methods for transforming and serializing `Digitalizations` data into RDF models
 * suitable for use in semantic web applications.
 */

object DigitalizationsRdfMapper {

  /**
   * Builds an RDF model from a sequence of `Digitalizations`.
   * For each `Digitalization`, it creates RDF triples for various attributes, including:
   * - digitalization project ID
   * - equipment type
   * - project title
   * - project URL
   * - description
   * - thumbnail image
   * - specimen metadata (institution code, collection code, catalog number)
   *
   * @param model The `ModelBuilder` used to construct the RDF model.
   * @param digitalizations A sequence of `Digitalizations` to be mapped to RDF.
   * @param date The current date used in RDF triples.
   */

  def modelBuilding(model: ModelBuilder, digitalizations: Seq[Digitalizations], date: String) = {
    digitalizations.foreach { digitalization =>
      if (digitalization.id.isDefined) {
        val digitalizationIRI = createIRI(s"https://www.data.mnhn.fr/data/digitalization/${digitalization.id.get}")
        model.subject(digitalizationIRI)
          .add(RDF.TYPE, "mnhn:Digitalization")

        model.addTripleIfPresent(digitalizationIRI, "mnhn:digitalizationProjectId", digitalization.projectId)
        model.addTripleIfPresent(digitalizationIRI, "mnhn:digitalizationEquipmentType", digitalization.equipmentType)
        model.addTripleIfPresent(digitalizationIRI, "mnhn:digitalizationProjectTitle", digitalization.projectTitle)
        model.addTripleIfPresent(digitalizationIRI, "rdfs:seeAlso", digitalization.projectUrl)
        model.addTripleIfPresent(digitalizationIRI, "dcterms:identifier", digitalization.id)
        model.addTripleIfPresent(digitalizationIRI, "dcterms:description", digitalization.description)

        val thumbnail = digitalization._links.flatMap(link => link.thumbnail.flatMap(_.href))

        model.addTripleIfPresent(digitalizationIRI, "schema:image", thumbnail.map(t => new URI(t)) )
        model.addTripleIfPresent(digitalizationIRI, "dwc:institutionCode", digitalization.specimen.flatMap(_.institutionCode))
        model.addTripleIfPresent(digitalizationIRI, "dwc:collectionCode", digitalization.specimen.flatMap(_.collectionCode))
        model.addTripleIfPresent(digitalizationIRI, "dwc:catalogNumber", digitalization.specimen.flatMap(_.catalogNumber))
        }
      }
    }

  /**
   * Implicit conversion to convert a sequence of `Digitalizations` into an RDF model.
   *
   * @param objs A sequence of `Digitalizations` objects to be converted into an RDF model.
   * @return An RDF `Model` representing the sequence of `Digitalizations`.
   */
    implicit val digitalizationsToRdfModel: ToRdfModel[Seq[Digitalizations]] = new ToRdfModel[Seq[Digitalizations]] {
      override def toRdfModel(objs: Seq[Digitalizations]): Model = {

        val builder = new ModelBuilder()
        val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/3dtheque/NG")
        modelBuilding(model, objs, DateUtils.currentDate())
        model.build()
      }
    }

  /**
   * Implicit conversion to convert a single `Digitalization` into an RDF model.
   *
   * @param obj A single `Digitalizations` object to be converted into an RDF model.
   * @return An RDF `Model` representing the `Digitalization`.
   */
    implicit val digitalizationToRdfModel: ToRdfModel[Digitalizations] = new ToRdfModel[Digitalizations] {
      override def toRdfModel(obj: Digitalizations): Model = {

        val builder = new ModelBuilder()
        val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/3dtheque/NG")
       modelBuilding(model, Seq(obj), DateUtils.currentDate())
        model.build()
      }
    }

  /**
   * Implicit conversion to convert a single `Digitalization` into a subgraph.
   *
   * @param obj A single `Digitalization` object to be converted into a subgraph.
   * @return A `Subgraph` representing the `Digitalization`.
   */
    implicit val digitalizationToSubgraph: ToSubgraph[Digitalizations] = new ToSubgraph[Digitalizations] {
      override def toSubgraph(obj: Digitalizations): Subgraph = {

       def id(): String = if (obj.id.isDefined) CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/digitalization/${obj.id.get}", LocalDate.now().toString)  else ""

       def graphUri(): String = "https://www.data.mnhn.fr/data/3dtheque/NG"

       def resourceUri(): String = if (obj.id.isDefined)  s"https://www.data.mnhn.fr/data/digitalization/${obj.id.get}" else ""

       def datatype: String = "Digitalization"


        val modelString = RDFSerializer.toTriG(obj.toRdfModel)
        val sha = FileContentHelper.sha256(modelString)
        Subgraph(id(), graphUri(), resourceUri(), datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
      }
    }
}