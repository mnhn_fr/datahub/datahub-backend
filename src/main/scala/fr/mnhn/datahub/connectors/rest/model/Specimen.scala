package fr.mnhn.datahub.connectors.rest.model

import play.api.libs.json.Json

case class EmbeddedSpecimens (
                               specimens: Option[Seq[Specimens]]
                             )

object EmbeddedSpecimens {
  implicit lazy val format = Json.format[EmbeddedSpecimens]
}

case class SpecimenRootInterface (
                                   _embedded: Option[EmbeddedSpecimens],
                                   _links: fr.mnhn.datahub.connectors.rest.model.Links,
                                   page: Page
                                 )

object SpecimenRootInterface {
  implicit lazy val format = Json.format[SpecimenRootInterface]
}
