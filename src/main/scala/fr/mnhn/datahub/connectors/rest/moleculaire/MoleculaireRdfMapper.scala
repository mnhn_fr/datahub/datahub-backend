package fr.mnhn.datahub.connectors.rest.moleculaire

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF
import fr.mnhn.datahub.connectors.utils.DateUtils
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.rest.model.Specimens

import java.time.LocalDate

/**
 * The `MoleculairesRdfMapper` object provides methods for mapping `Specimens` objects
 * (representing molecular specimens) to RDF models and serializing them into subgraphs.
 * This object facilitates the conversion of molecular specimen data into RDF (Resource Description Framework)
 * representations for semantic web applications.
 *
 * The object includes the following key methods:
 *
 * 1. **modelBuilding**: Builds an RDF model from a sequence of `Specimens`. It creates RDF triples for relevant
 *    attributes such as catalog number and collection code, associating them with a `moleculaire` IRI.
 *
 * 2. **Implicit Conversions**:
 *    - `moleculairesToRdfModel`: Converts a sequence of `Specimens` to an RDF model.
 *    - `moleculaireToRdfModel`: Converts a single `Specimens` to an RDF model.
 *    - `moleculaireToSubgraph`: Converts a single `Specimens` to a subgraph representation.
 *
 * 3. **RDF Serialization**: The generated RDF models are serialized into TriG format, with
 *    support for namespace management and SHA-256 hashing for content verification.
 *
 * @object MoleculairesRdfMapper
 * Provides utility methods to transform and serialize molecular specimen data into RDF models suitable
 * for integration with other semantic web technologies.
 */

object MoleculaireRdfMapper {
  /**
   * Builds an RDF model from a sequence of `Specimens`. For each `Specimens`, it creates RDF triples
   * for relevant attributes, including catalog number and collection code.
   *
   * @param model The `ModelBuilder` used to construct the RDF model.
   * @param moleculaires A sequence of `Specimens` to be mapped to RDF.
   * @param date The current date used in RDF triples.
   */
    def modelBuilding(model: ModelBuilder, moleculaires: Seq[Specimens], date: String) = {
    moleculaires.foreach { moleculaire =>
      if (moleculaire.id.isDefined) {
        val moleculaireIRI = createIRI(s"https://www.data.mnhn.fr/data/moleculaire/specimen/${moleculaire.id.get}")
        model.subject(moleculaireIRI)
          .add(RDF.TYPE, "mnhn:SequencedSpecimen")

        model.addTripleIfPresent(moleculaireIRI, "dwc:catalogNumber", moleculaire.catalogNumber)
        model.addTripleIfPresent(moleculaireIRI, "dwc:collectionCode", moleculaire.collectionCode)

        }
      }
    }

  /**
   * Implicit conversion to convert a sequence of `Specimens` into an RDF model.
   *
   * @param objs A sequence of `Specimens` objects to be converted into an RDF model.
   * @return An RDF `Model` representing the sequence of `Specimens`.
   */
    implicit val moleculairesToRdfModel: ToRdfModel[Seq[Specimens]] = new ToRdfModel[Seq[Specimens]] {
      override def toRdfModel(objs: Seq[Specimens]): Model = {

        val builder = new ModelBuilder()
        val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/moleculaire/NG")
        modelBuilding(model, objs, DateUtils.currentDate())
        model.build()
      }
    }

  /**
   * Implicit conversion to convert a single `Specimens` into an RDF model.
   *
   * @param obj A single `Specimens` object to be converted into an RDF model.
   * @return An RDF `Model` representing the `Specimens`.
   */
    implicit val moleculaireToRdfModel: ToRdfModel[Specimens] = new ToRdfModel[Specimens] {
      override def toRdfModel(obj: Specimens): Model = {

        val builder = new ModelBuilder()
        val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/moleculaire/NG")
       modelBuilding(model, Seq(obj), DateUtils.currentDate())
        model.build()
      }
    }

  /**
   * Implicit conversion to convert a single `Specimens` into a subgraph.
   *
   * @param obj A single `Specimens` object to be converted into a subgraph.
   * @return A `Subgraph` representing the `Specimens`.
   */
    implicit val moleculaireToSubgraph: ToSubgraph[Specimens] = new ToSubgraph[Specimens] {
      override def toSubgraph(obj: Specimens): Subgraph = {

       def id(): String = if (obj.id.isDefined) CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/moleculaire/${obj.id.get}", LocalDate.now().toString)  else ""

       def graphUri(): String = "https://www.data.mnhn.fr/data/moleculaire/NG"

       def resourceUri(): String = if (obj.id.isDefined)  s"https://www.data.mnhn.fr/data/moleculaire/${obj.id.get}" else ""

       def datatype: String = "Material"


        val modelString = RDFSerializer.toTriG(obj.toRdfModel)
        val sha = FileContentHelper.sha256(modelString)
        Subgraph(id(), graphUri(), resourceUri(), datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
      }
    }
}