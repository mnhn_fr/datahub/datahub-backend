package fr.mnhn.datahub.connectors.rest.moleculaire

import com.typesafe.config.ConfigFactory

object MoleculaireConfig {

  lazy val confMoleculaire = Option(ConfigFactory.load().getConfig("moleculaire")).getOrElse(ConfigFactory.empty())
  lazy val rdfDirMoleculaire = confMoleculaire.getString("rdf.dir")
  lazy val cacheDirMoleculaire = confMoleculaire.getString("cache.dir")

  lazy val confGenbank = Option(ConfigFactory.load().getConfig("genbank")).getOrElse(ConfigFactory.empty())
  lazy val rdfDirGenbank = confGenbank.getString("rdf.dir")
  lazy val cacheDirGenbank = confGenbank.getString("cache.dir")

  lazy val confbold = Option(ConfigFactory.load().getConfig("bold")).getOrElse(ConfigFactory.empty())
  lazy val rdfDirbold = confbold.getString("rdf.dir")
  lazy val cacheDirbold = confbold.getString("cache.dir")
}