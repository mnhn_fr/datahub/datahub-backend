package fr.mnhn.datahub.connectors.rest.moleculaire

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import MoleculaireRdfMapper.moleculaireToRdfModel
import fr.mnhn.datahub.connectors.rest.model.{BoldRecords}
import fr.mnhn.datahub.connectors.utils.DateUtils
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * The `BoldRdfMapper` object provides methods for mapping `BoldRecords` (representing genomic sequence data from BOLD systems)
 * into RDF models and serializing them into subgraphs. This object helps in converting BOLD sequence data into RDF (Resource Description Framework)
 * representations for use in semantic web applications.
 *
 * The object includes the following key methods:
 *
 * 1. **modelBuilding**: Builds an RDF model from a sequence of `BoldRecords`. It creates RDF triples for
 *    relevant attributes such as genomic sequence records, gene names, and links to process IDs.
 *
 * 2. **Implicit Conversions**:
 *    - `boldsToRdfModel`: Converts a sequence of `BoldRecords` to an RDF model.
 *    - `boldToRdfModel`: Converts a single `BoldRecords` to an RDF model.
 *    - `boldToSubgraph`: Converts a single `BoldRecords` to a subgraph representation.
 *
 * 3. **RDF Serialization**: The generated RDF models are serialized into TriG format, with
 *    support for namespace management and SHA-256 hashing for content verification.
 *
 * @object BoldRdfMapper
 * Provides utility methods to transform and serialize genomic sequence data from BOLD into RDF models suitable
 * for integration with other semantic web technologies.
 */
object BoldRdfMapper {


  /**
   * Builds an RDF model from a sequence of `BoldRecords`. For each `BoldRecords`, it creates RDF triples
   * for relevant attributes such as genomic sequence record, gene name, and links to process IDs.
   *
   * @param model The `ModelBuilder` used to construct the RDF model.
   * @param bolds A sequence of `BoldRecords` to be mapped to RDF.
   * @param date The current date used in RDF triples.
   */
  def modelBuilding(model: ModelBuilder, bolds: Seq[BoldRecords], date: String) = {
    bolds.foreach { bold =>
      if (bold.id.isDefined) {
        val moleculaireIRI = createIRI(s"https://www.data.mnhn.fr/data/moleculaire/specimen/${bold.specimenId.get}")
        bold.sequences.foreach { sequences =>
          sequences.foreach { sequence =>
            if (sequence.id.isDefined) {

              val sequenceUri = createIRI(s"https://www.data.mnhn.fr/data/moleculaire/bold-sequence/${sequence.id.get}")

              model.subject(moleculaireIRI)
                .add(RDF.TYPE, "mnhn:SequencedSpecimen")
                .add("mnhn:hasGenomicSequenceRecord", sequenceUri)

              model.subject(sequenceUri)
                .add(RDF.TYPE, "mnhn:GenomicSequenceRecord").add( "dwc:datasetName", "BOLD")


                model.addTripleIfPresent(sequenceUri, "rdfs:seeAlso",  bold.processId.map {processId =>  s"http://v3.boldsystems.org/index.php/API_Public/combined?ids=${processId}"})

              model.addTripleIfPresent(sequenceUri, "mnhn:geneName", sequence.markerCode)

              val l = sequence._links.flatMap(_.bold.flatMap(_.href))
            }
          }
        }
        }
      }
    }

  /**
   * Implicit conversion to convert a sequence of `BoldRecords` into an RDF model.
   *
   * @param objs A sequence of `BoldRecords` objects to be converted into an RDF model.
   * @return An RDF `Model` representing the sequence of `BoldRecords`.
   */
    implicit val boldsToRdfModel: ToRdfModel[Seq[BoldRecords]] = new ToRdfModel[Seq[BoldRecords]] {
      override def toRdfModel(objs: Seq[BoldRecords]): Model = {

        val builder = new ModelBuilder()
        val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/moleculaire/NG")
        modelBuilding(model, objs, DateUtils.currentDate())
        model.build()
      }
    }

  /**
   * Implicit conversion to convert a single `BoldRecords` into an RDF model.
   *
   * @param obj A single `BoldRecords` object to be converted into an RDF model.
   * @return An RDF `Model` representing the `BoldRecords`.
   */
    implicit val boldToRdfModel = new ToRdfModel[BoldRecords] {
      override def toRdfModel(obj: BoldRecords): Model = {

        val builder = new ModelBuilder()
        val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/moleculaire/NG")
       modelBuilding(model, Seq(obj), DateUtils.currentDate())
        model.build()
      }
    }

  /**
   * Implicit conversion to convert a single `BoldRecords` into a subgraph.
   *
   * @param obj A single `BoldRecords` object to be converted into a subgraph.
   * @return A `Subgraph` representing the `BoldRecords`.
   */
    implicit val boldToSubgraph: ToSubgraph[BoldRecords] = new ToSubgraph[BoldRecords] {
      override def toSubgraph(obj: BoldRecords): Subgraph = {

       def id(): String = if (obj.id.isDefined) CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/moleculaire/${obj.id.get}", LocalDate.now().toString)  else ""

       def graphUri(): String = "https://www.data.mnhn.fr/data/moleculaire/NG"

       def resourceUri(): String = if (obj.id.isDefined)  s"https://www.data.mnhn.fr/data/moleculaire/${obj.id.get}" else ""

       def datatype: String = "Material"


        val modelString = RDFSerializer.toTriG(obj.toRdfModel)
        val sha = FileContentHelper.sha256(modelString)
        Subgraph(id(), graphUri(), resourceUri(), datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
      }
    }
}