package fr.mnhn.datahub.connectors.rest.hal

import fr.mnhn.datahub.connectors.utils.CachedHalRestClient
import scala.xml.Elem

class HalResultsIterator(q:String="*%3A*", wt:String="xml-tei", fq:String, val rows: Int=100)(implicit client:CachedHalRestClient) extends Iterator[HalResult] {

  var nbPages: Int = 0
  var nbItems:Int = -1
  var currentPage = 0
  val idleTime:Long = 500L

  init()

  override def hasNext: Boolean = currentPage < nbPages

  override def next(): HalResult = {
    val offset = currentPage * rows
    println(s"#Offset: $offset")
    currentPage += 1
    println(s"#Current Page: $currentPage")
    Thread.sleep(idleTime)
    getPage(offset)
  }

  def init() = {
    val query = client.buildQueryUri(q, wt, fq, rows)
    val resp: Option[Elem] = client.processResponse(client.get(query))
    nbItems = getCount(resp.get)
    println(s"#Items found: $nbItems")
    nbPages = Math.ceil(nbItems.toDouble / rows.toDouble).toInt
    println(s"#Pages to scroll: $nbPages")
  }
  def getPage(start: Int): HalResult = {
    val query = client.buildQueryUri(q, wt, fq, rows, start)
    println(query)
    val root: Elem = client.processResponse(client.get(query)).get
    val entries = (root \ "text" \ "body" \ "listBibl" \\ "biblFull").toList
    val orgs = (root \ "text" \ "back" \ "listOrg" \\ "org").toList
    HalResult(entries, orgs)
  }

  def getCount(root: Elem): Int = {
    val count: Option[Int] = (root \ "teiHeader" \ "profileDesc" \ "creation" \\ "measure")
      .find(n => (n \ "@commodity").text == "totalSearchResults")
      .map(n => (n \ "@quantity").text).map(_.toInt)
    count.getOrElse(-1)
  }
}
