package fr.mnhn.datahub.connectors.rest.hal

import scala.xml.Node

case class HalResult(entries:List[Node], orgs:List[Node])
