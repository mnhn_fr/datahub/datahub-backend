package fr.mnhn.datahub.connectors.rest.model

import play.api.libs.json.Json

case class Page (
                  size: Int,
                  totalElements: Int,
                  totalPages: Int,
                  number: Int
                )

object Page {
  implicit lazy val format = Json.format[Page]
}
