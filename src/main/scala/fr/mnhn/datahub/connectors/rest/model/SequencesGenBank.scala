package fr.mnhn.datahub.connectors.rest.model

import fr.mnhn.datahub.connectors.Default
import play.api.libs.json.Json

case class SequencesGenBank (
                              id: Option[Int],
                              gene: Option[String],
                              madeByInstitution: Option[String],
                              urlFasta: Option[String],
                              boldPublicationDate: Option[String],
                              boldPublication:Option[String],
                              created: Option[String],
                              modified: Option[String],
                              _links: Option[LinksGen],
                              genbankAccessionNumber: Option[String],
                              remarks: Option[String],
                              specimenId: Option[String]

                            )

object SequencesGenBank {
  implicit val format = Json.format[SequencesGenBank]

      implicit val defaultForSequencesGenBank: Default[SequencesGenBank] = new Default[SequencesGenBank] {
    override def defaultValue: SequencesGenBank = SequencesGenBank(
      None, None,None, None,None, None,None, None,None, None, None, None
    )
  }

}
