package fr.mnhn.datahub.connectors.rest.model

import play.api.libs.json.Json

case class DigitalizationLinks (
                                 self: Option[Href],
                                 specimen: Option[Href],
                                 project: Option[Href],
                                 equipment: Option[Href],
                                 thumbnail: Option[Href]
                               )

object DigitalizationLinks {
  implicit lazy val format = Json.format[DigitalizationLinks]
}