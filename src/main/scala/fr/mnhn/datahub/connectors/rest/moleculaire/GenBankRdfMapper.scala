package fr.mnhn.datahub.connectors.rest.moleculaire

import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper
import com.mnemotix.synaptix.core.utils.CryptoUtils
import fr.mnhn.datahub.connectors.RDFSerializer.{addTripleIfPresent, createIRI}
import fr.mnhn.datahub.connectors.ToRdfModel.ToRdfModelOps
import fr.mnhn.datahub.connectors.rest.model.{SequencesGenBank}
import fr.mnhn.datahub.connectors.utils.DateUtils
import fr.mnhn.datahub.connectors.{RDFSerializer, ToRdfModel, ToSubgraph}
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.RDF

import java.time.LocalDate

/**
 * The `GenBankRdfMapper` object provides methods for mapping `SequencesGenBank` objects
 * (representing genomic sequence data from GenBank) to RDF models and serializing them into subgraphs.
 * This object facilitates the conversion of genomic sequence data into RDF (Resource Description Framework)
 * representations for use in semantic web applications.
 *
 * The object includes the following key methods:
 *
 * 1. **modelBuilding**: Builds an RDF model from a sequence of `SequencesGenBank`. It creates RDF triples for
 *    relevant attributes such as genomic sequence record and gene name, and associates them with a `genbank` IRI.
 *
 * 2. **Implicit Conversions**:
 *    - `genbanksToRdfModel`: Converts a sequence of `SequencesGenBank` to an RDF model.
 *    - `genbankToRdfModel`: Converts a single `SequencesGenBank` to an RDF model.
 *    - `genbankToSubgraph`: Converts a single `SequencesGenBank` to a subgraph representation.
 *
 * 3. **RDF Serialization**: The generated RDF models are serialized into TriG format, with
 *    support for namespace management and SHA-256 hashing for content verification.
 *
 * @object GenBankRdfMapper
 * Provides utility methods to transform and serialize genomic sequence data into RDF models suitable
 * for integration with other semantic web technologies.
 */

object GenBankRdfMapper {
  /**
   * Builds an RDF model from a sequence of `SequencesGenBank`. For each `SequencesGenBank`, it creates RDF triples
   * for relevant attributes, including genomic sequence record and gene name.
   *
   * @param model The `ModelBuilder` used to construct the RDF model.
   * @param genbanks A sequence of `SequencesGenBank` to be mapped to RDF.
   * @param date The current date used in RDF triples.
   */
  def modelBuilding(model: ModelBuilder, genbanks: Seq[SequencesGenBank], date: String) = {
    genbanks.foreach { genbank =>
      if (genbank.id.isDefined) {
        val moleculaireIRI = createIRI(s"https://www.data.mnhn.fr/data/moleculaire/specimen/${genbank.specimenId.get}")


        val sequenceUri = createIRI(s"https://www.data.mnhn.fr/data/moleculaire/genbank-sequence/${genbank.id.get}")

        model.subject(moleculaireIRI)
          .add(RDF.TYPE, "mnhn:SequencedSpecimen")
          .add("mnhn:hasGenomicSequenceRecord", sequenceUri)

        model.subject(sequenceUri)
          .add(RDF.TYPE, "mnhn:GenomicSequenceRecord").add("dwc:datasetName", "GenBank")

        model.addTripleIfPresent(sequenceUri, "mnhn:geneName", genbank.gene)
        val l = genbank._links.flatMap(_.GenBank.flatMap(_.href))
        model.addTripleIfPresent(sequenceUri, "rdfs:seeAlso", l)
      }
    }
  }

  /**
   * Implicit conversion to convert a sequence of `SequencesGenBank` into an RDF model.
   *
   * @param objs A sequence of `SequencesGenBank` objects to be converted into an RDF model.
   * @return An RDF `Model` representing the sequence of `SequencesGenBank`.
   */
  implicit val genbanksToRdfModel: ToRdfModel[Seq[SequencesGenBank]] = new ToRdfModel[Seq[SequencesGenBank]] {
    override def toRdfModel(objs: Seq[SequencesGenBank]): Model = {
      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/moleculaire/NG")
      modelBuilding(model, objs, DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion to convert a single `SequencesGenBank` into an RDF model.
   *
   * @param obj A single `SequencesGenBank` object to be converted into an RDF model.
   * @return An RDF `Model` representing the `SequencesGenBank`.
   */

  implicit val genbankToRdfModel = new ToRdfModel[SequencesGenBank] {
    override def toRdfModel(obj: SequencesGenBank): Model = {

      val builder = new ModelBuilder()
      val model = RDFSerializer.nameSpace(builder, RDFSerializer.nameSpaceMap).namedGraph("https://www.data.mnhn.fr/data/moleculaire/NG")
      modelBuilding(model, Seq(obj), DateUtils.currentDate())
      model.build()
    }
  }

  /**
   * Implicit conversion to convert a single `SequencesGenBank` into a subgraph.
   *
   * @param obj A single `SequencesGenBank` object to be converted into a subgraph.
   * @return A `Subgraph` representing the `SequencesGenBank`.
   */
  implicit val genbankToSubgraph: ToSubgraph[SequencesGenBank] = new ToSubgraph[SequencesGenBank] {
    override def toSubgraph(obj: SequencesGenBank): Subgraph = {

      def id(): String = if (obj.id.isDefined) CryptoUtils.md5sum(s"https://www.data.mnhn.fr/data/moleculaire/${obj.id.get}", LocalDate.now().toString) else ""

      def graphUri(): String = "https://www.data.mnhn.fr/data/moleculaire/NG"

      def resourceUri(): String = if (obj.id.isDefined) s"https://www.data.mnhn.fr/data/moleculaire/${obj.id.get}" else ""

      def datatype: String = "Material"

      val modelString = RDFSerializer.toTriG(obj.toRdfModel)
      val sha = FileContentHelper.sha256(modelString)
      Subgraph(id(), graphUri(), resourceUri(), datatype, LocalDate.now(), modelString.getBytes, sha, "trig")
    }
  }
}