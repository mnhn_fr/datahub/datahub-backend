/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse, StatusCodes}
import akka.stream.scaladsl.{Sink, Source}
import com.mnemotix.synaptix.cache.RocksDBStore
import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json.{Json, Reads}

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success, Try}

/**
 * A generic REST client for interacting with APIs that support pagination.
 * This client handles HTTP requests to the specified API, processes the response, and caches the results.
 * It supports fetching data from paginated API endpoints and handles both document-based requests and generic object requests.
 *
 * @param cache The cache store (e.g., RocksDBStore) used to store API responses.
 */
class GenericRestClient(cache: RocksDBStore) extends LazyLogging {
  /**
   * Fetches data from a paginated API endpoint and caches each page of results.
   * The method will recursively fetch all pages and accumulate the total count.
   *
   * @param size The size of the pages.
   * @param baseUrl The base URL of the API.
   * @param endpoint The specific API endpoint to fetch data from.
   * @param system The ActorSystem used for handling asynchronous tasks.
   * @param ec The ExecutionContext used for handling concurrency.
   * @param reads A function that converts the JSON response into a Scala case class object.
   * @param mnhnEnpointPageExtracter A function that extracts pagination information from the API response.
   * @param mnhnEndpointResultConverter A function that processes and converts the API results, including embedded objects.
   * @tparam A The type of the object returned by the API, which often contains embedded objects and pagination information.
   * @return A Future representing the total number of pages fetched.
   */

  val idx = new AtomicInteger()
  def get[A](size: Int, baseUrl: String, endpoint: String)(implicit system: ActorSystem, ec: ExecutionContextExecutor, reads: Reads[A], mnhnEnpointPageExtracter: MnhnEnpointPageExtracter[A], mnhnEndpointResultConverter:MnhnEndpointResultConverter[A]):  Future[Int] = {
    def fetchSpecimen(currentPage: Int, total: Int): Future[Int] = {
      logger.info(s"current page = $currentPage")

      val request = HttpRequest(
        method = HttpMethods.GET,
        uri = s"$baseUrl/$endpoint?page=$currentPage&size=$size"
      )

      Http().singleRequest(request).flatMap {
        case HttpResponse(StatusCodes.OK, _, entity, _) => {
          val futString = entity.toStrict(10.seconds).map(_.data.utf8String)

          val pagesInfos = for {
            string <- futString
            obj = {
              Try {
                Json.parse(string).as[A]
              }
              match {
                case Success(value) => value
                case Failure(exception) => {
                  //logger.error(s"we have an error trying to parse ${string}", exception)
                  throw exception
                }
              }
            }
            jstring = mnhnEndpointResultConverter.createJsonString(obj)
            b = cache.put(currentPage.toString, jstring.getBytes())
            pages = mnhnEnpointPageExtracter.extractPage(obj)
          } yield pages


          pagesInfos.flatMap { page =>
            //hot fixe
            if ((page.totalPages - 1) == currentPage) {
              Future.successful(total)
            } else {
              fetchSpecimen(currentPage + 1, page.size + total)
            }
          }
        }

        case HttpResponse(status, _, entity, _) => {
          val jstring = entity.toStrict(10.seconds).map(_.data.utf8String)
          logger.info(s"Request failed with status code $status and message: $jstring")
          Future.successful(total)
        }
      }
    }
    fetchSpecimen(1, 0)
  }

  /**
   * Fetches a document from the specified URL and caches the result.
   * If an error occurs during parsing, a default value is used.
   *
   * @param documentUrl The URL of the document to fetch.
   * @param cacheId The ID used to identify the cache entry.
   * @param actorSystem The ActorSystem used for handling asynchronous tasks.
   * @param ec The ExecutionContext used for handling concurrency.
   * @param reads A function that converts the JSON response into a Scala case class object.
   * @param moleculaireAPIResultConverter A function for converting the API result into a desired format.
   * @param default A Default instance used to provide a default value in case of parsing failure.
   * @tparam A The type of the document returned by the API.
   * @return A Future representing the cache operation after saving the document.
   */
  def getDocument[A](documentUrl: String, cacheId: Int)(implicit actorSystem: ActorSystem, ec: ExecutionContext, reads: Reads[A], moleculaireAPIResultConverter:MoleculaireAPIResultConverter[A], default: Default[A])= {

    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = documentUrl
    )

    Http().singleRequest(request).flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) => {
        val futString = entity.toStrict(20.seconds).map(_.data.utf8String)
        for {
          s <- futString
          rootInterface = {
            Try {
              Json.parse(s).as[A]
            }
            match {
              case Success(value) => value
              case Failure(exception) => {
                logger.error(s"we have an error trying to parse ${s} for $documentUrl", exception)
                default.defaultValue
              }
            }
          }
          jsonString = moleculaireAPIResultConverter.createJsonStringSeq(rootInterface, Some(cacheId.toString))
          res = jsonString.map(str => cache.put(idx.incrementAndGet().toString, str.getBytes("UTF-8")) )
        } yield res
      }
    }

  }

  /**
   * Fetches data from a paginated API endpoint, caches the results for each page, and processes all pages recursively.
   * This method is suitable for endpoints with many pages of results.
   *
   * @param size The size of the pages.
   * @param baseUrl The base URL of the API.
   * @param endpoint The specific API endpoint to fetch data from.
   * @param system The ActorSystem used for handling asynchronous tasks.
   * @param ec The ExecutionContext used for handling concurrency.
   * @param reads A function that converts the JSON response into a Scala case class object.
   * @param mnhnEnpointPageExtracter A function that extracts pagination information from the API response.
   * @param mnhnEndpointResultConverter A function that processes and converts the API results, including embedded objects.
   * @tparam A The type of the object returned by the API, which often contains embedded objects and pagination information.
   * @return A Future representing the total number of pages fetched.
   */
  def getAsMany[A](size: Int, baseUrl: String, endpoint: String)(implicit system: ActorSystem, ec: ExecutionContextExecutor, reads: Reads[A], mnhnEnpointPageExtracter: MnhnEnpointPageExtracter[A], mnhnEndpointResultConverter: MnhnEndpointResultConverter[A]): Future[Int] = {
    def fetchSpecimen(currentPage: Int, total: Int): Future[Int] = {
      logger.info(s"current page = $currentPage")
      val i = new AtomicInteger(0)
      logger.info(s"$baseUrl/$endpoint?page=$currentPage&size=$size")
      val request = HttpRequest(
        method = HttpMethods.GET,
        uri = s"$baseUrl/$endpoint?page=$currentPage&size=$size"
      )

      Http().singleRequest(request).flatMap {
        case HttpResponse(StatusCodes.OK, _, entity, _) => {
          val futString = entity.toStrict(10.seconds).map(_.data.utf8String)

          val pagesInfos = for {
            string <- futString
            obj = {
              Try {
                Json.parse(string).as[A]
              }
              match {
                case Success(value) => value
                case Failure(exception) => {
                  //logger.error(s"we have an error trying to parse ${string}", exception)
                  throw exception
                }
              }
            }
            jstring = mnhnEndpointResultConverter.createJsonStringSeq(obj)
            b = jstring.map { s =>
              val newI = i.addAndGet(1)
              cache.put(currentPage.toString + "_" + newI.toString, s.getBytes("UTF-8"))
            }
            pages = mnhnEnpointPageExtracter.extractPage(obj)
          } yield pages


          pagesInfos.flatMap { page =>
            //hot fixe
            if ((page.totalPages - 1) == currentPage) {
              Future.successful(total)
            } else {
              fetchSpecimen(currentPage + 1, page.size + total)
            }
          }
        }

        case HttpResponse(status, _, entity, _) => {
          val jstring = entity.toStrict(10.seconds).map(_.data.utf8String)
          logger.info(s"Request failed with status code $status and message: $jstring")
          Future.successful(total)
        }
      }
    }

    fetchSpecimen(1, 0)
  }
}