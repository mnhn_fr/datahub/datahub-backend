package fr.mnhn.datahub.connectors.utils

import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.core.utils.StringUtils
import com.mnemotix.synaptix.core.{MonitoredService, NotificationValues}
import com.softwaremill.sttp._
import fr.mnhn.datahub.connectors.rest.hal.HalResultsIterator

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import scala.xml.Elem

/**
 * The `CachedHalRestClient` class provides functionality for interacting with a REST API using the HAL (Hypertext Application Language) format,
 * while also caching the results using a RocksDB store. It supports paginated querying, data retrieval, and caching of responses.
 *
 * This class extends `MonitoredService` and provides methods for initializing, shutting down, and interacting with the cache, as well as sending HTTP requests
 * to a specified root URI. It also provides methods for building query URIs and processing responses.
 *
 * Key functionalities:
 * - **Caching**: It uses a RocksDB store to cache responses and data.
 * - **Query Building**: Constructs query URIs for sending to the API.
 * - **Response Handling**: Retrieves and processes XML responses from the API.
 * - **Pagination**: Supports paginated responses for handling large datasets.
 *
 * @param serviceName The name of the service.
 * @param rootUri The base URI for the API.
 * @param cachePath The path to the cache file (default is "./data/cache/rest.db").
 */
class CachedHalRestClient(override val serviceName: String, rootUri: String, cachePath: String = "./data/cache/rest.db") extends MonitoredService {

  implicit val backend = HttpURLConnectionBackend()

  private lazy val accessPointUri: String = StringUtils.removeTrailingSlashes(rootUri)
  lazy val cache: RocksDBStore = new RocksDBStore(cachePath)

  def init() = cache.init()

  def shutdown() = cache.shutdown()

  /**
   * Sends a GET request to the specified URI and retrieves the response.
   *
   * @param uri The URI to send the request to.
   * @return The response from the request, containing headers and body.
   */
  def get(uri: String) = {
    val req = sttp.headers(Map("Accept" -> "text/xml")).get(uri"$uri")
    val response = req.send()
    val headers: Seq[(String, String)] = response.headers
    val body: Either[String, String] = response.body
    response
  }

  /**
   * Builds a query URI for sending requests to the API.
   *
   * @param q The query to be sent (default is "*%3A*").
   * @param wt The response format (default is "xml-tei").
   * @param fq The filter query.
   * @param rows The number of rows to retrieve (default is 30).
   * @param start The starting index (default is 0).
   * @return The constructed query URI.
   */
  def buildQueryUri(q: String = "*%3A*", wt: String = "xml-tei", fq: String, rows: Int = 30, start: Int = 0): String = {
    val sb = new StringBuilder()
    sb.append(accessPointUri)
    sb.append(s"/?q=$q")
    sb.append(s"&wt=$wt")
    sb.append(s"&fq=$fq")
    sb.append(s"&rows=$rows")
    sb.append(s"&start=$start")
    sb.toString()
  }

  /**
   * Parses the XML root element to extract the count of search results.
   *
   * @param root The XML root element.
   * @return The count of search results, or -1 if not found.
   */

  def getCount(root: Elem): Int = {
    val count: Option[Int] = (root \ "teiHeader" \ "profileDesc" \ "creation" \\ "measure")
      .find(n => (n \ "@commodity").text == "totalSearchResults")
      .map(n => (n \ "@quantity").text).map(_.toInt)
    count.getOrElse(-1)
  }

  /**
   * Caches all records from the API by iterating over pages of results.
   *
   * @param q The query to be sent (default is "*%3A*").
   * @param wt The response format (default is "xml-tei").
   * @param fq The filter query.
   * @param rows The number of rows to retrieve per page (default is 30).
   * @param start The starting index for pagination (default is 0).
   * @param ec The execution context for handling futures.
   * @return A future representing the completion of the caching operation.
   */
  def cacheAllRecords(q: String = "*%3A*", wt: String = "xml-tei", fq: String, rows: Int = 30, start: Int = 0)(implicit ec: ExecutionContext) = {
    val iterator = new HalResultsIterator(q, wt, fq, rows)(this)
    var processedItems = 0
    val futures = collection.mutable.ArrayBuffer[Future[Boolean]]()
    if (!cache.isOpen) cache.init()
    notify(NotificationValues.STARTUP)

    while (iterator.hasNext) {
      val page = iterator.next()
      processedItems = processedItems + page.entries.size
      futures.addAll(
        Seq(
          cache.bulkAppend(page.entries.map { e =>
            notifyProgress(processedItems, None, None)
            val halUri = (e \ "publicationStmt" \ "idno").find(n => (n \ "@type").text == "halUri").map(_.text).get.trim
            (halUri, e.toString.getBytes("UTF-8"))
          }),
          cache.bulkAppend(page.orgs.map { o =>
            val orgId = (o \ "@{http://www.w3.org/XML/1998/namespace}id").text.trim
            (orgId, o.toString.getBytes("UTF-8"))
          })
        )
      )
    }
    val f = Future.sequence(futures)
    f.onComplete {
      case Success(_) => {
        processedItems = 0
        notify(NotificationValues.DONE)
      }
      case Failure(exception) => {
        processedItems = 0
        notifyError(exception.getMessage, Some(exception))
      }
    }
    f
  }

  /*
    Pagination
  */

  /**
   * Processes a response from the API and converts it into an XML element.
   *
   * @param response The response to be processed.
   * @return An optional XML element representing the response body, or None if the response is an error.
   */

  def processResponse(response: Id[Response[String]]): Option[Elem] = {
    val result: Either[String, String] = response.body
    result match {
      case Right(content) =>
        val elem = scala.xml.XML.loadString(content)
        //        val p = new scala.xml.PrettyPrinter(80, 4)
        //        println(p.format(elem))
        Some(elem)
      case Left(error) =>
        logger.error(s"${response.statusText}(${response.code}): $error")
        None
    }
  }
}
