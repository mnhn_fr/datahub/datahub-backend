/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors.utils

import java.text.SimpleDateFormat
import java.time.{OffsetDateTime, ZoneOffset, ZonedDateTime}
import java.time.format.DateTimeFormatter
import java.util.Date

object DateUtils {


  def romanToArabCentury(s: String) = {
    try {
      val numerals = Map('I' -> 1, 'V' -> 5, 'X' -> 10, 'L' -> 50, 'C' -> 100, 'D' -> 500, 'M' -> 1000)

      s.toUpperCase.map(numerals).foldLeft((0, 0)) {
        case ((sum, last), curr) => (sum + curr + (if (last < curr) -2 * last else 0), curr)
      }._1
    }
    catch {
      case e: Exception => throw new Exception(s""" "${s}" is not a valid Roman Numeral.""")
    }
  }

  def centuryToYear(century: Int) = {
    if (century <= 0) {
      //case e:Exception => throw new Exception(s""" "${century}" is not a valid Numeral.""")
      (0, 0)
    }
    else if (century == 1) {
      (0, 99)
    }
    else if (century > 1 && century < 10) {
      (100, 999)
    }
    else {
      ((century - 1) * 100, (century * 100) - 1)
    }
  }

  def toDate(s: String) = {
    val dateformatMnhm = new SimpleDateFormat("YYYY-MM-dd")
    val yearDate = new SimpleDateFormat("YYYY")
    val yearMonth = new SimpleDateFormat("YYYY-MM")
    if (s.matches("\\d\\d\\d\\d")) {
      val format = new SimpleDateFormat("YYYY")
      val date = format.parse(s)
      Some(yearDate.format(date))
    }
    else if (s.matches("\\d\\d\\d\\d\\-\\d\\d")) {
      // 2005-03
      val format = new SimpleDateFormat("YYYY-MM")
      val date = format.parse(s)
      Some(yearMonth.format(date))
    }
    else if (s.matches("\\d\\d\\d\\d\\-\\d\\d-\\d\\d\\s\\d\\d\\:\\d\\d:\\d\\d")) {
      val format = new SimpleDateFormat("yyyy-MM-dd")
      val dateString = convertMonth(s)
      val date = format.parse(s)
      Some(dateformatMnhm.format(date))
      // 1997-06-10 00:00:00
    }
    else if (s.matches("\\d\\s\\w{1,}\\s\\d\\d\\d\\d")) {
      // "3 juin 1868"
      val format = new SimpleDateFormat("dd MM yyyy")
      val dateString = convertMonth(s)
      val date = format.parse(dateString)
      Some(dateformatMnhm.format(date))
    }

    else {
      None
    }
  }


  def convertMonth(date: String) = {
    date.toUpperCase()
      .replaceAll("JANVIER", "01")
      .replaceAll("FEVRIER", "02")
      .replaceAll("MARS", "03")
      .replaceAll("AVRIL", "04")
      .replaceAll("MAI", "05")
      .replaceAll("JUIN", "06")
      .replaceAll("JUILLET", "07")
      .replaceAll("AOUT", "08")
      .replaceAll("SEPTEMBRE", "09")
      .replaceAll("SPTMBR", "09")
      .replaceAll("OCTOBRE", "10")
      .replaceAll("OCTOBR", "10")
      .replaceAll("NOVEMBRE", "11")
      .replaceAll("NOVMBR", "11")
      .replaceAll("DÉCEMBRE", "12")
      .replaceAll("DCMBR", "12")
  }

  def stringToDate(dateString: String) = {
    val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
    val offsetDateTime = OffsetDateTime.parse(dateString, formatter)
    // Convert OffsetDateTime to Date
    val date = Date.from(offsetDateTime.toInstant)
    date
  }

  def currentDate() = {
    // Get the current date and time in UTC
    val now = ZonedDateTime.now(ZoneOffset.UTC)

    // Define the desired date-time format
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX")

    // Format the date-time using the formatter
    val formattedDateTime = now.format(formatter)

    // Print the formatted date-time
    formattedDateTime
  }

}