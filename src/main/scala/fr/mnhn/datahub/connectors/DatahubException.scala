/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

import com.mnemotix.synaptix.core.SynaptixException

/**
 * Exception thrown during the RDF mapping process.
 * This class extends `SynaptixException` and provides specific functionality for RDF mapping-related errors.
 *
 * @param message The error message describing the RDF mapping-related exception.
 * @param cause An optional underlying cause of the exception, represented as a `Throwable`. Default is `None`.
 */
case class RDFMapperException(message: String, cause: Option[Throwable]=None) extends SynaptixException(message, cause)
