/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.connectors

import com.mnemotix.sql.Persistable
import com.mnemotix.sql.model.Subgraph
import com.mnemotix.sql.utils.FileContentHelper

import java.time.LocalDate

/**
 * A trait for converting an object of type A to a subgraph.
 *
 * This trait defines a method `toSubgraph` that, when implemented, allows the conversion
 * of an object of type A to a corresponding subgraph representation.
 *
 * @tparam A The type of the object to be converted to a subgraph.
 */
trait ToSubgraph[A] {
  /**
   * Converts an object of type A to a subgraph.
   *
   * This method is responsible for transforming an object of type A into a subgraph.
   * The specific implementation of this method is provided by concrete implementations
   * of the `ToSubgraph` trait for a given type A.
   *
   * @param obj The object to be converted to a subgraph.
   * @return The subgraph representation of the given object.
   */
  def toSubgraph(obj: A): Subgraph
}

object ToSubgraph {
  implicit class ToSubGraphModelOps[A](obj: A)(implicit toSubgraphInstance: ToSubgraph[A]) {
    def  toSubgraph: Subgraph = {
      toSubgraphInstance.toSubgraph(obj)
    }
  }
}
