package fr.mnhn.datahub.connectors.oai

import com.typesafe.config.{Config, ConfigFactory}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 29/09/2020
 */

object OaiHarvestingConfiguration {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("oai")).getOrElse(ConfigFactory.empty())
  lazy val halCacheDir: String = conf.getString("hal.cache.directory")
  lazy val halOutputDir: String = conf.getString("hal.output.directory")
  lazy val halMergeSize: Int = conf.getInt("hal.merge.size")
}
