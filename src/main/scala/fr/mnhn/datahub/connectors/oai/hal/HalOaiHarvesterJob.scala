package fr.mnhn.datahub.connectors.oai.hal

import akka.Done
import com.mnemotix.oai.OaiPmhClient
import com.mnemotix.synaptix.jobs.api.SynaptixJob
import fr.mnhn.datahub.connectors.HarvestingServiceListener
import fr.mnhn.datahub.connectors.oai.OaiHarvestingConfiguration

import scala.concurrent.Future

/**
 * HalOaiHarvesterJob is a specific implementation of the `SynaptixJob` that handles the harvesting of metadata from
 * the HAL OAI (Open Archives Initiative) data source. It uses the `OaiPmhClient` to connect to the HAL OAI PMH service,
 * fetches records, and processes them accordingly.
 *
 * The job interacts with the HAL OAI service located at `https://api.archives-ouvertes.fr/oai/hal/` and
 * stores the fetched records in a local cache directory specified in the configuration.
 *
 * This job performs the harvesting in the following steps:
 * 1. Initializes the `OaiPmhClient` before processing.
 * 2. Shuts down the client cache after processing.
 * 3. Fetches metadata records from the HAL OAI service using a specific metadataPrefix and set.
 *
 * @param client The client responsible for interacting with the HAL OAI service.
 * @param prefix A string identifier for the job instance, used for logging or other purposes.
 * @param label A label representing the job's purpose, used for identification in logs.
 * @param beforeProcess A method that is invoked before the main processing starts, used to initialize the client.
 * @param afterProcess A method that is invoked after processing to shut down the cache.
 * @param process A method that performs the core data harvesting logic.
 */
class HalOaiHarvesterJob extends SynaptixJob {
  /**
   * The prefix used for this job instance, used in logging and identifying the job.
   * Default value is "hal".
   */
  override val prefix: String = "hal"
  override val label: String = "HAL OAI Harvester Job"
  /**
   * The client responsible for interacting with the HAL OAI service. It initializes the connection,
   * fetches metadata records, and manages the local cache.
   */
  lazy val client = new OaiPmhClient("HAL OAI PMH Client", "https://api.archives-ouvertes.fr/oai/hal/", OaiHarvestingConfiguration.halCacheDir)
  /**
   * Registers a listener for the client to receive events related to the harvesting process.
   */
  client.registerListener(prefix , new HarvestingServiceListener)

  /**
   * Initializes the OAI PMH client before the harvesting process begins.
   * This method is invoked during the `beforeProcess` stage of the job.
   */
  override def beforeProcess(): Unit = client.init()

  /**
   * Shuts down the cache used by the OAI PMH client after the harvesting process finishes.
   * This method is invoked during the `afterProcess` stage of the job.
   */
  override def afterProcess(): Unit = client.cache.shutdown()

  /**
   * The core processing method that fetches metadata records from the HAL OAI service.
   * It uses the `metadataPrefix` parameter to define the format of the metadata records,
   * and a specific `set` ("collection:MNHN") to filter the records being harvested.
   *
   * @return A Future that resolves to `Done` when the harvesting process completes.
   */
  override def process(): Future[Done] = {
    client.cacheAllRecords(metadataPrefix = "oai_dc", set = Some("collection:MNHN")).map { _ => Done.done() }
  }
}