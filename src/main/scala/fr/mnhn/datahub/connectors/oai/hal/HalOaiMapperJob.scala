package fr.mnhn.datahub.connectors.oai.hal

import com.mnemotix.synaptix.cache.{CacheEntry, RocksDBStore}
import com.mnemotix.synaptix.core.GenericService
import com.mnemotix.synaptix.core.utils.CryptoUtils.md5sum
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.connectors.RDFSerializer
import fr.mnhn.datahub.connectors.RDFSerializer._
import fr.mnhn.datahub.connectors.oai.OaiHarvestingConfiguration
import fr.mnhn.datahub.connectors.utils.DateUtils
import org.eclipse.rdf4j.model.util.ModelBuilder
import org.eclipse.rdf4j.model.vocabulary.{DCTERMS, FOAF, RDF}
import org.eclipse.rdf4j.rio.helpers.RDFParserHelper.createLiteral

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import scala.xml._


/**
 * HalOaiMapperJob is a job that processes cached records from the HAL OAI (Open Archives Initiative) service,
 * maps the metadata to RDF (Resource Description Framework) format, and stores the result in TriG files.
 * It interacts with a cache (using `RocksDBStore`), processes records by grouping them into files, and uses a `ModelBuilder`
 * to generate RDF statements based on the metadata.
 *
 * The job executes the following steps:
 * 1. Initializes the cache before processing starts.
 * 2. Iterates over the cached records, maps them to RDF, and writes the results to TriG files.
 * 3. After processing, shuts down the cache.
 *
 * The RDF model is generated using metadata extracted from XML records stored in the cache, and each RDF triple is
 * built using a `ModelBuilder`. The job handles multiple metadata fields, including identifiers, descriptions,
 * subjects, creators, and dates. Additionally, it creates URIs for documents and authors using a specified base URL
 * and a hashing algorithm for person identifiers.
 *
 * @param cache The `RocksDBStore` used to store and retrieve cached records for processing.
 * @param builder The `ModelBuilder` that constructs the RDF triples.
 * @param init Initializes the cache before processing starts.
 * @param shutdown Shuts down the cache after processing completes.
 * @param process The core method that processes cached entries and produces RDF files.
 * @param map A helper method that maps cache entries to RDF statements.
 */
class HalOaiMapperJob extends GenericService {

  lazy val cache: RocksDBStore = new RocksDBStore(OaiHarvestingConfiguration.halCacheDir)

  var builder: ModelBuilder = _

  override def init(): Unit = cache.init()

  override def shutdown(): Unit = cache.shutdown()

  /**
   * The core processing method that iterates through the cached records,
   * processes them by mapping them to RDF format, and writes the results
   * to TriG files. The method handles grouping records into manageable
   * chunks and generates a new file for each chunk of processed records.
   *
   * The method performs the following steps:
   * 1. Retrieves records from the cache iterator.
   * 2. For each batch of records, maps them to RDF statements.
   * 3. Writes the RDF statements to a TriG file.
   *
   * The files are written to a directory defined in the configuration with
   * filenames based on the number of records processed.
   *
   * @throws IOException If an error occurs during file creation or writing.
   */
  def process(): Unit = {
    val start = System.currentTimeMillis()
    val it = cache.iterator()
    Files.createDirectories(Paths.get(s"${OaiHarvestingConfiguration.halOutputDir}"))
    var count = 0
    while (it.hasNext) {
      // take de N first elements from the iterator to control final files merge size.
      val pageIterator = it.take(OaiHarvestingConfiguration.halMergeSize);
      // reinitialize the builder
      builder = getModel().namedGraph(createIRI("https://datahub.mnhn.fr/data/hal#NG"))
      count += OaiHarvestingConfiguration.halMergeSize
      while(pageIterator.hasNext){
        val entry: CacheEntry = pageIterator.next()
        // map the cache entry to RDF and add statements to the shared model builder
        map(entry.key, new String(entry.value, "UTF-8").split(cache.RECORD_SEPERATOR).map(scala.xml.XML.loadString))
      }
      val filepath = Paths.get(s"${OaiHarvestingConfiguration.halOutputDir}/$count.trig")
      logger.debug(s"File created ${filepath.toAbsolutePath.toString}")
      Files.write(filepath, RDFSerializer.toTriG(builder.build()).getBytes(StandardCharsets.UTF_8))
    }
    it.close()
    val end = System.currentTimeMillis()
    println(s"Mapping total duration : ${end - start} millis}")
  }

  def uri(key: String): String = "https://www.datapoc.mnhn.fr/data/document/hal-museum/" + StringUtils.slugify(key.trim)

  def personUri(key: String): String = "https://www.datapoc.mnhn.fr/data/person/" + md5sum(StringUtils.slugify(key.trim),"hal-museum")

  /**
   * Maps a cache entry to RDF format and adds the corresponding RDF statements to the model builder.
   *
   * This method processes the metadata of a cache entry, extracting fields such as identifier, description,
   * subject, creator, and date, and adds them as RDF triples to the model.
   *
   * @param key The key representing the cache entry identifier.
   * @param data The metadata in XML format associated with the cache entry.
   * @return The updated `ModelBuilder` with the new RDF triples.
   */
  def map(key: String, data: Seq[NodeSeq]) = {

    data.foreach { node =>
      val ids = (node \ "dc" \\ "identifier").map(_.text).filter(_.matches("[a-z]+-\\d+"))
      val descriptions:Seq[Node] = node \ "dc" \\ "description"
      val subjects:Seq[String] = (node \ "dc" \\ "subject").map(_.text)
      val creators:Seq[String] = (node \ "dc" \\ "creator").map(_.text)
      val types:Seq[String] = (node \ "dc" \\ "type").map(_.text)
//      val date:Option[Date] = (node \ "dc" \\ "date").map(_.text).headOption.map(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(_).toDate)
      val date:Option[String] = (node \ "dc" \\ "date").map(_.text).headOption

      val id = if (ids.nonEmpty) ids.head else key
      val description = {
        if(descriptions.nonEmpty) {
          if(descriptions.exists(n => (n \ "@lang").text == "fr")) descriptions.filter(n => (n \ "@lang").text == "fr").map(_.text.trim).headOption
          else descriptions.map(_.text.trim).headOption
        }
        else None
      }
      val language = (node \ "dc" \ "language").text

      builder
        .subject(uri(id))
        .add(RDF.TYPE, "mnhn:Document")
        .add("prov:hadPrimarySource", createIRI(s"https://api.archives-ouvertes.fr/oai/hal/?verb=GetRecord&metadataPrefix=oai_dc&identifier=$key"))
        .add("mnx:sourceDataset", createIRI(s"https://www.datapoc.mnhn.fr/data/datasets/hal-museum"))
        .add("dcterms:title", (node \ "dc" \ "title").text)
        .add("dcterms:language", language)

      builder = description.map{d =>
        builder.add("dcterms:description", d)
      }.getOrElse(builder)

      builder = date.map{d =>
        DateUtils.toDate(d).map(dt => builder.add("dcterms:date", (dt, createIRI("http://www.w3.org/2001/XMLSchema#date")))).getOrElse(builder)
      }.getOrElse(builder)

      subjects.foreach{s =>
        builder.add("dcterms:subject", s)
      }

      types.foreach{s =>
        builder.add("dcterms:type", s)
      }

      creators.foreach{c =>
        val persUri = personUri(c)
        builder.subject(uri(id))
        builder.add("mnhn:author", createIRI(persUri))
        builder.subject(persUri)
          .add(RDF.TYPE, "mnhn:Person")
          .add(FOAF.NAME, c)
          .add(DCTERMS.IDENTIFIER, StringUtils.slugify(c.trim))
          .add("mnx:sourceDataset", createIRI(s"https://www.datapoc.mnhn.fr/data/datasets/hal-museum"))
      }
    }
    builder
  }
}
