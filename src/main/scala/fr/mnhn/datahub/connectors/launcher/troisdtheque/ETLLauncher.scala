package fr.mnhn.datahub.connectors.launcher.troisdtheque

import akka.actor.ActorSystem
import akka.stream.alpakka.slick.scaladsl.SlickSession
import akka.stream.scaladsl.Sink
import com.mnemotix.synaptix.cache.RocksDBStore
import com.typesafe.scalalogging.LazyLogging
import fr.mnhn.datahub.connectors.GenericRestClient
import fr.mnhn.datahub.connectors.es.{RdfDumper, RdfMapper}
import fr.mnhn.datahub.connectors.rest.model.{Digitalizations, DigitalizationsRootInterface}
import fr.mnhn.datahub.connectors.rest.troisdtheque.DigitalizationsRdfMapper.digitalizationsToRdfModel
import fr.mnhn.datahub.connectors.rest.troisdtheque.TroisDThequeConfig
import slick.basic.DatabaseConfig
import slick.jdbc.JdbcProfile

import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

object ETLLauncher extends App with LazyLogging {

  implicit val system: ActorSystem = ActorSystem("ETLAUNCHERDATAHUB")
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  // Initialize resources
  val store = new RocksDBStore(TroisDThequeConfig.cacheDir)
  store.init()

  val databaseConfig = DatabaseConfig.forConfig[JdbcProfile]("slick-postgres")
  implicit val session: SlickSession = SlickSession.forConfig(databaseConfig)

  val rdfDumper = new RdfDumper()
  val cacheDir = TroisDThequeConfig.cacheDir
  val rdfDir = TroisDThequeConfig.rdfDir
  val mapper = new RdfMapper(cacheDir)
  mapper.init()

  def runHarvester(): Future[Seq[Digitalizations]] = {
    val restClient = new GenericRestClient(store)
    logger.info("Starting harvester for Digitalizations")
    restClient.getAsMany[DigitalizationsRootInterface](1000, "http://datahub-test-2.arzt.mnhn.fr:8096", "digitalizations")
      .flatMap { _ =>
        mapper.cacheExtractor[Digitalizations].runWith(Sink.seq)
      }
      .map { content =>
        logger.info(s"Harvesting complete. Total records: ${content.size}")
        content
      }
      .recover {
        case ex: Exception =>
          logger.error(s"Error during harvesting: ${ex.getMessage}", ex)
          throw ex
      }
  }

  def runDumper(): Future[Unit] = {
    logger.info("Starting RDF dumping")
    mapper.process[Digitalizations](100000).runWith(rdfDumper.toFileDumper(rdfDir))
  .map { _ =>
      logger.info("RDF dumping completed successfully")
    }
      .recover {
        case ex: Exception =>
          logger.error(s"Error during RDF dumping: ${ex.getMessage}", ex)
          throw ex
      }
  }

  runHarvester().onComplete {
    case Success(_) =>
      logger.info("All harvesters completed. Starting dumpers.")
      runDumper().onComplete {
        case Success(_) =>
          logger.info("ETL process completed successfully.")
          system.terminate()
        case Failure(exception) =>
          logger.error(s"ETL process failed during dumping: ${exception.getMessage}", exception)
          system.terminate()
      }
    case Failure(exception) =>
      logger.error(s"ETL process failed during harvesting: ${exception.getMessage}", exception)
      system.terminate()
  }
}