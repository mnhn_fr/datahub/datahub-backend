package fr.mnhn.datahub.connectors.launcher.moleculaire

import akka.actor.ActorSystem
import com.mnemotix.synaptix.cache.RocksDBStore
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.typesafe.scalalogging.LazyLogging
import fr.mnhn.datahub.connectors.{Default, GenericRestClient, ToRdfModel}
import fr.mnhn.datahub.connectors.MnhnEndpointResultConverter.{BoldRootInterfaceConverter, GenBankConverter}
import fr.mnhn.datahub.connectors.es.model.{Collection, Geolocation, Material}
import fr.mnhn.datahub.connectors.es.utils.BuConfig
import fr.mnhn.datahub.connectors.es.{RdfDumper, RdfMapper}
import fr.mnhn.datahub.connectors.launcher.datahub.ETLLauncher.{dumpRdfFile, logger, rdfDumpTasks}
import fr.mnhn.datahub.connectors.rest.model.{BoldRecords, BoldRootInterface, GenBankRootInterface, SequencesGenBank, SpecimenRootInterface, Specimens}
import fr.mnhn.datahub.connectors.rest.moleculaire.BoldRdfMapper.boldsToRdfModel
import fr.mnhn.datahub.connectors.rest.moleculaire.GenBankRdfMapper.genbanksToRdfModel
import fr.mnhn.datahub.connectors.rest.moleculaire.MoleculaireConfig
import fr.mnhn.datahub.connectors.rest.moleculaire.MoleculaireRdfMapper.moleculairesToRdfModel
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
import play.api.libs.json.{Json, Reads}

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object ETLLauncher extends App with LazyLogging {

  implicit val system: ActorSystem = ActorSystem("MolecularDataHarvester")
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val molecularCachePath = MoleculaireConfig.cacheDirMoleculaire
  val boldCachePath = MoleculaireConfig.cacheDirbold
  val genBankCachePath = MoleculaireConfig.cacheDirGenbank
  val molecularApiUrl = "http://datahub-test-2.arzt.mnhn.fr:8092/specimens"

  // Helper function to process specimens from a store
  def processSpecimens(storePath: String, processSpecimen: Specimens => Unit): Unit = {
    val store = new RocksDBStore(storePath)
    store.init()
    try {
      store.iterator().iterator.foreach { kv =>
        val value = new String(kv.value, "UTF-8")
        val specimen = Json.parse(value).as[Specimens]
        processSpecimen(specimen)
      }
    } finally {
      store.shutdown()
    }
  }

  // Fetch all specimens from the REST API and store them
  def fetchAndStoreSpecimens(): Unit = {
    val store = new RocksDBStore(molecularCachePath)
    store.init()
    try {
      val restClient = new GenericRestClient(store)
      val fut = restClient.getAsMany[SpecimenRootInterface](1000, molecularApiUrl, "specimens")
      val resultCount = Await.result(fut, Duration.Inf)
      logger.info(s"Fetched $resultCount specimens from REST API.")
    } finally {
      store.shutdown()
    }
  }

  // Process specimens and fetch BOLD data
  def fetchBoldData(): Unit = {
    val boldStore = new RocksDBStore(boldCachePath)
    val restClient = new GenericRestClient(boldStore)
    boldStore.init()
    try {
      processSpecimens(molecularCachePath, { specimen =>
        specimen._links.flatMap(_.bold.flatMap(_.href.map(_.toString))).foreach { documentUrl =>
          val future = restClient.getDocument[BoldRootInterface](documentUrl, specimen.id.get)
          Await.result(future, Duration.Inf)
          logger.info(s"Fetched BOLD data for specimen ${specimen.id.get}.")
        }
      })
    } finally {
      boldStore.shutdown()
    }
  }

  // Process specimens and fetch GenBank data
  def fetchGenBankData(): Unit = {
    val genBankStore = new RocksDBStore(genBankCachePath)
    val restClient = new GenericRestClient(genBankStore)
    val counter = new AtomicInteger()
    genBankStore.init()
    try {
      processSpecimens(molecularCachePath, { specimen =>
        if (counter.incrementAndGet() % 1000 == 0) {
          logger.info(s"Processed ${counter.get()} specimens for GenBank.")
        }
        specimen._links.flatMap(_.sequences.flatMap(_.href.map(_.replace("{&sampleId}", "")))).foreach { documentUrl =>
          val future = restClient.getDocument[GenBankRootInterface](documentUrl, specimen.id.get)
          Await.result(future, Duration.Inf)
          logger.info(s"Fetched GenBank data for specimen ${specimen.id.get}.")
        }
      })
    } finally {
      genBankStore.shutdown()
    }
  }

  def dumpRdfFile[T](cacheDirKey: String, rdfDirKey: String, batchSize: Int)(implicit toRdfModel: ToRdfModel[Seq[T]], read: Reads[T], default: Default[T]): Future[Unit] = {
    logger.info(s"Starting RDF dumping for cacheDir: $cacheDirKey")
    val cacheDir = ESConfiguration.prefix.getOrElse("") + cacheDirKey
    val rdfDir = ESConfiguration.prefix.getOrElse("") + rdfDirKey
    val rdfDumper = new RdfDumper()
    val mapper = new RdfMapper(cacheDir)
    mapper.init()

    mapper.process[T](batchSize)
      .runWith(rdfDumper.toFileDumper(rdfDir))
      .map(_ => logger.info(s"Dumping RDF for $cacheDirKey to $rdfDir completed successfully"))
      .recover {
        case e: Exception =>
          logger.error(s"Error during RDF dumping for $cacheDirKey: ${e.getMessage}")
          throw e
      }
  }

  val rdfDumpTasks = Seq(
    dumpRdfFile[Specimens](MoleculaireConfig.cacheDirMoleculaire, MoleculaireConfig.rdfDirMoleculaire, 500000),
    dumpRdfFile[BoldRecords](MoleculaireConfig.cacheDirbold, MoleculaireConfig.rdfDirbold, 500000),
    dumpRdfFile[SequencesGenBank](MoleculaireConfig.cacheDirGenbank, MoleculaireConfig.rdfDirGenbank, 100000),
  )




  try {
    logger.info("Starting Molecular Data Harvester...")
    fetchAndStoreSpecimens()
    fetchBoldData()
    fetchGenBankData()
    Future.sequence(rdfDumpTasks).onComplete {
      case Success(_) => logger.info("All RDF dumps completed successfully.")
      case Failure(exception) => logger.error(s"One or more RDF dump tasks failed: ${exception.getMessage}")
    }
    logger.info("Molecular Data Harvester completed successfully.")
  } catch {
    case ex: Exception =>
      logger.error("An error occurred during the harvesting process.", ex)
  } finally {
    system.terminate()
  }


}