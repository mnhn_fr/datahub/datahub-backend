package fr.mnhn.datahub.connectors.launcher.datahub

import akka.actor.ActorSystem
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import com.typesafe.scalalogging.LazyLogging
import fr.mnhn.datahub.connectors.es.helper.CollectionEventRdfMapper.collectionEventsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.CollectionGroupRdfMapper.collectionGroupsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.CollectionRdfMapper.collectionsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.CoordinatesRdfMapper.coordinatesToRdfModel
import fr.mnhn.datahub.connectors.es.helper.DomainRdfMapper.domainsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.GeolocationRdfMapper.geolocationsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.IdentificationRdfMapper.identificationsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.InstitutionRdfMapper.institutionsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.MaterialRdfMapper.materialsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.MaterialRelationshipRdfMapper.materialRelationShipsToRdfModel
import fr.mnhn.datahub.connectors.es.helper.TaxonRdfMapper.taxonsToRdfModel
import fr.mnhn.datahub.connectors.{Default, ToRdfModel}
import fr.mnhn.datahub.connectors.es.model.{Collection, CollectionEvent, CollectionGroup, Coordinates, Domain, Geolocation, Identification, Institution, Material, MaterialRelationship, Taxon}
import fr.mnhn.datahub.connectors.es.utils.BuConfig
import fr.mnhn.datahub.connectors.es.{GenericHarvester, RdfDumper, RdfMapper}
import play.api.libs.json.Reads

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object ETLLauncher extends App with LazyLogging {

  implicit val system: ActorSystem = ActorSystem("ETLAUNCHERDATAHUB")
  implicit val executionContext: ExecutionContext = system.dispatcher

  // Method to run a harvester
  def runHarvester(cacheDir: String, indexName: String, sortField: String): Future[Unit] = {
    logger.info(s"Starting harvesting for index: $indexName")
    val harvester = new GenericHarvester(indexName, cacheDir, sortField)
    harvester.init()
    harvester.process().map { _ =>
      val cacheSize = harvester.cache.iterator().size
      if (cacheSize > 0) {
        logger.info(s"Harvesting for $indexName completed successfully. Cache size: $cacheSize")
      } else {
        logger.warn(s"Warning: Cache size is 0 for $indexName")
      }
    }.recover {
      case e: Exception =>
        logger.error(s"Error during harvesting for $indexName: ${e.getMessage}")
        throw e
    }.andThen { case _ =>
      harvester.shutdown()
    }
  }

  // Method to dump RDF files
  def dumpRdfFile[T](cacheDirKey: String, rdfDirKey: String, batchSize: Int)(implicit toRdfModel: ToRdfModel[Seq[T]], read: Reads[T], default: Default[T]): Future[Unit] = {
    logger.info(s"Starting RDF dumping for cacheDir: $cacheDirKey")
    val cacheDir = ESConfiguration.prefix.getOrElse("") + cacheDirKey
    val rdfDir = ESConfiguration.prefix.getOrElse("") + rdfDirKey
    val rdfDumper = new RdfDumper()
    val mapper = new RdfMapper(cacheDir)
    mapper.init()

    mapper.process[T](batchSize)
      .runWith(rdfDumper.toFileDumper(rdfDir))
      .map(_ => logger.info(s"Dumping RDF for $cacheDirKey to $rdfDir completed successfully"))
      .recover {
        case e: Exception =>
          logger.error(s"Error during RDF dumping for $cacheDirKey: ${e.getMessage}")
          throw e
      }
  }

  // Define harvesters
  val harvesters = Seq(
    (BuConfig.collectioncacheDir, BuConfig.collectionindexName, "collection_id"),
    (BuConfig.collectionEventcacheDir, BuConfig.collectionEventindexName, "collection_event_id"),
    (BuConfig.collectionGroupcacheDir, BuConfig.collectionGroupindexName, "collection_group_id"),
    (BuConfig.coordinatescacheDir, BuConfig.coordinatesindexName, "coordinates_id"),
    (BuConfig.domaincacheDir, BuConfig.domainindexName, "domain_id"),
    (BuConfig.geolocationcacheDir, BuConfig.geolocationindexName, "geolocation_id"),
    (BuConfig.identificationcacheDir, BuConfig.identificationindexName, "identification_id"),
    (BuConfig.institutioncacheDir, BuConfig.institutionindexName, "institution_id"),
    (BuConfig.materialcacheDir, BuConfig.materialindexName, "material_id"),
    (BuConfig.materialRelationshipcacheDir, BuConfig.materialRelationshipindexName, "subject_id"),
    (BuConfig.taxoncacheDir, BuConfig.taxonindexName, "taxon_id")
  )

  // Define RDF dump tasks
  val rdfDumpTasks = Seq(
    dumpRdfFile[Geolocation](BuConfig.geolocationcacheDir, BuConfig.geolocationrdfDir, 500000),
    dumpRdfFile[Material](BuConfig.materialcacheDir, BuConfig.materialrdfDir, 500000),
    dumpRdfFile[Collection](BuConfig.collectioncacheDir, BuConfig.collectionrdfDir, 100000),
    dumpRdfFile[CollectionEvent](BuConfig.collectionEventcacheDir, BuConfig.collectionEventrdfDir, 500000),
    dumpRdfFile[CollectionGroup](BuConfig.collectionGroupcacheDir, BuConfig.collectionGrouprdfDir, 1000000),
    dumpRdfFile[Coordinates](BuConfig.coordinatescacheDir, BuConfig.coordinatesrdfDir, 1000000),
    dumpRdfFile[Domain](BuConfig.domaincacheDir, BuConfig.domainrdfDir, 1000000),
    dumpRdfFile[Identification](BuConfig.identificationcacheDir, BuConfig.identificationrdfDir, 500000),
    dumpRdfFile[Institution](BuConfig.institutioncacheDir, BuConfig.institutionrdfDir, 100000),
    dumpRdfFile[MaterialRelationship](BuConfig.materialRelationshipcacheDir, BuConfig.materialRelationshiprdfDir, 100000),
    dumpRdfFile[Taxon](BuConfig.taxoncacheDir, BuConfig.taxonrdfDir, 100000)
  )

  // Launch harvesters
  val allHarvesters: Seq[Future[Unit]] = harvesters.map { case (cacheDirKey, indexNameKey, sortField) =>
    val cacheDir = ESConfiguration.prefix.getOrElse("") + cacheDirKey
    val indexName = ESConfiguration.prefix.getOrElse("") + indexNameKey
    runHarvester(cacheDir, indexName, sortField)
  }

  Future.sequence(allHarvesters).onComplete {
    case Success(_) =>
      logger.info("All harvesters completed successfully. Starting RDF dump tasks...")
      Future.sequence(rdfDumpTasks).onComplete {
        case Success(_) => logger.info("All RDF dumps completed successfully.")
        case Failure(exception) => logger.error(s"One or more RDF dump tasks failed: ${exception.getMessage}")
      }
    case Failure(exception) =>
      logger.error(s"One or more harvesters failed: ${exception.getMessage}")
  }
}