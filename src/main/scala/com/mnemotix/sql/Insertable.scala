/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.sql

import slick.dbio.DBIO

trait Insertable[T] {
  def insert(element: T): DBIO[Int]
  def insertMany(elements: Seq[T]): DBIO[Seq[Int]]
  def update(element: T): DBIO[Int]
  def updateMany(element: Seq[T]): DBIO[Seq[Int]]
  def insertOrUpdate(element: T): DBIO[Int]
  def insertOrUpdateMany(element: Seq[T]): DBIO[Seq[Int]]
  def flush
}