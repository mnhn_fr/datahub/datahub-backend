/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.sql

import akka.actor.ActorSystem
import akka.stream.alpakka.slick.javadsl.SlickSession
import com.mnemotix.sql.model.{NamedGraphModel, OwlModel, Subgraph}
import slick.jdbc.PostgresProfile
import slick.dbio
import slick.dbio.Effect
import slick.sql.FixedSqlAction

import scala.collection.immutable.Seq
import scala.concurrent.ExecutionContext

class GraphManagementRepository()(implicit actorSystem: ActorSystem, ec: ExecutionContext, session: SlickSession) extends SlickTablesGeneric(PostgresProfile) {

  import profile.api._

  val namedGraphModelTable = TableQuery[NamedGraphModelTable]
  val owlModelTable = TableQuery[OwlModelTable]
  val subGraphTable = TableQuery[SubGraphTable]

  def createTables(): DBIO[Unit] = {
    (namedGraphModelTable.schema ++ owlModelTable.schema ++ subGraphTable.schema).createIfNotExists
  }

  // Instances of the type class for your types
  implicit val subGraphInsertable: Insertable[Subgraph] = new Insertable[Subgraph] {
    override def insert(element: Subgraph): DBIO[Int] = (subGraphTable += element).transactionally

    override def insertMany(elements: Seq[Subgraph]):  DBIO[Seq[Int]] = {
      dbio.DBIO.sequence(elements.map(insert(_))).transactionally
    }

    override def update(element: Subgraph): dbio.DBIO[Int] = subGraphTable.insertOrUpdate(element)

    override def updateMany(elements: Seq[Subgraph]): dbio.DBIO[Seq[Int]] = dbio.DBIO.sequence(elements.map(update(_))).transactionally

    override def insertOrUpdate(element: Subgraph): dbio.DBIO[Int] = {
      val existingQuery = subGraphTable.filter(row => row.resourceUri === element.resourceUri && row.checksum === element.checksum)
      val updateAction = existingQuery.result.flatMap { existingSubgraphs =>
        existingSubgraphs.headOption match {
          case Some(_) =>
            // Do nothing if record with the same resourceUri and checksum exists
            DBIO.successful(0)
          case None =>
            // Insert if no existing record with the same resourceUri and checksum
            subGraphTable += element
        }
      }
      updateAction.transactionally
    }

    override def insertOrUpdateMany(elements: Seq[Subgraph]): dbio.DBIO[Seq[Int]] = dbio.DBIO.sequence(elements.map(insertOrUpdate(_))).transactionally


    override def flush = subGraphTable.delete.transactionally
  }

  implicit val owlModelInsertable: Insertable[OwlModel] = new Insertable[OwlModel] {
    override def insert(element: OwlModel): DBIO[Int] = owlModelTable += element

    override def insertMany(elements: Seq[OwlModel]): DBIO[Seq[Int]] = {
      dbio.DBIO.sequence(elements.map(insert(_))).transactionally
    }

    override def update(element: OwlModel): dbio.DBIO[Int] = owlModelTable.insertOrUpdate(element)

    override def updateMany(elements: Seq[OwlModel]): dbio.DBIO[Seq[Int]] = {
      dbio.DBIO.sequence(elements.map(update(_))).transactionally
    }

    override def insertOrUpdate(element: OwlModel): dbio.DBIO[Int] = {

      // case class OwlModel(model_id: String, uri: String, file: Array[Byte], checksum: String)
      val existingQuery = owlModelTable.filter(row => row.uri === element.uri && row.checksum === element.checksum)
      val updateAction = existingQuery.result.flatMap { existingSubgraphs =>
        existingSubgraphs.headOption match {
          case Some(_) =>
            // Do nothing if record with the same resourceUri and checksum exists
            DBIO.successful(0)
          case None =>
            // Insert if no existing record with the same resourceUri and checksum
            owlModelTable += element
          }
        }
      updateAction.transactionally
    }

    override def insertOrUpdateMany(elements: Seq[OwlModel]): dbio.DBIO[Seq[Int]] = dbio.DBIO.sequence(elements.map(insertOrUpdate(_))).transactionally

    override def flush: Unit = owlModelTable.delete.transactionally

  }

  implicit val namedGraphModelInsertable: Insertable[NamedGraphModel] = new Insertable[NamedGraphModel] {
    override def insert(element: NamedGraphModel): DBIO[Int] = namedGraphModelTable += element

    override def insertMany(elements: Seq[NamedGraphModel]): DBIO[Seq[Int]] = {
      dbio.DBIO.sequence(elements.map(insert(_))).transactionally
    }

    override def update(element: NamedGraphModel): dbio.DBIO[Int] = {
      namedGraphModelTable.insertOrUpdate(element)
    }

    override def updateMany(elements: Seq[NamedGraphModel]): dbio.DBIO[Seq[Int]] = dbio.DBIO.sequence(elements.map(update(_))).transactionally

    override def insertOrUpdate(element: NamedGraphModel): dbio.DBIO[Int] = {

      val existingQuery = namedGraphModelTable.filter(row => row.namedGraphId === element.named_graph_id && row.modelId === element.model_id)
      val updateAction = existingQuery.result.flatMap { existingNamedGraphModel =>
        existingNamedGraphModel.headOption match {
          case Some(_) =>
            // Do nothing if record with the same resourceUri and checksum exists
            DBIO.successful(0)
          case None =>
            // Insert if no existing record with the same resourceUri and checksum
            namedGraphModelTable += element
          }
        }
      updateAction.transactionally
    }

    override def insertOrUpdateMany(elements: Seq[NamedGraphModel]): dbio.DBIO[Seq[Int]] =  dbio.DBIO.sequence(elements.map(insertOrUpdate(_))).transactionally

    override def flush: Unit = namedGraphModelTable.delete.transactionally

  }

  def deleteNamedGraph(id: String): DBIO[Int] = subGraphTable.filter(_.id.toString() == id).delete
  def deleteOwlModel(id: String): DBIO[Int] = owlModelTable.filter(_.id.toString() == id).delete
  def deleteNamedGraphModel(id: String): DBIO[Int] = namedGraphModelTable.filter(_.namedGraphId.toString() == id).delete

  def modelTableSize = owlModelTable.size.result

  def subGraphTableSize(): dbio.DBIO[Int] = subGraphTable.size.result

  def namedGraphSize() = namedGraphModelTable.size.result

}