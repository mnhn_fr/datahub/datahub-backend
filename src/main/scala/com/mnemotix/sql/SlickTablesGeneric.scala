/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.sql

import com.mnemotix.sql.model.{Subgraph, NamedGraphModel, OwlModel}
import slick.jdbc.PostgresProfile
import slick.lifted.ProvenShape

import java.time.LocalDate

class SlickTablesGeneric(val profile: PostgresProfile) {
  import profile.api._

  class SubGraphTable(tag: Tag) extends Table[Subgraph](tag, "Subgraph") {
    def id: Rep[String] = column[String]("id", O.PrimaryKey)

    def graphUri: Rep[String] = column[String]("graphUri")

    def resourceUri: Rep[String] = column[String]("resourceUri")

    def generationDate: Rep[LocalDate] = column[LocalDate]("generationDate")

    def file: Rep[Array[Byte]] = column[Array[Byte]]("file")

    def checksum: Rep[String] = column[String]("checksum")

    def fileExtension: Rep[String] = column[String]("fileExtension")

    def datatype: Rep[String] = column[String]("datatype")

    override def * : ProvenShape[Subgraph] = (id, graphUri, resourceUri, datatype, generationDate, file, checksum, fileExtension) <> (Subgraph.tupled, Subgraph.unapply)
  }

  class OwlModelTable(tag: Tag) extends Table[OwlModel](tag, "OwlModel") {
    def id: Rep[String] = column[String]("model_id", O.PrimaryKey)
    def uri: Rep[String] = column[String]("uri")
    def file: Rep[Array[Byte]] = column[Array[Byte]]("file")
    def checksum: Rep[String] = column[String]("checksum")
    override def * : ProvenShape[OwlModel] = (id, uri, file, checksum) <> (OwlModel.tupled, OwlModel.unapply)
  }

  class NamedGraphModelTable(tag: Tag) extends Table[NamedGraphModel](tag,"NamedGraphModel") {
    def namedGraphId: Rep[String] = column[String]("named_graph_id")
    def modelId: Rep[String] = column[String]("model_id")

    override def * : ProvenShape[NamedGraphModel] = (namedGraphId, modelId) <> (NamedGraphModel.tupled, NamedGraphModel.unapply)
  }
}