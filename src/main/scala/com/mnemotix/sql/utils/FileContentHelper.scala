/**
 * Copyright (C) 2013-2023 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.sql.utils

import java.security.MessageDigest

object FileContentHelper {

  def sha256(content: String): String = {
    val byteArray = content.getBytes
    val digest = MessageDigest.getInstance("SHA-256").digest(byteArray)
    digest.map("%02x".format(_)).mkString
  }

  def hasContentChanged(fileContent: String, storedHash: String): Boolean = {
    val currentHash = sha256(fileContent)
    currentHash != fileContent
  }
}