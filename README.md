# Documentation: How to Launch the ETL Applications

This document explains how to run the ETL (Extract, Transform, Load) applications for the **DataHub**, **Molecular**, and **TroisDTheque** modules. Each module is implemented as a separate Scala object extending `App`.

---

## Prerequisites

1. **Scala and SBT**:
    - Ensure that `Scala` and `SBT` are installed on your machine.
    - Recommended Scala version: `2.13.8` or `3.x`
    - Recommended SBT version: `1.7.x` or higher.

2. **Dependencies**:
    - All required dependencies are specified in the project's `build.sbt`. Run `sbt update` to download them.

3. **Environment Setup**:
    - Verify the required directories for caching and RDF dumps exist:
        - **DataHub**: RocksDB cache directory and target RDF directory.
        - **Molecular**: RocksDB cache directories for molecular data, BOLD, and GenBank.
        - **TroisDTheque**: RocksDB cache directory and RDF output directory.
    - Ensure the REST API endpoints specified in each ETL module are accessible.

4. **Database** (if applicable):
    - Ensure any required database configurations (e.g., Postgres) are correctly set in `application.conf`.

---

## How to Run the ETL Applications

Each ETL application is defined in its respective package:

1. **DataHub ETL Launcher**:
    - **Class**: `fr.mnhn.datahub.connectors.launcher.datahub.ETLLauncher`
    - **Purpose**: Extracts, transforms, and loads data from the DataHub REST API to RocksDB and RDF formats.
    - **Command**:
      ```bash
      sbt "runMain fr.mnhn.datahub.connectors.launcher.datahub.ETLLauncher"
      ```

2. **Molecular ETL Launcher**:
    - **Class**: `fr.mnhn.datahub.connectors.launcher.moleculaire.ETLLauncher`
    - **Purpose**: Processes molecular data from the REST API, caches it in RocksDB, and extracts additional data such as BOLD and GenBank.
    - **Command**:
      ```bash
      sbt "runMain fr.mnhn.datahub.connectors.launcher.moleculaire.ETLLauncher"
      ```

3. **TroisDTheque ETL Launcher**:
    - **Class**: `fr.mnhn.datahub.connectors.launcher.troisdtheque.ETLLauncher`
    - **Purpose**: Processes 3D object data, caches it in RocksDB, and converts it into RDF format for further analysis.
    - **Command**:
      ```bash
      sbt "runMain fr.mnhn.datahub.connectors.launcher.troisdtheque.ETLLauncher"
      ```

---

## Configuration

Modify the following configurations based on your environment:

1. **Cache Directories**:
    - Ensure that the RocksDB cache directories exist and have write permissions:
        - **DataHub**: `data/datahub/cache.db`
        - **Molecular**: `data/moleculaires/cache2.db`, `data/bold/cache.db`, `data/genbank/cache.db`
        - **TroisDTheque**: `data/troisdtheque/cache.db`

2. **REST API Endpoints**:
    - Verify the REST API endpoints configured in each launcher:
        - DataHub: `http://datahub-test-2.arzt.mnhn.fr:8096/digitalizations`
        - Molecular: `http://datahub-test-2.arzt.mnhn.fr:8092/specimens`
        - TroisDTheque: `http://datahub-test-2.arzt.mnhn.fr:8096`

3. **Application Configuration**:
    - Update `application.conf` (if applicable) to reflect database or custom service configurations.

---

## Logs

Each ETL launcher logs its progress to the console. It includes:
- Fetching progress from the REST API.
- Processing records.
- Caching and dumping data.

Use a process manager like `screen` or `tmux` to monitor long-running tasks.

---

## Troubleshooting

1. **Missing Dependencies**:
    - Run `sbt clean update` to ensure all dependencies are resolved.

2. **Permission Issues**:
    - Verify that the cache directories and RDF dump directories have the correct read/write permissions.

3. **REST API Errors**:
    - Ensure the specified REST API endpoints are reachable.

4. **Logs**:
    - Check the logs for detailed error messages.

---

## Running All ETL Launchers

To run all ETL launchers in sequence, you can use the following script:

```bash
#!/bin/bash

echo "Starting DataHub ETL..."
sbt "runMain fr.mnhn.datahub.connectors.launcher.datahub.ETLLauncher"

echo "Starting Molecular ETL..."
sbt "runMain fr.mnhn.datahub.connectors.launcher.moleculaire.ETLLauncher"

echo "Starting TroisDTheque ETL..."
sbt "runMain fr.mnhn.datahub.connectors.launcher.troisdtheque.ETLLauncher"

echo "All ETL processes completed successfully."

```

# Code Structure

The code consists of **6 packages**:

1. **es**:
   - Contains the code that connects to DataHub indexes, retrieves data from the indexes, and converts it into RDF format.

2. **graphdb**:
   - Contains the code to index GraphDB data into Elasticsearch indexes.

3. **oai**:
   - Contains an OAI PMH client for connecting to HAL data and transforming it into RDF format.

4. **rest**:
   - Contains REST clients for APIs like Molecular, 3DTheque, and HAL, which connect to these APIs and transform the data into RDF format.

5. **utils**:
   - Contains utility functions for name extraction, date conversion, and the OAI PMH client for HAL.


# Documentation of Environment Variables for Configuration

This section provides descriptions of the environment variables used for configuration.

## collectionEvent
- **COLLECTION_EVENT_RDF_DIR**: Path to the directory containing RDF files for collectionEvent.
- **COLLECTION_EVENT_CACHE_DIR**: Path to the cache file for collectionEvent.
- **COLLECTION_EVENT_INDEX_NAME**: Name of the Elasticsearch index for collectionEvent.

## collectionGroup
- **COLLECTION_GROUP_RDF_DIR**: Path to the directory containing RDF files for collectionGroup.
- **COLLECTION_GROUP_CACHE_DIR**: Path to the cache file for collectionGroup.
- **COLLECTION_GROUP_INDEX_NAME**: Name of the Elasticsearch index for collectionGroup.

## coordinates
- **COORDINATES_RDF_DIR**: Path to the directory containing RDF files for coordinates.
- **COORDINATES_CACHE_DIR**: Path to the cache file for coordinates.
- **COORDINATES_INDEX_NAME**: Name of the Elasticsearch index for coordinates.

## collection
- **COLLECTION_RDF_DIR**: Path to the directory containing RDF files for collection.
- **COLLECTION_CACHE_DIR**: Path to the cache file for collection.
- **COLLECTION_INDEX_NAME**: Name of the Elasticsearch index for collection.

## domain
- **DOMAIN_RDF_DIR**: Path to the directory containing RDF files for domain.
- **DOMAIN_CACHE_DIR**: Path to the cache file for domain.
- **DOMAIN_INDEX_NAME**: Name of the Elasticsearch index for domain.

## geolocation
- **GEOLOCATION_RDF_DIR**: Path to the directory containing RDF files for geolocation.
- **GEOLOCATION_CACHE_DIR**: Path to the cache file for geolocation.
- **GEOLOCATION_INDEX_NAME**: Name of the Elasticsearch index for geolocation.

## identification
- **IDENTIFICATION_RDF_DIR**: Path to the directory containing RDF files for identification.
- **IDENTIFICATION_CACHE_DIR**: Path to the cache file for identification.
- **IDENTIFICATION_INDEX_NAME**: Name of the Elasticsearch index for identification.

## institution
- **INSTITUTION_RDF_DIR**: Path to the directory containing RDF files for institution.
- **INSTITUTION_CACHE_DIR**: Path to the cache file for institution.
- **INSTITUTION_INDEX_NAME**: Name of the Elasticsearch index for institution.

## materialRelationship
- **MATERIAL_RELATIONSHIP_RDF_DIR**: Path to the directory containing RDF files for materialRelationship.
- **MATERIAL_RELATIONSHIP_CACHE_DIR**: Path to the cache file for materialRelationship.
- **MATERIAL_RELATIONSHIP_INDEX_NAME**: Name of the Elasticsearch index for materialRelationship.

## material
- **MATERIAL_RDF_DIR**: Path to the directory containing RDF files for material.
- **MATERIAL_CACHE_DIR**: Path to the cache file for material.
- **MATERIAL_INDEX_NAME**: Name of the Elasticsearch index for material.

## taxon
- **TAXON_RDF_DIR**: Path to the directory containing RDF files for taxon.
- **TAXON_CACHE_DIR**: Path to the cache file for taxon.
- **TAXON_INDEX_NAME**: Name of the Elasticsearch index for taxon.

## index (Elasticsearch)
- **ES_MASTER_URI**: URI of the Elasticsearch master node.
- **ES_CLUSTER_USER**: Username for the Elasticsearch cluster.
- **ES_CLUSTER_PWD**: Password for the Elasticsearch cluster.
- **ES_PREFIX_TYPES_WITH**: Prefix for Elasticsearch index types.
- **ES_DEFAULT_SHARDS**: Default number of shards for Elasticsearch indexes.
- **ES_DEFAULT_REPLICAS**: Default number of replicas for Elasticsearch indexes.

## rdf
- **RDFSTORE_USER**: Username for the RDF store.
- **RDFSTORE_PWD**: Password for the RDF store.
- **RDFSTORE_ROOT_URI**: Root URI for the RDF store.
- **RDFSTORE_READ_ENDPOINT**: RDF store read endpoint.
- **RDFSTORE_WRITE_ENDPOINT**: RDF store write endpoint.
- **RDFSTORE_CLIENT_DRIVER**: Client driver for the RDF store.

## connector
- **RDF_REPO**: Name of the RDF repository.
- **ELASTIC_SEARCHNODE**: URI of the Elasticsearch node.