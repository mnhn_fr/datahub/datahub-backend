import Dependencies.{akkaActor, akkaSlf4J, akkaStream, akkaStreamTestkit, alpakkaCsv, alpakkaSlick, logbackClassic, playJson, playJsonExt, postgresql, rocksdb, scalaLogging, scalaTest, typeSafeSllConfig, typesafeConfig}
import Synaptix.{scopt, synaptixCacheToolkit, synaptixCore, synaptixGraphIndexingToolkit, synaptixHttp, synaptixIndexToolkit, synaptixJobToolkit, synaptixOaiToolkit, synaptixRdfToolkit}
import sbt.Keys.artifact

enablePlugins(sbtdocker.DockerPlugin, sbtassembly.AssemblyPlugin)

ThisBuild / scalaVersion := "2.13.12"

val meta = """META.INF(.)*""".r


lazy val root = (project in file("."))
  .settings(
    name := "datahub-backend",
    description := "Middleware sémantique pour le projet MNHN datahub",
    libraryDependencies ++= Seq(
      scalaTest,
      scalaLogging,
      logbackClassic,
      typesafeConfig,
      akkaActor,
      akkaStream,
      akkaStreamTestkit,
      akkaSlf4J,
      playJson,
      playJsonExt,
      rocksdb,
      synaptixCore,
      synaptixHttp,
      synaptixRdfToolkit,
      synaptixCacheToolkit,
      synaptixJobToolkit,
      synaptixOaiToolkit,
      synaptixIndexToolkit,
      synaptixGraphIndexingToolkit,
      alpakkaSlick,
      postgresql,
      typeSafeSllConfig,
      scopt,
      alpakkaCsv
    ),
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("application.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", "org.apache.jena.system.JenaSubsystemLifecycle") => MergeStrategy.concat
      case PathList("META-INF", "services", "org.apache.spark.sql.sources.DataSourceRegister") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    Compile / mainClass := Some("fr.mnhn.datahub.connectors.launcher.GraphDBConnector"),
   // run / mainClass := Some("fr.mnhn.datahub.connectors.launcher.GraphDBConnector"),
    assembly / mainClass := Some("fr.mnhn.datahub.connectors.launcher.GraphDBConnector"),
    assembly /assemblyJarName  := "datahub-backend-rdf.jar",
    docker / imageNames := Seq(
      ImageName(
        namespace = Some("registry.gitlab.com/mnhn_fr/datahub/datahub-backend"),
        repository = artifact.value.name,
        tag = Some(version.value)
      )
    ),
    docker / buildOptions := BuildOptions(
      cache = false,
      platforms = List("linux/amd64")
    ),
    docker / dockerfile := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("openjdk:11-jre-slim-buster")
        add(artifact, artifactTargetPath)
        run("mkdir", "-p", "/data/")
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  )