import sbt._

object Version {
  lazy val scalaVersion = "2.13.12"
  lazy val tsConfig = "1.4.1"
  lazy val akkaVersion = "2.9.0"
  lazy val playJson = "2.9.2"
  lazy val playJsonExt = "0.42.0"
  lazy val scalaTest = "3.2.9"
  lazy val scalaLogging = "3.9.4"
  lazy val logback = "1.3.5"
  lazy val rocksdb = "8.5.3"

  lazy val synaptix = "3.2.7-SNAPSHOT"
  lazy val alpakkaSlick = "7.0.0"
  lazy val akkaSlf4J = "2.9.0"

  lazy val scopt = "4.0.1"
  lazy val alpakkaCsv = "3.0.4"
  lazy val version = "0.1.0-SNAPSHOT"
}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val typesafeConfig = "com.typesafe" % "config" % Version.tsConfig
  lazy val typeSafeSllConfig = "com.typesafe" %% "ssl-config-core" % "0.6.1"


  lazy val akkaActor = "com.typesafe.akka" %% "akka-actor" % Version.akkaVersion
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akkaVersion
  lazy val akkaStreamTestkit = "com.typesafe.akka" %% "akka-stream-testkit" % Version.akkaVersion
  lazy val akkaSlf4J = "com.typesafe.akka" %% "akka-slf4j" % Version.akkaSlf4J
  lazy val alpakkaSlick = "com.lightbend.akka" %% "akka-stream-alpakka-slick" % Version.alpakkaSlick

  lazy val playJson = "com.typesafe.play" %% "play-json" % Version.playJson
  lazy val playJsonExt = "ai.x" %% "play-json-extensions" % Version.playJsonExt

  lazy val rocksdb = "org.rocksdb" % "rocksdbjni" % Version.rocksdb
  lazy val postgresql = "org.postgresql" % "postgresql" % "42.7.0"
  lazy val alpakkaCsv = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % Version.alpakkaCsv

}

object Synaptix {
  lazy val synaptixCore = "com.mnemotix" % "synaptix-core_2.13" % Version.synaptix
  lazy val synaptixHttp = "com.mnemotix" % "synaptix-http-toolkit_2.13" % Version.synaptix
  lazy val synaptixRdfToolkit = "com.mnemotix" % "synaptix-rdf-toolkit_2.13" % Version.synaptix
  lazy val synaptixCacheToolkit = "com.mnemotix" % "synaptix-cache-toolkit_2.13" % Version.synaptix
  lazy val synaptixIndexToolkit = "com.mnemotix" % "synaptix-indexing-toolkit_2.13" % Version.synaptix
  lazy val synaptixJobToolkit = "com.mnemotix" % "synaptix-jobs-toolkit_2.13" % Version.synaptix
  lazy val synaptixOaiToolkit = "com.mnemotix" % "synaptix-oai-toolkit_2.13" % Version.synaptix
  lazy val synaptixGraphIndexingToolkit = "com.mnemotix" %% "synaptix-graph-indexing-toolkit" % Version.synaptix

  lazy val scopt = "com.github.scopt" %% "scopt" % Version.scopt
}
